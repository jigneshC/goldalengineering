@extends('layouts.apex')
@section('title',trans('issue.label.issues'))

@section('content')

 @include("admin.issues.adcat")
    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('issue.label.issues') </div>
                {{-- @include('partials.page_tooltip',['model' => 'user','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
							<a href="{{ url('/admin/survey/'.$item->survey_id) }}" title="@lang('survey.label.survey_detail')">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
								<i class="fa fa-angle-left"></i> @lang('survey.label.survey_detail')
							</button>
							 </a>
							 
							 <a href="{{ url('/admin/issues/'.$item->id) }}" class="price_option" title="@lang('issue.label.issue_detail')">
                            <button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">
								<i class="fa fa-eye"></i> @lang('issue.label.issue_detail')
							</button>
							 </a>
							 
							
							
							
							
							 
							 @if($item->survey ) 
							 <a href="{{ url('/admin/survey/'.$item->survey->id.'/detail') }}?#issue_detail_{{$item->id}}" class="price_option" title="@lang('survey.label.all_detail')">
                            <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1">
								<i class="ft-file-text"></i> @lang('survey.label.all_detail')
							</button>
							 </a>
							 @endif
							 
							 {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/issues', $item->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> '.trans('tooltip.common.icon.delete'), array(
                            'type' => 'submit',
                            'class' => 'btn btn-raised btn-danger btn-min-width mr-1 mb-1',
                            'title' => trans('comman.label.delete'),
                            'onclick'=>"return confirm('".trans('common.js_msg.confirm_for_delete_data')."')"
                            ))!!}
                            {!! Form::close() !!} 

								
	            </div>
	            <div class="card-body">
	                <div class="px-3">
					
	                    {!! Form::model($item, [
                                'method' => 'PATCH',
                                'url' => ['/admin/issues', $item->id],
                                'class' => 'form-horizontal',
                                'files' => true,
                                'autocomplete'=>'off'
                            ]) !!}

                            @include ('admin.issues.form', ['submitButtonText' => trans('common.label.update')])

                            {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>

   
@endsection


