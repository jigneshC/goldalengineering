@extends('layouts.apex')

@section('title',trans('issue.label.issues'))

@section('content')
 @include("admin.issues.adcat")
    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('issue.label.issues') </div>
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                          <a href="{{ url('/admin/survey/'.$survey->id) }}" title="@lang('survey.label.survey_detail')">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
								<i class="fa fa-angle-left"></i> @lang('survey.label.survey_detail')
							</button>
							 </a>
	                <div class="actions pull-right">
                          
                        </div>
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
					
					
	                     {!! Form::open(['url' => '/admin/issues', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                                @include ('admin.issues.form')

                        {!! Form::close() !!}
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection
