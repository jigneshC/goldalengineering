<?php
 $hid =	"";
 if(isset($item)){
	 $hid =	"_".$item->id;
 }
 ?>

<div class="modal fade text-left" id="unitpackage_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                     <h3 class="modal-title title_form_model">@lang('categories.label.create_survey_category')</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                   
                
            </div>
            {!! Form::open(['url' => '','id'=>'unitpackage_form','class' => 'form-horizontal unitpackage_form', 'files' => true]) !!}
			
            <input type="hidden" name="survey_unique_id" id="survey_unique_id" value="{{$survey->unique_id}}" />
            <input type="hidden" name="cat_parent_id" id="cat_parent_id_hidden" value="" />
           
                        
            <div class="modal-body">
				<div class="form-group {{ $errors->has('cat_parent_id') ? 'has-error' : ''}}">
					<label for="cat_parent_id" class="">
						<span class="field_compulsory">*</span>@lang('categories.label.category')
					</label>
					 @if(old('category_id'))
						@php( $selected = old('category_id') ) 
					 @elseif(isset($item))
						 @php( $selected = $item->category_id ) 
					 @else
						 @php( $selected = "" )  
					 @endif
					<select class="form-control" id="cat_parent_id" disabled name="cat_parent_id0">
					@foreach($categories as $k => $val)
						 <option value="{{$val->id}}" {{ ($val->id == $selected ) ? 'selected':'' }} >{{$val->order_label}} {{$val->name}}</option>
					@endforeach
					</select>
					{!! $errors->first('category_id', '<p class="help-block text-danger">:message</p>') !!}
				</div>
				
				<div class="form-group {{ $errors->has('category_name') ? ' has-error' : ''}}">
                {!! Form::label('category_name',trans('categories.label.sub_chapter') ,['class' => '']) !!}
                <div class="form-group position-relative">
                    <input type="text" name="category_name" class="filter form-control"  id="category_name" style="">    
                </div>
				</div>
                
                
				{{-- <div class="form-group {{ $errors->has('display_order') ? ' has-error' : ''}}">
					<label for="display_order" >
					<span class="field_compulsory">*</span>
						@lang('categories.label.display_order')
					  
					</label>
					<div >
						{!! Form::number('display_order', null, ['class' => 'form-control']) !!}
					</div>
				</div> --}}
							
			</div>
            <div class="modal-footer">
                <p class="form_submit_error text-error"></p>
               
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.submit'), ['class' => 'btn btn-primary']) !!}

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



@push('js')


<script>


	 
	 
 
/***************action model******************/
		var _model = "#unitpackage_modal";
        $(document).on('click', '.form_open', function (e) {
            var id= $("#category_id").val();
			$('#unitpackage_form')[0].reset();
			$("#cat_parent_id").val(id);
			$("#cat_parent_id_hidden").val(id);
			$(_model).modal('show');
            return false;
        });
		

    $('.unitpackage_form').submit(function(event) {

        var error_msg = "";
        

        
        var url = "{{url('admin/survey-category')}}";
        var method = "POST";

        $.ajax({
            type: method,
            url: url,
            dataType:'json',
            async:false,
            processData: false,
            contentType: false,
            data:new FormData($("#unitpackage_form")[0]),
            success: function (result) {
                $(_model).modal('hide');
                toastr.success('Action Success!', result.message);
				data = result.data;
				$("#cat_img_selection").html("");
				$("#quote{{$hid}}").val("");
				$("#recommendation{{$hid}}").val("");
				$("#issue_detail{{$hid}}").val("");
				$("#cost_detail{{$hid}}").val("");
				$("#unit_cost{{$hid}}").val(0);
				initSelect(data.unique_id);
               
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
</script>
@endpush