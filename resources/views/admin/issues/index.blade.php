@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('survey.label.survey'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('survey.label.survey') </div>
        {{--  @include('partials.page_tooltip',['model' => 'user','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-6">
                        <div class="actions pull-left">
                            
                              
                            
                          </div>
                         </div>
                        <div class="col-6">
                          
                            @include("admin.filter_deleted") 
							
						</div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <div class="table-responsive">
                           <table class="table table-bordered table-striped datatable responsive">
                            <thead>
                            <tr>
                                <th># @lang('common.label.id')</th>
                                <th>@lang('survey.label.name')</th>
                                <th>@lang('survey.label.email')</th>
                                <th>@lang('survey.label.address')</th>
                                <th>@lang('survey.label.issue_count')</th>
                                <th>@lang('survey.label.status')</th>
                                <th>@lang('survey.label.created')</th>
                                <th>@lang('common.label.action')</th>
                              
                            </tr>
                            </thead>

                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection




@push('js')
<script>

	var statues = <?php echo json_encode(trans('survey.status')); ?>;
	
	
	var url ="{{ url('/admin/survey') }}";
    var edit_url = "{{ url('/admin/survey') }}";
    var auth_check = "{{ Auth::check() }}";
		
	var auth_uid = {{\Auth::user()->id}};
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('common.datatable.emptyTable')",
            "infoEmpty":"@lang('common.datatable.infoEmpty')",
            "search": "@lang('common.datatable.search')",
            "sLengthMenu": "@lang('common.datatable.show') _MENU_ @lang('common.datatable.entries')",
            "sInfo": "@lang('common.datatable.showing') _START_ @lang('common.datatable.to') _END_ @lang('common.datatable.of') _TOTAL_ @lang('common.datatable.small_entries')",
            paginate: {
                next: '@lang('common.datatable.paginate.next')',
                previous: '@lang('common.datatable.paginate.previous')',
                first:'@lang('common.datatable.paginate.first')',
                last:'@lang('common.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: true,
        order: [1, "asc"],
        columns: [
                { data: 'id',name : 'id',"searchable": true, "orderable": true},
                { data: 'owner_name',name : 'owner_name',"searchable": true, "orderable": true},
                { data: 'owner_email',name : 'owner_email',"searchable": true, "orderable": true},
                { 
					"data": null,
					"name":"address",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						var content = o.address;
						if(content && content.length > 80){
							content = content.substr(0, 80) + " ...";
						}
						return "<span class='line-break'>"+content+"</span>";
					}
				},
				{ data: 'issue_count',name : 'issue_count',"searchable": false, "orderable": true},
				{ 
					"data": null,
					"name":"status",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						return (o.status in statues) ? statues[o.status] : o.status;
					}
				},
				{ 
					"data": null,
					"name":"created_at",
					"searchable": false,
					"orderable": true,
					"render": function (o) {
						return o.created_a+"<br/>"+o.created_tz;
					}
				},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='@lang('tooltip.common.icon.eye')' ><i class='fa fa-eye' ></i></button></a>&nbsp;";

						if(o.status == "closed"){
                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" title='@lang('tooltip.common.icon.edit')' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a>&nbsp;";
						}

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-log' title='@lang('tooltip.common.icon.delete')'  data-id="+o.id+" ><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";
                        return v+e+d;
                    }

                }
         ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/survey-data') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                @if(request()->has('force'))
					d.enable_deleted = ($('#is_deleted_record').is(":checked")) ? 1 : 0;
				@endif
            }
        }
    });

    $('.filter').change(function() {
        datatable.fnDraw();
    });
	$('#is_deleted_record').change(function() {
		datatable.fnDraw();
    });

    $(document).on('click', '.del-log', function (e) {
			var id = $(this).attr('data-id');
			var r = confirm("@lang('common.js_msg.confirm_for_delete_data')");
			if (r == true) {
				$.ajax({
					type: "DELETE",
					url: "{{ url('/admin/survey') }}" + "/" + id,
					headers: {
						"X-CSRF-TOKEN": "{{ csrf_token() }}"
					},
					success: function (data) {
						datatable.fnDraw();
						toastr.success("@lang('common.js_msg.action_success')", data.message)
					},
					error: function (xhr, status, error) {
						toastr.error("@lang('common.js_msg.action_not_procede')",erro)
					}
				});
			}
		});


</script>


@endpush
