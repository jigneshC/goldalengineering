<?php
 $hid =	"";
 $otherJson = [];
 if(isset($item)){
	 $hid =	"_".$item->id;
	 $otherJson = json_decode($item->other_json_data,true);
}
 ?>



<div class="row ">


    <div class="col-md-12">
      @if(isset($survey))
        {!! Form::hidden('survey_id',$survey->id, ['class' => 'form-control','id'=>'survey_id']) !!}
      @endif
		{!! Form::hidden('status',"new", ['class' => 'form-control']) !!}


		<div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
            <label for="category_id" class="">
                <span class="field_compulsory">*</span>@lang('issue.label.category')
            </label>
			 @if(old('category_id'))
				@php( $selected = old('category_id') )
			 @elseif(isset($item))
				 @php( $selected = $item->category_id )
			 @else
				 @php( $selected = "" )
			 @endif
			<select class="form-control" id="category_id" name="category_id">
			@foreach($categories as $k => $val)
                 <option value="{{$val->id}}" {{ ($val->id == $selected ) ? 'selected':'' }} >{{$val->order_label}} {{$val->name}}</option>
            @endforeach
			</select>
			{!! $errors->first('category_id', '<p class="help-block text-danger">:message</p>') !!}
		</div>


		<div class="form-group {{ $errors->has('child_category_id') ? 'has-error' : ''}}">
            <label for="child_category_id" class="">
                <span class="field_compulsory">*</span>@lang('issue.label.child_category')
            </label>
			<select class="form-control" id="child_category_id" name="child_category_id">
			</select>
			<a href="#" class="form_open" >@lang('categories.label.create_survey_subchapter')</a>
			{!! $errors->first('child_category_id', '<p class="help-block text-danger">:message</p>') !!}



			<div class="card" id="cat_img_selection">

			</div>




		</div>

		<div class="form-group {{ $errors->has('location') ? ' has-error' : ''}}">
            <label for="location" >
                <span class="field_compulsory">*</span>@lang('issue.label.location')
            </label>
            <div >
			@if(isset($item) && $item->location)
			{!! Form::text('location', null, ['class' => 'location','id'=>'location','style'=>'width:100%']) !!}
			@else
			{!! Form::text('location', $survey->getLastLocation(), ['class' => 'location','id'=>'location','style'=>'width:100%']) !!}
			@endif
				{!! $errors->first('location', '<p class="help-block text-danger">:message</p>') !!}

            </div>
        </div>

	   <div class="form-group {{ $errors->has('issue_detail') ? ' has-error' : ''}}">
            <label for="issue_detail" >
                @lang('issue.label.issue_detail')
            </label>
            <div >
                {!! Form::textarea('issue_detail', old('issue_detail',null) , ['id'=>'issue_detail'.$hid,'class' => 'form-control']) !!}
				{!! $errors->first('issue_detail', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>



		<div class="form-group {{ $errors->has('recommendation') ? ' has-error' : ''}}">
            <label for="recommendation" >
                @lang('issue.label.recommendation')
            </label>
            <div >
                {!! Form::textarea('recommendation', old('recommendation',null) , ['id'=>'recommendation'.$hid,'class' => 'form-control']) !!}
				{!! $errors->first('recommendation', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>

		<div class="form-group {{ $errors->has('quote') ? ' has-error' : ''}}">
            <label for="quote" >
                @lang('issue.label.quote')
            </label>
            <div >
                {!! Form::textarea('quote', old('quote',null) , ['id'=>'quote'.$hid,'class' => 'form-control']) !!}
				{!! $errors->first('quote', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>

		<div class="form-group {{ $errors->has('note') ? ' has-error' : ''}}">
            <label for="note" >
                @lang('issue.label.note')
            </label>
            <div >
                {!! Form::textarea('note', old('note',null) , ['id'=>'note'.$hid,'class' => 'form-control']) !!}
				{!! $errors->first('note', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>


		<div class="form-group {{ $errors->has('number_of_unit') ? ' has-error' : ''}}">
			<label for="number_of_unit" >
			
				@lang('issue.label.number_of_unit')

			</label>
			<div >
				{!! Form::number('number_of_unit', null, ['step'=>"0.01",'min'=>0,'class' => 'form-control cost_calc number_of_unit','id'=>'number_of_unit'.$hid]) !!}
				{!! $errors->first('number_of_unit', '<p class="help-block text-danger">:message</p>') !!}
			</div>
		</div>

		<div class="form-group {{ $errors->has('unit_cost') ? ' has-error' : ''}}">
			<label for="flag_show_unit_price_in_report" >
			
				@lang('issue.label.unit_cost')
						 | @lang('issue.label.show_in_report') <input type="checkbox" name="other[flag_show_unit_price_in_report]" @if(isset($otherJson['flag_show_unit_price_in_report'])) checked @endif class="switchery"  id="flag_show_unit_price_in_report" value='1' >
			</label>
			<div >
				{!! Form::number('unit_cost', null, ['step'=>"0.01",'min'=>0,'class' => 'form-control cost_calc unit_cost',"id"=>"unit_cost".$hid]) !!}
				{!! $errors->first('unit_cost', '<p class="help-block text-danger">:message</p>') !!}
			</div>
		</div>


		<div class="form-group {{ $errors->has('total_cost') ? ' has-error' : ''}}">
			<label for="total_cost" >
				@lang('issue.label.total_cost')

			</label>
			<div >
				{!! Form::number('total_cost', null, ['readonly'=>'readonly','class' => 'form-control total_cost','id'=>'total_cost'.$hid]) !!}
				{!! $errors->first('total_cost', '<p class="help-block text-danger">:message</p>') !!}
			</div>
		</div>


		<div class="form-group {{ $errors->has('cost_detail') ? ' has-error' : ''}}">
            <label >
                @lang('issue.label.cost_detail')
				 | @lang('issue.label.show_default_price_detail') <input type="checkbox" name="show_default_price_detail" @if(isset($item) && $item->show_default_price_detail) checked @elseif(isset($item))  @else checked @endif class="switchery"  id="show_default_price_detail" value='1' >
            </label>
			@if(isset($item) && $item->costImage && $item->costImage->file_thumb_url && $item->costImage->file_thumb_url != "")
			<div class="row">
			<div class="col-sm-2 relative-container" id="ref{{$item->costImage->id}}">
				<a href="{{url('admin/reference-file/'.$item->costImage->id.'/delete')}}" onclick="return confirm('@lang('common.js_msg.confirm_for_delete',['item_name'=>trans('common.label.file')])')" class="close btn btn-danger btn-sm"><i class='fa fa-trash' aria-hidden='true'></i></a>
				<a class="example-image-link " href="{!! $item->costImage->file_url !!}?uid={{ time() }}" data-lightbox="example-2" data-title="{{$item->costImage->refe_file_real_name}}">
				<img src="{!! $item->costImage->file_thumb_url !!}?uid={{ time() }}" height="75" />
				</a>
				<a href="{{url('admin/reference-file/'.$item->costImage->id.'/rotate')}}" class="rotate btn btn-danger btn-sm"  ><i class='ft-corner-up-right' aria-hidden='true'></i>  </a>
				<a href="{{url('admin/reference-file/'.$item->costImage->id.'/rotate')}}?dir=left"  class="rotate right35 btn btn-danger btn-sm" ><i class='ft-corner-up-left' aria-hidden='true'></i>  </a>
			</div>
			</div>
			@else
			<div class="past-fields">
				{!! Form::textarea('cost_detail', old('cost_detail',null) , ['id'=>'cost_detail'.$hid,'class' => 'form-control']) !!}
				{!! $errors->first('cost_detail', '<p class="help-block text-danger">:message</p>') !!}
			</div>
			@endif
			
			 
			
        </div>
		<div class="form-group {{ $errors->has('cost_detail') ? ' has-error' : ''}}">
		{!! Form::hidden('cost_detail_img',"", ['class' => 'form-control',"id"=>"cost_detail_img"]) !!}
		<img src="" style="height:300px;width:500px;display:none" id="past-preview">
		
		</div>


		<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
			<label for="logo"> @lang('issue.label.issue_images') </label>
			<div class="">
				@if(isset($item) && $item->refefile->count())
			<a href="{{ url('admin/issues-image-edit/'.$item->id)}}">	@lang('issue.label.update_image') </a>
			@endif
				@if(isset($item) && $item->refefile->count())
				<div class="row">
				@foreach($item->refefile as $rf)
				@if($rf->file_thumb_url && $rf->file_thumb_url != "")
				<div class="col-sm-2 relative-container" id="ref{{$rf->id}}">
					<a href="{{url('admin/reference-file/'.$rf->id.'/delete')}}" onclick="return confirm('@lang('common.js_msg.confirm_for_delete',['item_name'=>trans('common.label.file')])')" class="close btn btn-danger btn-sm"><i class='fa fa-trash' aria-hidden='true'></i></a>
					<a class="example-image-link " href="{!! $rf->file_url !!}?uid={{ time() }}" data-lightbox="example-2" data-title="{{$rf->refe_file_real_name}}">
					<img src="{!! $rf->file_thumb_url !!}?uid={{ time() }}" height="75" />
					</a>
					<a href="{{url('admin/reference-file/'.$rf->id.'/rotate')}}" class="rotate btn btn-danger btn-sm"  ><i class='ft-corner-up-right' aria-hidden='true'></i>  </a>
					<a href="{{url('admin/reference-file/'.$rf->id.'/rotate')}}?dir=left"  class="rotate right35 btn btn-danger btn-sm" ><i class='ft-corner-up-left' aria-hidden='true'></i>  </a>
				</div>

				@endif
				@endforeach
				</div>
				<br/>
				@endif
				{!! Form::file('images[]',  ['id'=>'issie_images'.$hid,'class' => 'form-control','multiple'=>true]) !!}
				{!! $errors->first('images.*', '<p class="help-block text-danger">:message</p>') !!}
				<p> @lang('common.label.max_limit') </p>
			</div>
		</div>

        <div class="form-group">
		@if(isset($item))
			{!! Form::hidden('saveandnext',"", ['class' => 'form-control',"id"=>"saveandnext"]) !!}
			{!! Form::submit(trans('common.label.save'), ['class' => 'btn btn-primary form_submit']) !!}
			{!! Form::submit(trans('common.label.saveandnext'), ['class' => 'btn btn-primary form_submit saveandnext','onclick'=>"document.getElementById('saveandnext').value = '1';; return true;"]) !!}
		@else
		{!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-primary form_submit']) !!}	
		@endif
        {{ Form::reset(trans('common.label.clear_form'), ['class' => 'btn btn-light']) }}
		@if(isset($item))


		@endif
        </div>


    </div>


</div>



@push('js')
<script>
	$(function() {

	  var max_file_number = 30,
		  // Define your form id or class or just tag.
		  $form = $('form'),
		  // Define your upload field class or id or tag.
		  $file_upload = $('#issie_images{{$hid}}_'),
		  // Define your submit class or id or tag.
		  $button = $('.form_submit_', $form);

	  // Disable submit button on page ready.
	  $button.prop('disabled', 'disabled');

	  $file_upload.on('change', function () {
		var mexsize = "";
		var number_of_images = $(this)[0].files.length;
		for (var i = 0; i <= number_of_images - 1; i++) {

            var fsize = $(this)[0].files[i].size;
            var filekb = Math.round((fsize / 1024));
            // The size of the file.
            if (filekb >= 1024*8) {
                mexsize = "File too Big, please select a file less than 8mb";
            }
        }
		if (number_of_images > max_file_number) {
		  alert(`You can upload maximum ${max_file_number} files.`);
		  $(this).val('');
		  $button.prop('disabled', 'disabled');
		}else if(mexsize != ""){
			alert(mexsize);
			$(this).val('');
			$button.prop('disabled', 'disabled');
		} else {
		  $button.prop('disabled', false);
		}
	  });
	});


    var sub_search_url ="{{url('admin/categories/search')}}";


	$("#location").select2({
        placeholder: "@lang('location.label.search_location')",
        allowClear: true,

        @if(old('location'))
            initSelection: function (element, callback) {
                callback({id: "{{old('location')}}", text: "{{old('location')}}" });
            },
		@elseif(isset($item))
            initSelection: function (element, callback) {
				@php( $arr = json_encode(['id'=>$item->location,'text'=>$item->location], JSON_HEX_APOS|JSON_HEX_QUOT) )
				var obje = {!! $arr !!};
                callback(obje);
            },
		@else
            initSelection: function (element, callback) {
                callback({id: "{{ $survey->getLastLocation() }}", text: "{{ $survey->getLastLocation() }}" });
            },
        @endif

        ajax: {
            url: "{{ url('admin/prefill-location') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    search: params, // search term
					survey_id:"{{$survey->id}}",
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.desc, text: obj.desc};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);


    var selected_cat = 0;
    var issue_id = 0;
    var default_price_info = "";

	@if(isset($item))
		 issue_id = "{{ $item->id }}";
	@endif

    @if(old('child_category_id'))
		 selected_cat = "{{ old('child_category_id') }}";
	@elseif(isset($item) && $item->child_category_id && $item->child_category_id !="" && $item->child_category_id != 0)
		 selected_cat = "{{ $item->child_category_id }}";
	@elseif(isset($item) && $item->child_unique_id && $item->child_unique_id !="")
		 selected_cat = "{{ $item->child_unique_id }}";
	@endif

    initSelect();

    function initSelect(sid=0){
		if(sid && sid !=0){
			selected_cat = sid;
		}
		$("#child_category_id").html("");

		$.ajax({
            type: "get",
            url: sub_search_url,
            data:{parent_id:$('#category_id').val(),survey_id:$('#survey_id').val()},
            success: function (result) {
                data = result.data;
                for(var i=0;i<data.length;i++){
                    var selected="";
                    if (data[i]['id'] == selected_cat) { selected = "selected=selected"; }
                    $("#child_category_id").append("<option value='" + data[i]['id'] + "' "+selected+">"+data[i]['order_label']+" "+data[i]['name'] + "</option>");
                }
				catImgview();

			},
            error: function (xhr, status, error) {
            }
        });

	}

	function catImgview(){
		default_price_info = "";
		@if(!isset($item))
		$("#cat_img_selection").html("");
		$("#quote{{$hid}}").val("");
		$("#recommendation{{$hid}}").val("");
		$("#issue_detail{{$hid}}").val("");
		$("#cost_detail{{$hid}}").val("");
		$("#unit_cost{{$hid}}").val(0);
		@endif

		$.ajax({
            type: "get",
            url: "{{url('admin/categories/img-view')}}",
            data:{category_id:$('#child_category_id').val(),issue_id:issue_id},
            success: function (result) {
                $("#cat_img_selection").html(result.html);
				if(result.data){
					default_price_info = result.data.cost_detail;
					@if(!isset($item))
					$("#quote{{$hid}}").val(result.data.quote);
					$("#recommendation{{$hid}}").val(result.data.recommendation);
					$("#issue_detail{{$hid}}").val(result.data.issue_detail);
					$("#cost_detail{{$hid}}").val(result.data.cost_detail);
					$("#unit_cost{{$hid}}").val(result.data.default_price);
					@else
						if(!$("#quote{{$hid}}").val() ){ $("#quote{{$hid}}").val(result.data.quote); }
						if(!$("#recommendation{{$hid}}").val() ){ $("#recommendation{{$hid}}").val(result.data.recommendation);  }
						if(!$("#issue_detail{{$hid}}").val() ){ $("#issue_detail{{$hid}}").val(result.data.issue_detail);  }
						@if($item->cost_detail && $item->cost_detail != "")
							@if(0)
							$("#cost_detail{{$hid}}").val("{{ htmlspecialchars($item->cost_detail) }}"); 
							@endif
						@else
							if($('#show_default_price_detail').is(":checked") ){ $("#cost_detail{{$hid}}").val(result.data.cost_detail);  }
						@endif
						
						if(!$("#unit_cost{{$hid}}").val() ){ $("#unit_cost{{$hid}}").val(result.data.default_price);  }
					@endif
				}
			},
            error: function (xhr, status, error) {
            }
        });

	}

	$('#child_category_id').change(function() {
        catImgview();
    });

	$('#show_default_price_detail').change(function() {
		$("#cost_detail{{$hid}}").val(default_price_info);
    });

    $('#category_id').change(function() {
        initSelect();
    });
	$('.cost_calc').change(function() {

        var unit_cost = $('.unit_cost').val();
        var number_of_unit = $('.number_of_unit').val();
        var total_cost = unit_cost*number_of_unit;
		$('.total_cost').val(total_cost);
    });





</script>
<script src="{!! asset('public/apex/javascripts/past.js') !!}" type="text/javascript"></script>

@endpush


