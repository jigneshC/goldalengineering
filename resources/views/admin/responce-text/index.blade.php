@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('constant.label.constant_text'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('constant.label.constant_text') </div>
        {{--  @include('partials.page_tooltip',['model' => 'user','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
					{{--
                        <div class="actions pull-left">
                            
                                <a href="{{ url('/admin/constants/create') }}" class="btn btn-success btn-sm"
                                   title="@lang('common.label.add_new')">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('common.label.add_new')
                                </a>

                            
                          </div> --}}
                         </div>
                        <div class="col-9">
                          
                            
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <div class="table-responsive">
                           <table class="table table-bordered table-striped datatable responsive">
                            <thead>
                            <tr>
                                <th># @lang('common.label.id')</th>
                                <th>@lang('constant.label.slug')</th>
								<th>@lang('check-list.label.title')</th>
                                <th>@lang('constant.label.value')</th>
                                <th>@lang('common.label.action')</th>
                            </tr>
                            </thead>

                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection




@push('js')
<script>

	var slugs = <?php echo json_encode(trans('constant.slug')); ?>;
	var responce_types = <?php echo json_encode(trans('common.responce_type')); ?>;
	
	var url ="{{ url('/admin/constants') }}";
    var edit_url = "{{ url('/admin/constants') }}";
    var auth_check = "{{ Auth::check() }}";
		
	var auth_uid = {{\Auth::user()->id}};
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('common.datatable.emptyTable')",
            "infoEmpty":"@lang('common.datatable.infoEmpty')",
            "search": "@lang('common.datatable.search')",
            "sLengthMenu": "@lang('common.datatable.show') _MENU_ @lang('common.datatable.entries')",
            "sInfo": "@lang('common.datatable.showing') _START_ @lang('common.datatable.to') _END_ @lang('common.datatable.of') _TOTAL_ @lang('common.datatable.small_entries')",
            paginate: {
                next: '@lang('common.datatable.paginate.next')',
                previous: '@lang('common.datatable.paginate.previous')',
                first:'@lang('common.datatable.paginate.first')',
                last:'@lang('common.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [0, "DESC"],
        columns: [
                { data: 'id',name : 'id',"searchable": true, "orderable": true},
                { 
					"data": null,
					"name":"slug",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						return (o.slug in slugs) ? slugs[o.slug] : o.slug;
					}
				},
				{ data: 'title',name : 'title'},
				{ 
					"data": null,
					"name":"desc",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						var content = o.desc;
						if(content && content.length > 80){
							content = content.substr(0, 80) + " ...";
						}
						return "<span class='line-break'>"+content+"</span>";
					}
				},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='@lang('tooltip.common.icon.eye')' ><i class='fa fa-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" title='@lang('tooltip.common.icon.edit')' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a>&nbsp;";

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-log' title='@lang('tooltip.common.icon.delete')'  data-id="+o.id+" ><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";
                        return v+e;
                    }

                }
         ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/constants-data') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                
            }
        }
    });

    $('.filter').change(function() {
        datatable.fnDraw();
    });

    $(document).on('click', '.del-log', function (e) {
			var id = $(this).attr('data-id');
			var r = confirm("@lang('common.js_msg.confirm_for_delete_data')");
			if (r == true) {
				$.ajax({
					type: "DELETE",
					url: "{{ url('/admin/constants') }}" + "/" + id,
					headers: {
						"X-CSRF-TOKEN": "{{ csrf_token() }}"
					},
					success: function (data) {
						datatable.fnDraw();
						toastr.success("@lang('common.js_msg.action_success')", data.message)
					},
					error: function (xhr, status, error) {
						toastr.error("@lang('common.js_msg.action_not_procede')",erro)
					}
				});
			}
		});


</script>


@endpush
