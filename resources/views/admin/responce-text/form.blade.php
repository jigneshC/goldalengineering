

<div class="row ">

    
    <div class="col-md-12">
      
        
        {!! Form::hidden('responce_type',"constant", ['class' => 'form-control']) !!}
		@if(isset($item))
        {!! Form::hidden('slug',$item->slug, ['class' => 'form-control']) !!}
		@else
			<div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
				<label for="slug" class="">
					<span class="field_compulsory">*</span>@lang('constant.label.slug')
				</label>
				{!! Form::select('slug',$editable_slug, null,['class' => 'form-control','id'=>'slug']) !!}
				{!! $errors->first('slug', '<p class="help-block text-danger">:message</p>') !!}

			</div>
		@endif

        
        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
            <label for="title" class="">
                <span class="field_compulsory">*</span>@lang('check-list.label.title')
            </label>
			{!! Form::text('title',null,['class' => 'form-control','id'=>'title']) !!}
			{!! $errors->first('title', '<p class="help-block text-danger">:message</p>') !!}

        </div>
        
        
        <div class="form-group{{ $errors->has('desc') ? ' has-error' : ''}}">
            <label for="desc" >
                <span class="field_compulsory">*</span>@lang('constant.label.value')
            </label>
            <div >
                {!! Form::textarea('desc', old('description',null) , ['rows' => 10,'class' => 'form-control','id'=>'desc']) !!}
				{!! $errors->first('desc', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>
		
		<?php
		$seckey=["general_info","property_desc","property_description","legal_thing_a","legal_thing_isa","not_tested","reviews_and_summary","surveyor_note","other_info"];	
		?>
		@if(isset($item) && in_array($item->slug,$seckey))
		<div class="multi-group {{ $errors->has('professional_materials') ? 'has-error' : ''}}">
			@if(isset($item) && $item->textChecklist->count())
				@foreach($item->textChecklist as  $list)
				
				<div class="form-group ">
					<label for="password" class="title">
					   @lang('check-list.label.in_list_format')
					</label>
					 <label  class="action_btn pull-right">
					   <a href="#" class="add_row" ><i style="padding: 8px;background-color: #7235a2;border-radius: 50%;" class='fa fa-plus'></i></a>
					   <a href="#" class="remove_row" ><i style="padding: 8px;background-color: #7235a2;border-radius: 50%;" class='fa fa-minus'></i></a>
					</label>
					<div class="row">
					<div class="col-md-12">
					 {!! Form::textarea('desc_list[]',$list->desc , ['rows' => 3,'class' => 'form-control','id'=>'']) !!}
					 @lang('constant.label.allow_customize') <input type="checkbox" name="customise_option" value="1" @if($list->refe_type =='1') checked @endif> 
					 <input type="hidden" class="refe_types" name="refe_types[]" value="{{ $list->refe_type }}" >
					</div>
					</div>

				</div>
				@endforeach
			@else	
				<div class="form-group ">
					<label for="password" class="title">
					   @lang('check-list.label.in_list_format')
					</label>
					 <label  class="action_btn pull-right">
					   <a href="#" class="add_row" ><i style="padding: 8px;background-color: #7235a2;border-radius: 50%;" class='fa fa-plus'></i></a>
					   <a href="#" class="remove_row" ><i style="padding: 8px;background-color: #7235a2;border-radius: 50%;" class='fa fa-minus'></i></a>
					</label>
					<div class="row">
					<div class="col-md-12">
					{!! Form::textarea('desc_list[]','' , ['rows' => 3,'class' => 'form-control','id'=>'']) !!}
					@lang('constant.label.allow_customize') <input type="checkbox" name="customise_option" value="1"> 
					<input type="hidden" class="refe_types" name="refe_types[]" value="0" >
					</div>
					</div>

				</div>
			@endif
		</div>
		@endif

        
        

        
      
        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('common.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
   
        
    </div>
   
    
</div>



@push('js')
<script src="https://cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>
<script>

   var urla ="{{ url('/admin/constants') }}";
   //CKEDITOR.replace( 'desc' );
   
   
   $('#slug').change(function() {
        var slug=$(this).val();
        if(slug != ""){
            var url = urla+"/"+slug+"/edit?type=slug";   
            $.ajax({
                type: "GET",
                url: url,
                success: function (result) {
                    var data=result.data;
                    $('#desc').val(data.desc);
                }
            });
        }
        return false;
    });
	
	$(document).on('click', '.add_row', function (e) {
		var rootc = $(this).parents('.multi-group');
        var rowdata = $(this).parents('.form-group');
		$(rootc).append("<div class='form-group'>"+rowdata.html()+"</div>");
		$(rootc).children(".form-group").last().find(".form-control").val("");
		return false;
    });

	$(document).on('click', '.remove_row', function (e) {
        var rowdata = $(this).parents('.form-group');
		rowdata.remove();
		return false;
    });
	
	$(document).on('change', 'input[name="customise_option"]', function (e) {
		var isChecked = $(this).prop("checked");
		if (isChecked) {
			$(this).parent().find(".refe_types").val(1);
		} else {
			$(this).parent().find(".refe_types").val(0);
		}
	});
	
	


</script>


@endpush

