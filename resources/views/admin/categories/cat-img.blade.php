@if(isset($cat) && $cat->refefile->count() >0 )
<div class="card-header">
					<h4 class="card-title mb-0">@lang('categories.label.category_images')</h4>
				</div>
				<div class="card-body">
					<div class="card-block">
					
						@foreach($cat->refefile as $k => $ref)
						<div class="media mb-1">
							<a class="example-image-link " href="{!! $ref->file_url !!}" data-lightbox="example-2" data-title="" > 
								<img  class="media-object d-flex mr-3 bg-primary height-50 " src="{{$ref->file_thumb_url}}">
							</a>
							
							<div class="mt-1">
								<div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
									<input type="checkbox" name="cat_img[{{$ref->id}}]" @if(in_array($ref->id,$selected_img) || !$issue) checked @endif class="custom-control-input" value="1" id="cat_img{{$k}}">
									<label class="custom-control-label" for="cat_img{{$k}}"></label>
								</div>

							</div>
						</div>
						@endforeach
					</div>
				</div>
@endif				