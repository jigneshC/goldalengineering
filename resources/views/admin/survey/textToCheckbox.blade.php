@php
$_checklist = [];
$_checklist_checked = [];
if(isset($item)){
	
	if($item->textChecklist->count()){
		$fildsection = $item->textChecklist->where("refe_code",$slug_s);
		if($fildsection->count()){
			$_checklist = $fildsection->pluck("desc","priority")->toArray();
			$_checklist_checked = $fildsection->where("refe_type",1)->pluck("desc","priority")->toArray();
		}
	}
	// echo "<pre>"; print_r($_checklist_checked);
}
	
$const = \App\Responcetext::where("slug",$slug_c)->first();

@endphp

@if($const && $const->textChecklist->count())
<div class="view_list">
	<table border="1" style="width:100%">
	@foreach($const->textChecklist as  $list)
	<tr>
	<td style="width:15px">
	<input type="checkbox" @if((count($_checklist)==0 && $list->refe_type) || isset($_checklist_checked[$list->id])) checked @endif name="{{$slug_s}}_ison[{{$list->id}}]" value="1" >
	<input id="rtcb_list_{{$list->id}}" type="hidden"  name="{{$slug_s}}[{{$list->id}}]" value="@if(isset($_checklist[$list->id])) {{ $_checklist[$list->id] }} @else {{$list->desc}} @endif" >
	</td>
	
	<td class="val" id="rt_list_{{$list->id}}">
	@if(isset($_checklist[$list->id])) {{ $_checklist[$list->id] }} @else {{ $list->desc }} @endif
	</td>
	
	<td style="width:15px">
	<a href='#' data-id="{{$list->id}}" data-value="@if(isset($_checklist[$list->id])) {{ $_checklist[$list->id] }} @else {{ $list->desc }} @endif" title='@lang('tooltip.common.icon.edit')' class='btn btn-warning btn-sm update_rt_list'><i class='fa fa-pencil'></i></a>
	</td>
	
	
	@endforeach
	</table>
</div>
@else
<div class="view_form">
	{!! Form::textarea($slug_s, old($slug_s,null) , ['id'=>$slug_s,'class' => 'form-control']) !!}
	{!! $errors->first($slug_s, '<p class="help-block text-danger">:message</p>') !!}
</div>
@endif











