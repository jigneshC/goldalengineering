@extends('layouts.apex')
@section('title',trans('tooltip.common.icon.export_advance'))

@section('content')
    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('tooltip.common.icon.export_advance') </div>
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/survey') }}" title="@lang('common.label.back')">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
								<i class="fa fa-angle-left"></i> @lang('common.label.back')
							</button>
							 </a>
	            </div>
	            <div class="card-body">
	                <div class="px-3">
						<form method="get">
						<div class="row ">
							<div class="col-md-12">
								<div class="form-group">
									<label for="last_name" class="">
										@lang('survey.label.export_type')
									</label>
									<select id="export_type" class="form-control" name="export_type">
										<option value="export_record">@lang('tooltip.common.icon.export_record')</option>
										<option value="export_by_place">@lang('survey.label.export_by_place')</option>
										<option value="all_detail">@lang('survey.label.all_detail')</option>
									</select>
									
								</div>
								<div class="form-group">
									<label for="last_name" class="">
										@lang('survey.label.without_price')
									</label>
									<input type="checkbox" name="price" class="switchery"  id="price" value='1'>
								</div>
								<div class="form-group">
									<label for="last_name" class="">
										@lang('report.label.free_text')
									</label>
									<input type="checkbox" name="free_text" class="switchery"  id="free_text" value='1'>
								</div>
								
								<div class="form-group">
									<label for="last_name" class="">
										@lang('issue.label.category')
									</label>
									<select class="form-control" id="category_id" name="category_id">
										<option value="all" >All</option>
										@foreach($item->usedParentCategory() as $k => $val)
											 <option value="{{$val->id}}" >{{$val->order_label}} {{$val->name}}</option>
										@endforeach
										</select>
								</div>
								
								<div class="form-group">
								<a href="#" class="btn btn-primary" id = "submit_export">@lang('common.label.submit') </a>
								
								</div>
							</div>
						</div>
						</form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	
</section>

   
@endsection



@push('js')
<script>
	$(document).on('click', '#submit_export', function (e) {
		var tip = $("#export_type").val();
		var url = "{{ url('/admin/survey/'.$item->id.'/export') }}";
		
		if(tip == "export_by_place"){
			url = "{{ url('/admin/survey/'.$item->id.'/export-by-place') }}";
		}else if(tip == "all_detail"){
			url = "{{ url('/admin/survey/'.$item->id.'/detail') }}";
		}
		url =  url + "?t=1";
		
		if($("#free_text").prop("checked")){
			url =  url + "&free_text=1";
		}
		
		if($("#price").prop("checked")) {
			url =  url + "&price=1";
		}
		if($("#category_id").val() && $("#category_id").val() != "all") {
			url =  url + "&category_id="+$("#category_id").val();
		}		
        
		window.location.href = url;
    });

	
 </script>

@endpush

