@extends('layouts.apex')

@section('title',trans('survey.label.survey'))

@section('content')


@include ('admin.survey.issue_table',['item'=>$item])

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('survey.label.survey_detail')</div>
               
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header"> 
                        <div class="form-group">
                            
							<a href="{{ url('/admin/survey') }}" title="@lang('common.label.back')">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
								<i class="fa fa-angle-left"></i> @lang('common.label.back')
							</button>
							 </a>
							 
							 
							 
							@if($item->status == "closed" || request()->has('force'))
							<a href="{{ url('/admin/survey/'.$item->id.'/edit') }}?hashurl=total_cost_{{$item->id}}" title="@lang('tooltip.common.icon.edit')">
								<button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">
								<i class="fa fa-pencil"></i>
								@lang('tooltip.common.icon.edit')
								</button>
							</a>
							
							<a href='javascript:void(0);' data-surveyor-id="{{$item->surveyor_id}}" @if($item->surveyor) data-surveyor-name="{{$item->surveyor->full_name}}" @endif data-id="{{$item->id}}" title='@lang('tooltip.common.icon.edit')' class='to_local'>
							<button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
							<i class='fa ft-download'></i>
							@lang('survey.label.local_sync')
							</button>
							</a>
							
							
							@endif
							
							{!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/survey', $item->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> '.trans('tooltip.common.icon.delete'), array(
                            'type' => 'submit',
                            'class' => 'btn btn-raised btn-danger btn-min-width mr-1 mb-1',
                            'title' => trans('comman.label.delete'),
                            'onclick'=>"return confirm('".trans('common.js_msg.confirm_for_delete_data')."')"
                            ))!!}
                            {!! Form::close() !!}
							
							<a href="{{ url('/admin/survey/'.$item->id.'/export-advance') }}" class="price_option" title="@lang('tooltip.common.icon.export_advance')">
                            <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1">
								<i class="ft-file-text"></i> @lang('tooltip.common.icon.export_advance')
							</button>
							 </a>
							 
							<a href="{{ url('/admin/survey/'.$item->id.'/export') }}" class="price_option" title="@lang('tooltip.common.icon.export_record')">
                            <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1">
								<i class="ft-file-text"></i> @lang('tooltip.common.icon.export_record')
							</button>
							 </a>
							 
							 <a href="{{ url('/admin/survey/'.$item->id.'/export-by-place') }}" class="price_option" title="Exporty by placename">
                            <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1">
								<i class="ft-file-text"></i> @lang('survey.label.export_by_place')
							</button>
							 </a>
							 
							 <a href="{{ url('/admin/survey/'.$item->id.'/detail') }}" class="price_option" title="@lang('survey.label.all_detail')">
                            <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1">
								<i class="fa fa-eye"></i> @lang('survey.label.all_detail')
							</button>
							 </a>
							 
							 <a href="{{ url('/admin/issues-create/'.$item->id.'') }}" title="@lang('issue.label.add_issue')">
								<button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">
									<i class="fa fa-plus"></i> @lang('issue.label.add_issue')
								</button>
							 </a>
							<a href="{{ url('/admin/survey-duplicate/'.$item->unique_id.'') }}" title="@lang('survey.label.duplicate_survey')">
								<button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
									<i class="fa fa-plus"></i> @lang('survey.label.duplicate_option')
								</button>
							</a>	
							
							{!! Form::open(['url' => '/admin/survey-duplicate', 'class' => '','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
								
								<input type="hidden" name="unique_id" value="{{$item->unique_id}}">	
                                <button type="submit" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
									<i class="fa fa-plus"></i> @lang('survey.label.duplicate_survey')
								</button>
							{!! Form::close() !!}
							
								
							 
                        </div>
	                 <div class="next_previous pull-right">
                   
                      </div>  
                          
                        
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                   <div class="table-responsive custom-table-responsive">
                                        <table class="table table-striped">
                                            <tbody>

                                            <tr>
                                                <th>@lang('common.label.id')</th>
                                                <td>{{ $item->id }}</td>
                                            </tr>
											
											 <tr>

                                                <th>@lang('survey.label.surway_logo')</th>
                                                <td> 
												@if($item->branch) {{$item->branch->name}}
												<br/> 
													@if($item->branch->file_url && $item->branch->file_url != "")
														
																<a class="example-image-link " href="{!! $item->branch->file_url !!}" data-lightbox="example-2" data-title="{{$item->branch->name}}">
																<img src="{!! $item->branch->file_url !!}"  height="80" />
																</a>
													@endif
												@else 
													-
												@endif </td>
                                            </tr>
											
                                            <tr>

                                                <th>@lang('survey.label.name')</th>
                                                <td> {{ $item->owner_name }} </td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.email')</th>
                                                <td> {{ $item->owner_email }} </td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.purpose_of_visit')</th>
                                                <td> {{ $item->purpose_of_visit }}</td>
                                            </tr>
											
                                            <tr>

                                                <th>@lang('survey.label.status')</th>
                                                <td> @lang('survey.status.'.$item->status)</td>
                                            </tr>
											
											 <tr>

                                                <th>@lang('survey.label.issue_count')</th>
                                                <td> {{ $item->issue_count }} </td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.is_the_sale_law')</th>
                                                <td>@if($item->is_the_sale_law_1973) @lang('common.label.yes')  @else @lang('common.label.no') @endif </td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.is_consultants')</th>
                                                <td> @if($item->is_consultants) @lang('common.label.yes') @else @lang('common.label.no')  @endif </td>
                                            </tr>
											
											 <tr>

                                                <th>@lang('survey.label.created')</th>
                                                <td> {{ $item->created_tz }} </td>
                                            </tr>
											
											
											 <tr>

                                                <th>@lang('survey.label.surveyor_name')</th>
                                                <td> @if($item->surveyor) {{ $item->surveyor->email }}  @endif</td>
                                            </tr>
											
											
											 <tr>

                                                <th>@lang('survey.label.address')</th>
                                                <td> <pre>{{ $item->address }} </pre></td>
                                            </tr>
											
											<tr>

                                                <th>@lang('issue.label.issue_images')</th>
                                                <td> 
													@if($item->refefile->count())
															<div class="row">
															@foreach($item->refefile as $rf)
															@if($rf->file_thumb_url && $rf->file_thumb_url != "")
															<div class="col-sm-2 relative-container">
																<a href="{{url('admin/reference-file/'.$rf->id.'/delete')}}" onclick="return confirm('@lang('common.js_msg.confirm_for_delete',['item_name'=>trans('common.label.file')])')" class="close btn btn-danger btn-sm"><i class='fa fa-trash' aria-hidden='true'></i></a>
																<a class="example-image-link " href="{!! $rf->file_url !!}" data-lightbox="example-2" data-title="{{$rf->refe_file_real_name}}">
																<img src="{!! $rf->file_thumb_url !!}" class="pull-right" height="80" />
																</a>
															</div>
															
															@endif
															@endforeach
															</div>
															<br/>
															@endif
												</td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.property_desc')</th>
                                                <td> <pre> {{ $item->property_desc }} </pre> </td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.legal_thing_a')</th>
                                                <td> <pre> {{ $item->legal_thing_a }} </pre> </td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.legal_thing_isa')</th>
                                                <td> <pre> {{ $item->legal_thing_isa }} </pre> </td>
                                            </tr>
											
											
											<tr>

                                                <th>@lang('survey.label.reviews_and_summary')</th>
                                                <td> <pre>{{ $item->reviews_and_summary }} </pre> </td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.not_tested')</th>
                                                <td><pre> {{ $item->not_tested }} </pre> </td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.general_info')</th>
												<td>
												@php( $general_info = str_replace(trans('survey.label.general_info_text2'),"",$item->general_info) )
													<pre> {{ $general_info }} </pre>
												<p>@if($item->is_consultants) 
													@lang('survey.label.general_info_text2')
												@endif</p>
												
												</td>
                                            </tr>
											
											<tr>

                                                <th>@lang('survey.label.other_info')</th>
                                                <td>
													@php( $other_info = str_replace(trans('survey.label.general_info_text1'),"",$item->other_info) )
													<pre>{{ $other_info }} </pre>
												<p>@if($item->is_the_sale_law_1973) 
													@lang('survey.label.general_info_text1')
												@endif</p>								
												</td>
												
                                            </tr>
											
											
											
											
											
											
											
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>

@include ('admin.survey.assignmodel',['isReload'=>1])


@endsection


     