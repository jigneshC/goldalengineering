<div class="modal fade text-left the_assign_modal" id="assign_modal"  role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel34">@lang('survey.label.local_sync')</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['url' => 'admin/survey-local-sync', 'class' => 'form-horizontal assign_form', 'files' => true]) !!}
{!! Form::token() !!}
            {!! Form::hidden('survey_id',0, ['class' => 'form-control','id'=>'survey_id']) !!}
            <div class="modal-body">
                <label>@lang('user.label.select_surveyor') </label>
                <div class="form-group position-relative">
					<input type="text" name="surveyor_id" class=" " id="assign_user_id" style="width: 100%;height: 32px;margin: 0 !important;">
                  {!! $errors->first('surveyor_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="modal-footer">
               
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.submit'), ['class' => 'btn btn-light']) !!}
				 <button type="button" class="btn btn-primary" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('common.label.cancel')</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')

<script>

   
/***************action model******************/
	var s2 = null;
    var _model = "#assign_modal";
        $(document).on('click', '.to_local', function (e) {
            var id=$(this).attr('data-id');
            var user_id=$(this).attr('data-uid');
            var surveyor_id=$(this).attr('data-surveyor-id');
            var surveyor_name=$(this).attr('data-surveyor-name');
			$("#survey_id").val(id);
            $("#assign_user_id").select2('val', '');
           // $(_model).modal('show');
			s2.select2('data', {id:surveyor_id, text:surveyor_name});
			var r = confirm("@lang('common.js_msg.confirm_for_local_sync')");
			if (r == true) {
				$('.assign_form').submit();
			}
            return false;
        });
	$(document).ready(function(){ 
		 s2 = $("#assign_user_id").select2({
			dropdownParent: $(".the_assign_modal"),
			placeholder: "@lang('user.label.select_surveyor')",
			allowClear: true,
			ajax: {
				url: "{{ url('admin/users/search') }}",
				dataType: 'json',
				type: "GET",
				data: function (params) {
					return {
						filter_user: params, // search term
					};
				},
				results: function (result) {
					var data = result.data;
					return {
						results: $.map(data, function (obj) {
							return {id: obj.id, text: obj.full_name};
						})
					};
				},
				cache: false
			}
		});
		
		
	});


    $('.assign_form2').submit(function(event) {


        var formData = {
            'survey_id': $("#survey_id").val(),
            'surveyor_id': $("#assign_user_id").val(),
        };

        $.ajax({
            type: "POST",
            url: "{{ url('admin/survey-local-sync') }}",
            data: formData,
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function (data) {

                $(_model).modal('hide');
                @if($isReload == 0)
                    datatable.fnDraw(false);
                    toastr.success('Action Success!', data.message);
                @else
					toastr.success('Action Success!', data.message);
					window.setTimeout(function(){ window.location.reload() },3000)
                    
                @endif
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
</script>
@endpush