<?php

\App::setLocale('he');

$logo = url('public/assets/images/logo.png');
$name = "";

if($item->branch && $item->branch->refeimg && $item->branch->refeimg->file_url != ""){
	$logo = $item->branch->refeimg->file_url;
	$name = $item->branch->name;
}
		
$rtexts = \App\Responcetext::where("slug","other_info_image_1")->get();


$sign_url = "";
if($item->surveyor && $item->surveyor->signature){
	$sign_url = $item->surveyor->signature->file_url;
}else{
	$rf_sign = \App\Responcetext::where("slug","signature")->first();
	
	if($rf_sign && $rf_sign->refefile &&  $rf_sign->refefile->file_url != ""){
		$sign_url = $rf_sign->refefile->file_url;
	}
}		

$survey_date = \Carbon\Carbon::parse($item->survey_date)->format("d/m/Y");
if(isset($item) && $item->parent_id && $item->parent_id >0 && $item->survey_date_option){
	$survey_date = \Carbon\Carbon::parse($item->created_at)->format("d/m/Y");	
}
?>


<html lang="he" class="loading" dir="rtl" >
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    


    <style>
        
body {
    font-family: DejaVu Sans;
}



    </style>
</head>
<body>

<div class="card-body card-white-board card-center" style="max-width: 842px; margin: 0 auto;">
                    <div class="card-block"  >
						
						<div class="topfixed-header-board clearfix" style="text-align:center;">
							<div class="logo-thumb-div" style="margin-bottom: 15px;">
								<img alt="Logo" class="logo-img01" height="100" src="{{$logo}}" />
							</div>
						</div>
						@if($item->branch)
						<div class="card-content" style="margin-b">
							<p style="width: 100%;">
							
							<span style="padding: 0 10px; display: inline-block; width: 49%; text-align: left;">{{ $item->branch->title_2 }}</span>
							<span style=" text-align:right; width: 49%; padding: 0 10px; display: inline-block;">{{ $item->branch->title_1 }}</span>
							</p>
							<p style="width: 100%;">
							
							<span style="padding: 0 10px; display: inline-block; width: 49%; text-align: left;"> {{$item->branch->phone_2}}</span>
							<span style=" text-align:right; width: 49%; padding: 0 10px; display: inline-block;"> {{ $item->branch->phone_1 }}</span>
							</p>
							
							
							<p style="width: 100%;"> 
							<span style="padding: 0 10px; display: inline-block; width: 49%; text-align: left;">{{ $item->branch->email_2 }}</span>
							<span style=" text-align:right; width: 49%; padding: 0 10px; display: inline-block;"> {{$item->branch->email_1 }}</span>
							</p>
						</div>
						@endif
						<div class="date-and-to" style="display: flex; justify-content: space-between; flex-direction: row; margin-top: 15px;">
							<div class="to-content">
							<p><strong>@lang('survey.label.owner_name') : <a href="{{url('/admin/survey/'.$item->id.'/edit')}}"> {{$item->owner_name}} </a></strong></p>
							<p>מייל : <a href="{{url('/admin/survey/'.$item->id.'/edit')}}">{{$item->owner_email}} </a></p>
							<p>טלפון : <a href="{{url('/admin/survey/'.$item->id.'/edit')}}">{{$item->owner_phone}} </a> </p>
							

							<p style="margin-t0p: 20px;">@lang('report.label.report_above_header')</p>
							</div>
							
							<div class="date-content">
							<p><strong>@lang('survey.label.created') :  <a href="{{url('/admin/survey/'.$item->id.'/edit')}}"> {{ \Carbon\Carbon::now()->format("d/m/Y") }} </a> </strong></p>
							</div>
							
							
						</div>

						<div class="reports-title">
							<h3 style="font-size: 30px; text-align: center; font-weight: 600; color: #004d73; text-decoration: underline; margin-bottom: 30px;"><a href="{{url('/admin/survey/'.$item->id.'/edit')}}">
							הנדון:</span> ליקויים בניה ב{{$item->address}} <span style="display:block">@lang('survey.label.professional_opinion')</span></a></h3>
							
						</div>
						<div style="report-content;text-align: right;" >
						<ul style="list-style: none; text-align: right;">
							<li style="text-align: right; direction: rtl">@lang('report.declartaion.l1')</li>
							<li style="text-align: right; ">@lang('report.declartaion.l2',["name"=>$item->owner_name])</li>
							<li style="text-align: right; ">@lang('report.declartaion.l3',["date"=>$survey_date])</li>
							<li style="text-align: right; ">@lang('report.declartaion.l4')</li>
							<li style="text-align: right; ">@lang('report.declartaion.l5')</li>
							<li style="text-align: right; ">@lang('report.declartaion.l6')</li>
							
							<li style="text-align: right">
										<p style="text-align: right;">	@lang('report.declartaion.l6_a')</p>
										<ul style="list-style: disc !important; text-align: right;">
										
										<li style="text-align: right; padding-right: 15px;">
											@lang('report.declartaion.l6_a_1') 
											<br/> 
											@lang('report.declartaion.l6_a_2') </li>
										</ul>
							</li>
							<li style="text-align: right">
										<p style="text-align: right;">	@lang('report.declartaion.l6_b') </p>
										<ul style="text-align: right; list-style: disc !important;">
											<li style="text-align: right; padding-right: 15px;">
											@lang('report.declartaion.l6_b_1') <br/>
											@lang('report.declartaion.l6_b_2') <br/>
											@lang('report.declartaion.l6_b_3') <br/>
											</li>
											<li style="text-align: right; padding-right: 15px;">@lang('report.declartaion.l6_b_4')</li>					
										</ul>
							</li>

							<li style="text-align: right; ">@lang('report.declartaion.l7')
								<br/>
								&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; @lang('report.declartaion.l7_a')
							</li>
							<li style="text-align: right; ">@lang('report.declartaion.l8')

									<ul style="list-style: none !important; display: inline-block; width: 100%;">
										<li style="text-align: right; padding-right: 15px; width: 50%; float: left; ">@lang('report.declartaion.l8_r1_b')  &nbsp;&nbsp;&nbsp;</li>
										<li style="text-align: right; padding-right: 15px; width: 50%; float: left; "> @lang('report.declartaion.l8_r1_a') &nbsp;&nbsp;&nbsp;</li>
										<li style="text-align: right; padding-right: 15px; width: 50%; float: left; "> @lang('report.declartaion.l8_r2_b')  &nbsp;&nbsp;&nbsp;</li>
										<li style="text-align: right; padding-right: 15px; width: 50%; float: left; "> @lang('report.declartaion.l8_r2_a')  &nbsp;&nbsp;&nbsp;</li>
										<li style="text-align: right; padding-right: 15px; width: 50%; float: left; "> @lang('report.declartaion.l8_r3_b')  &nbsp;&nbsp;&nbsp;</li>
										<li style="text-align: right; padding-right: 15px; width: 50%; float: left; "> @lang('report.declartaion.l8_r3_a')  &nbsp;&nbsp;&nbsp;</li>
										<li style="text-align: right; padding-right: 15px; width: 50%; float: left; ">  @lang('report.declartaion.l8_r4_b')  &nbsp;&nbsp;&nbsp;</li>
										<li style="text-align: right; padding-right: 15px; width: 50%; float: left; ">  @lang('report.declartaion.l8_r4_a')  &nbsp;&nbsp;&nbsp;</li>
									</ul>
							</li>

							<li style="text-align: right; ">@lang('report.declartaion.l9')
							
							@foreach($rtexts as $rd)
								@php($rf = $rd->refefile)
								@if($rf && $rf->file_url != "")
									<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px;">
										<img src="{{$rf->file_url}}" alt="img-tbale" width="100%;">
									</div>
								@endif
							@endforeach
							
							<h3 style="font-weight: 500; font-size: 18px;"><a href="{{url('/admin/survey/'.$item->id.'/edit')}}">@lang('survey.label.general_info')</a></h3>
							<pre style="white-space: pre-wrap;">{{$item->general_info}}</pre>
								
								<p style="font-weight: 600; font-size: 16px;"><a href="{{url('/admin/survey/'.$item->id.'/edit')}}">@lang('survey.label.property_desc')</a></p>
								<pre style="white-space: pre-wrap;">{{$item->property_desc}}</pre>


								<p style="font-weight: 600; font-size: 16px; "><a href="{{url('/admin/survey/'.$item->id.'/edit')}}">@lang('survey.label.legal_thing_isa')</a></p>
								<pre style="white-space: pre-wrap;">{{$item->legal_thing_isa}}</pre>
								<pre style="white-space: pre-wrap;">{{$item->other_info}}</pre>

							<?php  $rtexts = \App\Responcetext::where("slug","other_info_image_2")->get(); ?>

							@foreach($rtexts as $rd)
								@php($rf = $rd->refefile)
								@if($rf && $rf->file_url != "")
									<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px;">
										<img src="{{$rf->file_url}}" alt="img-tbale" width="100%;">
									</div>
								@endif
							@endforeach
							
							<?php  $rtexts = \App\Responcetext::where("slug","other_info_image_3")->get(); ?>

							@foreach($rtexts as $rd)
								@php($rf = $rd->refefile)
								@if($rf && $rf->file_url != "")
									<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px;">
										<img src="{{$rf->file_url}}" alt="img-tbale" width="100%;">
									</div>
								@endif
							@endforeach
								
							<?php $rd = \App\Responcetext::where("slug","report_focus_note")->first(); ?>	
								@if($rd)
								<p style="font-weight: 600; font-size: 16px; margin-top: 15px;">@lang('constant.slug.report_focus_note')</p>
								
								<pre style="white-space: pre-wrap;">{{$rd->desc}}</pre>

								@endif
							</li>

						</ul>		
							<h3 style="font-weight: 600; margin-bottom: 15px; margin-top: 15px; text-align: center;">@lang('report.label.findings')</h3>
							
							<?php
							
							$cat_id = 0;
							$cat_cntr = 0;
							$cat_sub_cntr = 0;
							
							$grp_location = "";
							$total_cost = 0;
							$cat_cost = [];
							$sub_cat_cost = [];
							
							$cat_group = array();
							foreach($item->issue as $k => $issue){
								
								if(!isset($cat_group[$issue->category_id]) && isset($issue->category)){
									$cat_group[$issue->category_id] = ['name'=>$issue->category->name,'items'=>[]];
								}
								
								if(isset($cat_group[$issue->category_id]['items'][$issue->child_category_id]) && isset($issue->subcategory)){
									$cat_group[$issue->category_id]['items'][$issue->child_category_id]['items'][] = $issue;
								}else if(isset($issue->subcategory)){
									$cat_group[$issue->category_id]['items'][$issue->child_category_id]['name'] = $issue->subcategory->name;
									$cat_group[$issue->category_id]['items'][$issue->child_category_id]['items'][] = $issue;
								}
							}
							
							$k = 9;
							$d = 0;
							$e = 0;
							foreach($cat_group as  $grp){
								$k++;
								?>
								<p style="text-align: right;  font-weight: 600; ">
								{{$k}}.  <span style="text-decoration: underline; margin-right:12px;">  {{$grp['name']}}</span>
								</p>
								<?php
								$d=0;
								foreach($grp['items'] as  $gr){
									$d++;
									$e = 0;
									$total_issue_c = count($gr['items']);
									foreach($gr['items'] as  $issue){
										$e++;
										$otherJson = json_decode($issue->other_json_data,true);
										?>
										@if($e == 1 && $issue->issue_detail && $issue->issue_detail != "")
											 <span style="margin-right:30px;font-weight: 600;">{{$k}}.{{$d}}. </span> 
											<p style="margin-right:30px;font-weight: 600;"> <a href="{{url('/admin/issue/'.$issue->id.'/edit')}}"> {{$issue->issue_detail}} </a></p>
										@endif
										
										 <span style="margin-right:30px;"> {{$k}}.{{$d}}.{{$e}}. <a href="{{url('/admin/issue/'.$issue->id.'/edit')}}">{{$issue->location}} </a></span> 
										 
										@if($issue->refefile->count())
						
											@foreach($issue->refefile as $rf)
												@if($rf->file_url && $rf->file_url != "")
												<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px;">
												<img src="{{$rf->file_url}}" alt="img-tbale" width="100%;">
												</div>
												@endif
											@endforeach
											
										@endif
										
										<p style="margin-right:30px;"> {{$issue->note}} </p>
										<p style="margin-right:30px;font-weight: 600;"><a href="{{url('/admin/issue/'.$issue->id.'/edit')}}"> {{$k}}.{{$d}}.{{$e}}.  @lang('issue.label.total_of_the_issue',['place'=>$issue->location]) : {{$issue->total_cost}} ש"ח  @if(isset($otherJson['flag_show_unit_price_in_report'])) ( {{$issue->unit_cost}}X{{$issue->number_of_unit}}) @endif</a></p>
										
										
										
										<?php 
										$total_cost = $total_cost + $issue->total_cost;
					
										if(isset($cat_cost[$issue->category_id]) && isset($cat_cost[$issue->category_id]['cost'])){
											$cat_cost[$issue->category_id]['cost'] = $cat_cost[$issue->category_id]['cost'] + $issue->total_cost;
										}else{
											$cat_cost[$issue->category_id] = ['cost'=>$issue->total_cost,'name'=>$issue->category->name];
										}
										
										if(isset($sub_cat_cost[$issue->child_category_id]) && isset($sub_cat_cost[$issue->child_category_id]['cost'])){
											$sub_cat_cost[$issue->child_category_id]['cost'] = $sub_cat_cost[$issue->child_category_id]['cost'] + $issue->total_cost;
										}else{
											$sub_cat_cost[$issue->child_category_id] = ['cost'=>$issue->total_cost,'name'=>$issue->subcategory->name];
										}
										
										
										if($total_issue_c == $e){
						
											if($issue->subcategory){
												$pluck = $issue->subcategory->refefile->pluck("id");
												if($issue->img_hint && $issue->img_hint != ""){
													?> <p style="margin-right:30px;font-weight: 600;"> ציטוט: </p> <?php
													//$img_hint = json_decode($issue->img_hint,true);
													$img_hint = $issue->hintImages();
													$imgc = 0;
													foreach($img_hint as $z => $hint){
														if(isset($hint['img_id']) && in_array($hint['img_id'],$pluck->toArray())){
															$rf = \App\Refefile::whereId($hint['img_id'])->first();
															if($rf && $rf->file_url && $rf->file_url != ""){
																if($imgc ==0){
																	if($issue->quote && $issue->quote != ""){
																		?> <p style="margin-right:30px;"> {{$issue->quote}}</p> <?php
																	}
																}
																?> 
																<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px;">
																	<img src="{{$rf->file_url}}" alt="img-tbale" width="100%;">
																</div> 
																<?php
																$imgc++;
															}
														}
													}
													
												}
												
											
											}
											if($issue->recommendation && $issue->recommendation != ""){
												?>
												 <span style="margin-right:30px;font-weight: 600;">@lang('issue.label.recommendation') </span> 
													<pre style="margin-right:30px;white-space: pre-wrap;"> {{$issue->recommendation}}</pre>
												<?php
											}
											?>
												<p style="margin-right:30px;font-weight: 600;"><a href="{{url('/admin/issue/'.$issue->id.'/edit')}}"> {{$k}}.{{$d}}.  @lang('issue.label.total_cost') : {{$sub_cat_cost[$issue->child_category_id]['cost']}} ש"ח  </a></p>
											<?php
										}
										?>
										
										
										
										<?php
									}
								}
							}	
					
							$supervision_charge = 10;
							$gdp_charge = 15;
							$vat_charge = 17;
							
							$gdp_per = round($gdp_charge*$total_cost/100);
							$supervision_per = round($supervision_charge*$total_cost/100);
							
							$sub_total = $total_cost + $gdp_per + $supervision_per;
							$vat_per = round($vat_charge*$sub_total/100);
							$net_toal = $vat_per + $sub_total;
							
							$cat_tr = "";
							$index = 1;
							foreach($cat_cost as $k=> $cs){
								$cat_tr.= '<tr><td style="text-align: center; "> '.$cs['cost'].'</td><td style="text-align: center; "> '.$cs['name'].'</td><td style="text-align: center; ">'.$index.'</td></tr>';
								$index++;
							}
							
							$table = '<table style="width: 100%; text-align: center; margin-top: 30px; margin-bottom: 30px;" border="1">'.
            '<thead>'.
                '<tr style="text-align: center; font-weight: bold; ">'.
                    '<th style="width: 33.33%; text-align:center; ">'.trans('survey.label.price').'</th>'.
                    '<th style="width: 33.33%; text-align:center;">'.trans('survey.label.section').'</th>'.
                    '<th style="width: 33.33%; text-align:center;">'.trans('survey.label.tax').'</th>'.
                '</tr>'.
            '</thead>'.
            '<tbody>'.$cat_tr.
                '<tr><td style="text-align: center; "> '.$total_cost.'</td><td style="text-align: center; "> '.trans('survey.label.all_problem_price').'</td><td ></td></tr>'.
                '<tr><td style="text-align: center; "> '.$gdp_per.' </td><td style="text-align: center; "> '.trans('survey.label.gdp')." ".$gdp_charge."%".'</td><td></td></tr>'.
                '<tr><td style="text-align: center; "> '.$supervision_per.'</td><td style="text-align: center; "> '.trans('survey.label.supervision')." ".$supervision_charge."%".'</td><td></td></tr>'.
                '<tr><td style="text-align: center; ">'.$sub_total.'</td><td style="text-align: center; ">'.trans('survey.label.total').'</td><td></td></tr>'.
                '<tr><td style="text-align: center; ">'.$vat_per.'</td><td style="text-align: center; ">'.trans('survey.label.vat')." ".$vat_charge."%".'</td><td></td></tr>'.
                '<tr><th style="text-align: center; font-weight: bold; ">'.number_format(round($net_toal)).'</th><th style="text-align: center; font-weight: bold; ">'.trans('survey.label.total_payment_including_VAT').'</th><td></td></tr>'.
            '</tbody>'.
         '</table>';;
							?>

							


							<h3 style="font-weight: 600; font-size: 18px; margin-top: 20px; margin-bottom: 15px; text-align: center;">@lang('survey.label.estimated_repair_costs')</h3>

							{!! $table !!}


								<p style="font-weight: 600;">@lang('survey.label.reviews_and_summary')</p>
								<pre style="margin-right:30px;white-space: pre-wrap;"> {{$item->reviews_and_summary}}</pre>


								@if($item->checklist && $item->checklist != "")
									@php( $checklist = json_decode($item->checklist,true) )
									@if($checklist && count($checklist) > 0)
										@foreach($checklist as $chl)
											
											<p style="font-weight: 600;">{{ $chl['title'] }}</p>
											<pre style="margin-right:30px;white-space: pre-wrap;"> {{ $chl['desc'] }}</pre>
										@endforeach
									@endif
								@endif
								<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px; text-align: left;">
									<img src="{{$sign_url}}" alt="img-tbale" height="100" width="auto;">
								</div>


							


						
						</div>

					</div>
				</div>	
				
</body>

</html>
