

<div class="row ">


    <div class="col-md-12">

		
		
		<div class="form-group">
            <label for="purpose_of_visit" >
                @lang('survey.label.purpose_of_visit')
            </label>
            <div >
                {!! Form::textarea('purpose_of_visit', old('purpose_of_visit',null) , ['id'=>'purpose_of_visit','class' => 'form-control', 'rows' => '3']) !!}
            </div>
        </div>
		
		<div class="form-group {{ $errors->has('document_title') ? 'has-error' : ''}}">
            <label for="document_title" class="">
				@lang('report.label.document_title') 
			</label>
			 
			 
			 @php
			 
				if(old('document_title')){
					$selected = old('document_title');
				}elseif(isset($item)){
					 $selected = $item->document_title;
				}else{
					 $selected = "";
				}
			 
				$rts = \App\Responcetext::where("responce_type","document_title")->get();
			 @endphp
			<select class="form-control" id="document_title" name="document_title">
			@foreach($rts as $k => $val)
                 <option value="{{$val->title}}" {{ ($val->title == $selected ) ? 'selected':'' }} >{{$val->title}}</option>
            @endforeach
			</select>
			<a href="#" class="add_title_open" >@lang('common.label.add_new')</a>
			{!! $errors->first('document_title', '<p class="help-block text-danger">:message</p>') !!}
		</div>
		@if(isset($item) && $item->parent_id && $item->parent_id >0)
		<div class="row ">
		<div class="col-md-6">
		<div class="form-group{{ $errors->has('survey_date') ? ' has-error' : ''}}">
			<label for="name" >
			<span class="field_compulsory"></span>
				@lang('survey.label.survey_date')
			</label>
			<div>
				{!! Form::date('survey_date', \Carbon\Carbon::parse($item->survey_date)->format("Y-m-d"), ['id'=>'survey_date','class' => 'form-control']) !!}
				{!! $errors->first('survey_date', '<p class="help-block text-danger">:message</p>') !!}
			</div>
		</div>
		</div>
		<div class="col-md-6">
		<div class="form-group{{ $errors->has('survey_date') ? ' has-error' : ''}}">
			<label for="name" >
			<span class="field_compulsory"></span>
				@lang('survey.label.duplicate_date')
			</label>
			<div>
				{!! Form::date('survey_dup_date', \Carbon\Carbon::parse($item->created_at)->format("Y-m-d"), ['id'=>'survey_dup_date','class' => 'form-control']) !!}
				
			</div>
		</div>
		</div>
		</div>
		
		<div class="form-group {{ $errors->has('survey_date_option') ? 'has-error' : ''}}">
            <label for="survey_date_option" class="">
                @lang('survey.label.survey_date_option') 
            </label>
			<input type="checkbox" name="survey_date_option" class=" "  @if(isset($item) && $item->survey_date_option ) checked  @endif id="survey_date_option" value='1' >
		</div>
		@endif
		
        <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
            <label for="status" class="">
                <span class="field_compulsory">*</span>@lang('survey.label.status')
            </label>
			{!! Form::select('status',trans('survey.status'), null,['id'=>'status','class' => 'form-control']) !!}
			{!! $errors->first('status', '<p class="help-block text-danger">:message</p>') !!}
		</div>

		<div class="form-group {{ $errors->has('surway_logo') ? 'has-error' : ''}}">
            <label for="last_name" class="">
                <span class="surway_logo">*</span>@lang('survey.label.surway_logo')
            </label>
			{!! Form::select('surway_logo',$branches, null,['id'=>'surway_logo','class' => 'form-control']) !!}
			{!! $errors->first('surway_logo', '<p class="help-block text-danger">:message</p>') !!}
		</div>

		<div class="form-group {{ $errors->has('is_the_sale_law_1973') ? 'has-error' : ''}}">
            <label for="is_the_sale_law_1973" class="">
                @lang('survey.label.is_the_sale_law_1973') 
            </label>
			<input type="checkbox" name="is_the_sale_law_1973" class="switchery "  @if(isset($item) && $item->is_the_sale_law_1973 ) checked  @endif id="is_the_sale_law_1973" value='1' >
			{!! $errors->first('is_the_sale_law_1973', '<p class="help-block text-danger">:message</p>') !!}
		</div>
		
		<div class="form-group {{ $errors->has('is_consultants') ? 'has-error' : ''}}">
            <label for="is_consultants" class="">
                @lang('survey.label.is_consultants') 
            </label>
			<input type="checkbox" name="is_consultants" class="switchery "  @if(isset($item) && $item->is_consultants ) checked  @endif id="is_consultants" value='1' >
			{!! $errors->first('is_consultants', '<p class="help-block text-danger">:message</p>') !!}
		</div>
		
		<div class="form-group {{ $errors->has('enable_court') ? 'has-error' : ''}}">
            <label for="enable_court" class="">
                @lang('survey.label.enable_court') 
            </label>
			<input type="checkbox" name="enable_court" class="switchery "  @if(isset($item) && $item->enable_court ) checked  @endif id="enable_court" value='1' >
			{!! $errors->first('enable_court', '<p class="help-block text-danger">:message</p>') !!}
		</div>
		
		<div class="row cort_option_div @if(isset($item) && $item->enable_court ) @else hide  @endif">
			<div class="col-md-4">
				<div class="form-group {{ $errors->has('case_number') ? 'has-error' : ''}}">
					<label for="case_number" class="">
						@lang('report.label.case_number') 
					</label>
					{!! Form::text('case_number', null, ['id'=>'case_number','class' => 'form-control']) !!}
					{!! $errors->first('case_number', '<p class="help-block text-danger">:message</p>') !!}
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group {{ $errors->has('name_of_court') ? 'has-error' : ''}}">
					<label for="name_of_court" class="">
						@lang('report.label.name_of_court') 
					</label>
					{!! Form::text('name_of_court', null, ['id'=>'name_of_court','class' => 'form-control']) !!}
					{!! $errors->first('name_of_court', '<p class="help-block text-danger">:message</p>') !!}
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group {{ $errors->has('court_date') ? 'has-error' : ''}}">
					<label for="court_date" class="">
						@lang('report.label.court_date') 
					</label>
					{!! Form::date('court_date', null, ['id'=>'court_date','class' => 'form-control']) !!}
					{!! $errors->first('court_date', '<p class="help-block text-danger">:message</p>') !!}
				</div>
			</div>
		</div>	
		
		<div class="form-group{{ $errors->has('owner_name') ? ' has-error' : ''}}">
			<label for="name" >
			<span class="field_compulsory">*</span>
				@lang('survey.label.name')

			</label>
			<div >
				{!! Form::text('owner_name', null, ['id'=>'owner_name','class' => 'form-control']) !!}
				{!! $errors->first('owner_name', '<p class="help-block text-danger">:message</p>') !!}
			</div>
		</div>

		<div class="form-group{{ $errors->has('owner_email') ? ' has-error' : ''}}">
			<label for="name" >
			<span class="field_compulsory"></span>
				@lang('survey.label.email')

			</label>
			<div>
				{!! Form::text('owner_email', null, ['id'=>'owner_email','class' => 'form-control']) !!}
				{!! $errors->first('owner_email', '<p class="help-block text-danger">:message</p>') !!}
			</div>
		</div>


		{{--
		<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
			<label for="logo"> @lang('common.label.logo_image') </label>
			<div class="">
				@if($item->refefile->count())
				<div class="row">
				@foreach($item->refefile as $rf)
				@if($rf->file_thumb_url && $rf->file_thumb_url != "")
				<div class="col-sm-2 relative-container">
					<a href="{{url('admin/reference-file/'.$rf->id.'/delete')}}" onclick="return confirm('@lang('common.js_msg.confirm_for_delete',['item_name'=>trans('common.label.file')])')" class="close btn btn-danger btn-sm"><i class='fa fa-trash' aria-hidden='true'></i></a>
					<a class="example-image-link " href="{!! $rf->file_url !!}" data-lightbox="example-2" data-title="{{$rf->refe_file_real_name}}">
					<img src="{!! $rf->file_thumb_url !!}" class="pull-right" height="80" />
					</a>
				</div>

				@endif
				@endforeach
				</div>
				<br/>
				@endif
				{!! Form::file('image', null, ['id'=>'survey_image','class' => 'form-control']) !!}
				{!! $errors->first('image', '<p class="help-block text-danger">:message</p>') !!}
			</div>
		</div>
		--}}

		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.address')
            </label>
            <div >
                {!! Form::textarea('address', old('address',null) , ['id'=>'address','class' => 'form-control']) !!}
				{!! $errors->first('address', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>

		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.property_desc')
            </label>
			@include ('admin.survey.textToCheckbox', ['slug_s' => 'property_desc','slug_c' => 'property_description'])
		</div>



		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.legal_thing_a')
            </label>
			@include ('admin.survey.textToCheckbox', ['slug_s' => 'legal_thing_a','slug_c' => 'legal_thing_a'])
		</div>

		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.legal_thing_isa')
            </label>
			@include ('admin.survey.textToCheckbox', ['slug_s' => 'legal_thing_isa','slug_c' => 'legal_thing_isa'])
        </div>

		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.surveyor_note')
            </label>
            <div >
				@include ('admin.survey.textToCheckbox', ['slug_s' => 'surveyor_note','slug_c' => 'surveyor_note'])
                {!! $errors->first('surveyor_note', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>

		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.reviews_and_summary')
            </label>
            <div >
				@include ('admin.survey.textToCheckbox', ['slug_s' => 'reviews_and_summary','slug_c' => 'reviews_and_summary'])
                {!! $errors->first('reviews_and_summary', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>

		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.not_tested')
            </label>
            <div >
				@include('admin.survey.textToCheckbox', ['slug_s' => 'not_tested','slug_c' => 'not_tested'])
            </div>
        </div>



		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.general_info')
            </label>
			@include ('admin.survey.textToCheckbox', ['slug_s' => 'general_info','slug_c' => 'general_info'])
		</div>


        <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.other_info')
            </label>
            <div >
				@include ('admin.survey.textToCheckbox', ['slug_s' => 'other_info','slug_c' => 'other_info'])
                <p id="text_general_info1">@lang('survey.label.general_info_text1')</p>
				{!! $errors->first('other_info', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>
		@if(request()->has('force'))
	<div class="pull-right">
		<label for="is_deleted_record" class="font-medium-2 text-bold-600 ml-1 ">@lang('common.label.deleted_decord')</label>
		<input type="checkbox" id="is_deleted_record" class="switchery" />
	
	</div>
@endif
		<?php
			$default_material = 0;
			if(request()->has('default_material')){
				$default_material = 1;
			}
			$default_accessories = 0;
			if(request()->has('default_accessories')){
				$default_accessories = 1;
			}
			
			$selected_q = [];
			$selected_pm = [];
			$other_information_a = "";
			$other_information_b = "";
			if(isset($item)){
				foreach($item->getaccessories($default_accessories) as $val){
					if($val->desc && $val->desc != ""){
						$selected_q[] = $val->desc;
					}
				}
				foreach($item->getpfmaterials($default_material) as $val){
					if($val->desc && $val->desc != ""){
						$selected_pm[] = $val->desc;
					}
				}
				if($item->getspinfoa()){
					$other_information_a = $item->getspinfoa()->desc;
				}
				if($item->getspinfob()){
					$other_information_b = $item->getspinfob()->desc;
				}
			}

			if(count($selected_q) <=0){ $selected_q = [""]; }
			if(count($selected_pm) <=0){ $selected_pm = [""]; }




		?>

		<div class="multi-group {{ $errors->has('accessories') ? 'has-error' : ''}}">
			@foreach($selected_q as $k => $q)
			@if($k%2 == 0)
			<div class="form-group ">
				<label for="password" class="title">
				   @lang('survey.label.accessories')
				</label>
				 <label  class="action_btn pull-right">
				   <a href="#" class="add_row" ><i style="padding: 8px;background-color: black;border-radius: 50%;" class='fa fa-plus'></i></a>
				   <a href="#" class="remove_row" ><i style="padding: 8px;background-color: black;border-radius: 50%;" class='fa fa-minus'></i></a>
				</label>
				<div class="row">
				<div class="col-md-6">
				{!! Form::text('accessories[]', $q,['placeholder'=>trans('survey.label.accessories'),'class' => 'form-control']) !!}
				</div>
				<div class="col-md-6">
				{!! Form::text('accessories[]', (isset($selected_q[$k+1]))? $selected_q[$k+1] : "",['placeholder'=>trans('survey.label.accessories'),'class' => 'form-control']) !!}
				</div>
				</div>

			</div>
			@endif
			@endforeach
			
			@if((count($selected_q) <=0 || $selected_q[0] =="") && !request()->has('default_accessories'))
				<p> <a href="{{request()->fullUrlWithQuery(['default_accessories'=>'1'])}}"> @lang('survey.label.default_list') </a></p>
			@endif

			{!! $errors->first('accessories', '<p class="help-block text-danger">:message</p>') !!}
		</div>

		<div class="multi-group {{ $errors->has('professional_materials') ? 'has-error' : ''}}">
			@foreach($selected_pm as $k => $q)

			<div class="form-group ">
				<label for="password" class="title">
				   @lang('survey.label.professional_materials')
				</label>
				 <label  class="action_btn pull-right">
				   <a href="#" class="add_row" ><i style="padding: 8px;background-color: #7235a2;border-radius: 50%;" class='fa fa-plus'></i></a>
				   <a href="#" class="remove_row" ><i style="padding: 8px;background-color: #7235a2;border-radius: 50%;" class='fa fa-minus'></i></a>
				</label>
				<div class="row">
				<div class="col-md-12">
				{!! Form::text('professional_materials[]', $q,['placeholder'=>trans('survey.label.professional_materials'),'class' => 'form-control']) !!}
				</div>
				</div>

			</div>

			@endforeach
			
			@if((count($selected_pm) <=0 || $selected_pm[0]=="") && !request()->has('default_material'))
				<p> <a href="{{request()->fullUrlWithQuery(['default_material'=>'1'])}}"> @lang('survey.label.default_list') </a></p>
			@endif

			{!! $errors->first('professional_materials', '<p class="help-block text-danger">:message</p>') !!}
		</div>

		<div class="form-group{{ $errors->has('other_information_a') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.other_information_a')
            </label>
            <div >

                {!! Form::textarea('other_information_a', ($other_information_a)? $other_information_a : null , ['id'=>'other_information_a','class' => 'form-control']) !!}
				{!! $errors->first('other_information_a', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>
		<div class="form-group{{ $errors->has('other_information_b') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.other_information_b')
            </label>
            <div >
                {!! Form::textarea('other_information_b', ($other_information_b)? $other_information_b : null , ['id'=>'other_information_b','class' => 'form-control']) !!}
				{!! $errors->first('other_information_b', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>
		
		



        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('common.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>


    </div>


</div>

@push('js')
<script src="https://cdn.ckeditor.com/4.10.1/standard/ckeditor.js"></script>
<script>

	//CKEDITOR.replace( 'general_info' );

	$(document).on('click', '.add_row', function (e) {
        var rootc = $(this).parents('.multi-group');
        var rowdata = $(this).parents('.form-group');
		$(rootc).append("<div class='form-group'>"+rowdata.html()+"</div>");
		$(rootc).children(".form-group").last().find("input[type=text]").val("");
		return false;
    });

	$(document).on('click', '.remove_row', function (e) {
        var rowdata = $(this).parents('.form-group');
		rowdata.remove();
		return false;
    });

	var isChecked1 = $('input[name="is_the_sale_law_1973"]').prop("checked");
	if (isChecked1) {
		$("#text_general_info1").css('display', 'block');
	} else {
		$("#text_general_info1").css('display', 'none');
	}
	$(document).on('change', 'input[name="is_the_sale_law_1973"]', function (e) {
		var isChecked1 = $(this).prop("checked");
		if (isChecked1) {
			$("#text_general_info1").css('display', 'block');
		} else {
			$("#text_general_info1").css('display', 'none');
		}
	});

	var isChecked = $('input[name="is_consultants"]').prop("checked");
	if (isChecked) {
		$("#text_general_info2").css('display', 'block');
	} else {
		$("#text_general_info2").css('display', 'none');
	}
	$(document).on('change', 'input[name="is_consultants"]', function (e) {
		var isChecked = $(this).prop("checked");
		if (isChecked) {
			$("#text_general_info2").css('display', 'block');
		} else {
			$("#text_general_info2").css('display', 'none');
		}
	});
	
	$(document).on('change', 'input[name="enable_court"]', function (e) {
		var isChecked = $(this).prop("checked");
		if (isChecked) {
			$(".cort_option_div").removeClass('hide');
		} else {
			$(".cort_option_div").addClass('hide');
		}
	});
 </script>

@endpush

