@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('survey.label.survey'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('survey.label.survey') </div>
        {{--  @include('partials.page_tooltip',['model' => 'user','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                        <div class="col-6">
						  <input type="text" name="filter_user" class="filter form-control" id="filter_user" style="max-width: 300px;">
                            
						</div>
						<div class="col-6">
                          
                            @include("admin.filter_deleted") 
							
						</div>
                    </div>
                </div>
				
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        <div style="max-width: 100% !important">
                        
                            <table class="table table-striped table-bordered base-style  responsive datatable" cellspacing="0" >
                            <thead>
                            <tr>
                                <th data-priority="1"># @lang('common.label.id') </th>
                                <th data-priority="2" >@lang('survey.label.name')</th>
                                <th data-priority="3">@lang('survey.label.email')</th>
                                <th data-priority="5">@lang('survey.label.address')</th>
                                <th data-priority="6">@lang('survey.label.issue_count')</th>
                                <th data-priority="7">@lang('survey.label.status')</th>
                                <th data-priority="8">@lang('survey.label.created')</th>
                                <th data-priority="9">@lang('survey.label.last_sync_at')</th>
                                <th data-priority="4">@lang('common.label.action')</th>
                              
                            </tr>
                            </thead>

                        </table>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

@include ('admin.survey.assignmodel',['isReload'=>0])
@endsection




@push('js')
<script>

	var statues = <?php echo json_encode(trans('survey.status')); ?>;
	
	
	$("#filter_user").select2({
        placeholder: "@lang('user.label.select_surveyor')",
        allowClear: true,

        @if(isset($surveyor) && $surveyor)
            initSelection: function (element, callback) {
                callback({id: {{$surveyor->id}}, text: "{{$surveyor->full_name}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif

        ajax: {
            url: "{{ url('admin/users/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_user: params, // search term
                };
            },
            results: function (result) {
                var data = result.data;
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.full_name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);
	
	
	var url ="{{ url('/admin/survey') }}";
    var edit_url = "{{ url('/admin/survey') }}";
    var auth_check = "{{ Auth::check() }}";
		
	var auth_uid = {{\Auth::user()->id}};
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('common.datatable.emptyTable')",
            "infoEmpty":"@lang('common.datatable.infoEmpty')",
            "search": "@lang('common.datatable.search')",
            "sLengthMenu": "@lang('common.datatable.show') _MENU_ @lang('common.datatable.entries')",
            "sInfo": "@lang('common.datatable.showing') _START_ @lang('common.datatable.to') _END_ @lang('common.datatable.of') _TOTAL_ @lang('common.datatable.small_entries')",
            paginate: {
                next: '@lang('common.datatable.paginate.next')',
                previous: '@lang('common.datatable.paginate.previous')',
                first:'@lang('common.datatable.paginate.first')',
                last:'@lang('common.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
		responsive: true,
        order: [0, "DESC"],
        columns: [
                { 
					"data": null,
					"name":"id",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						var content = o.id;
						if( o.parent_id && o.parent_id != "" && o.parent_id > 0){
							content = content + " <br/> @lang('survey.label.duplicate_from') <a href='"+edit_url+"/"+o.parent_id+"?hashurl=total_cost_"+o.id+"'> "+ o.parent_id + "</a>&nbsp;";
						}else if( o.merge_ids && o.merge_ids != ""){
							var res = o.merge_ids.split(",");
							content = content + " <br/> @lang('survey.label.merge_from') ";
							for(var i=0;i<res.length;i++){
								content = content + "<a href='"+edit_url+"/"+res[i]+"'> "+ res[i] + "</a>&nbsp;";
								if(i != (res.length-1)){
									content = content + ",";
								}
							}
							
						}
						return content;
					}
				},
                { data: 'owner_name',name : 'owner_name',"searchable": true, "orderable": true},
                { data: 'owner_email',name : 'owner_email',"searchable": true, "orderable": true},
                { 
					"data": null,
					"name":"address",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						var content = o.address;
						if(content && content.length > 80){
							content = content.substr(0, 80) + " ...";
						}
						return "<span class='line-break'>"+content+"</span>";
					}
				},
				{ data: 'issue_count',name : 'issue_count',"searchable": false, "orderable": true},
				{ 
					"data": null,
					"name":"status",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						return (o.status in statues) ? statues[o.status] : o.status;
					}
				},
				{ 
					"data": null,
					"name":"created_at",
					"searchable": false,
					"orderable": true,
					"render": function (o) {
						return o.created_a;
					}
				},
				{ 
					"data": null,
					"name":"last_sync_at",
					"searchable": false,
					"orderable": true,
					"render": function (o) {
						return o.last_sync_at_a;
					}
				},
				{
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "" ; var ex = "";
						
						ex = "<a href='"+edit_url+"/"+o.id+"/export' value="+o.id+" data-id="+o.id+" class='price_option' ><button class='btn btn-secondary btn-sm' title='@lang('tooltip.common.icon.export_record')' ><i class='ft-file-text' ></i></button></a>&nbsp;";
						
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='@lang('tooltip.common.icon.eye')' ><i class='fa fa-eye' ></i></button></a>&nbsp;";

						var froce = "{{request()->has('force')}}";
						if(o.status == "closed" ||  froce){
							var s_name="";
							if(o.surveyor){
								s_name=o.surveyor.full_name
							}
							var k = "<a href='javascript:void(0);' value="+o.id+" data-surveyor-id="+o.surveyor_id+" data-surveyor-name="+s_name+" data-id="+o.id+" title='@lang('tooltip.common.icon.edit')' class='btn btn-success btn-sm to_local'><i class='fa ft-download'></i></a>&nbsp;";
							
                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" title='@lang('tooltip.common.icon.edit')' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a>&nbsp;";
						
						
						}

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-log' title='@lang('tooltip.common.icon.delete')' data-deleted='0' data-id="+o.id+" ><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";
						
						if(o.deleted_at && o.deleted_at!=""){
							d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-log' title='@lang('tooltip.common.icon.delete')' data-deleted='1' data-id="+o.id+" ><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";
						  return "<a href='javascript:void(0);' class='recover-item btn btn-info btn-sm' moduel='survey' data-id="+o.id+" title='@lang('common.label.recover')'><i class='fa fa-repeat '></i></a>" + d;
						}else{
							return ex+v+e+d;	
						}
                       
                    }

                }
         ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/survey-data') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                d.surveyor_id = $("#filter_user").val();
				@if(request()->has('force'))
					d.enable_deleted = ($('#is_deleted_record').is(":checked")) ? 1 : 0;
				@endif
            }
        }
    });

	
	
	
    $('.filter').change(function() {
		
		var url ="{{ url('admin/survey') }}?surveyor_id="+$(this).val();
		window.location = url;
       // datatable.fnDraw();
		//datatable.columns.adjust().responsive.recalc();
    });
	$('#is_deleted_record').change(function() {
		datatable.fnDraw();
    });

	$(document).on('click', '.recover-item', function (e) {
        var id = $(this).attr('data-id');
        var moduel = $(this).attr('moduel');
        var r = confirm("@lang('common.js_msg.confirm_for_delete_recover',['item_name'=>'Survey'])");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/recover-item') }}",
				data: {item:moduel,id:id},
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
    $(document).on('click', '.del-log', function (e) {
			var id = $(this).attr('data-id');
			
			if($(this).attr('data-deleted') =="1"){
				var r = confirm("@lang('common.js_msg.confirm_for_delete_forever')");
			}else{
				var r = confirm("@lang('common.js_msg.confirm_for_delete_data')");
			}
			if (r == true) {
				$.ajax({
					type: "DELETE",
					url: "{{ url('/admin/survey') }}" + "/" + id,
					headers: {
						"X-CSRF-TOKEN": "{{ csrf_token() }}"
					},
					success: function (data) {
						datatable.fnDraw();
						toastr.success("@lang('common.js_msg.action_success')", data.message)
					},
					error: function (xhr, status, error) {
						toastr.error("@lang('common.js_msg.action_not_procede')",erro)
					}
				});
			}
		});


</script>


@endpush
