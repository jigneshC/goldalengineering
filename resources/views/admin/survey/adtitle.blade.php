

<div class="modal fade text-left" id="title_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                     <h3 class="modal-title title_form_model">@lang('report.label.document_title') </h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                   
                
            </div>
            {!! Form::open(['url' => '','id'=>'unitpackage_form','class' => 'form-horizontal unitpackage_form', 'files' => true]) !!}
			
            <input type="hidden" name="responce_type"  value="document_title" />
                        
            <div class="modal-body">
				<div class="form-group {{ $errors->has('document_title_a') ? ' has-error' : ''}}">
                {!! Form::label('document_title_a',trans('report.label.document_title') ,['class' => '']) !!}
                <div class="form-group position-relative">
                    <input type="text" name="document_title_a" class="filter form-control"  id="document_title_a" style="">    
                </div>
				</div>
            </div>
            <div class="modal-footer">
            <p class="form_submit_error text-error"></p>
               {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.add_new'), ['class' => 'btn btn-primary']) !!}

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



@push('js')


<script>


	 
	 
 
/***************action model******************/
		var _models = "#title_modal";
        $(document).on('click', '.add_title_open', function (e) {
			$('#unitpackage_form')[0].reset();
			$(_models).modal('show');
            return false;
        });
		

    $('.unitpackage_form').submit(function(event) {

        var error_msg = "";
        

        
        var url = "{{url('admin/constants-add-title')}}";
        var method = "POST";

        $.ajax({
            type: method,
            url: url,
            dataType:'json',
            async:false,
            processData: false,
            contentType: false,
            data:new FormData($("#unitpackage_form")[0]),
            success: function (result) {
                $(_models).modal('hide');
                toastr.success('Action Success!', result.message);
				var iddata = result.data.title;
				$("#document_title").append("<option value='" + iddata + "' selected>"+iddata+"</option>");
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
</script>
@endpush