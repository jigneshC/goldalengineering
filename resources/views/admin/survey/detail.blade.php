<?php

\App::setLocale('he');

$logo = url('public/assets/images/logo.png');
$name = "";
$is_fullsizelogo = 0;
if($item->branch && $item->branch->refeimg && $item->branch->refeimg->file_url != ""){
	$logo = $item->branch->refeimg->file_url;
	$name = $item->branch->name;
	$is_fullsizelogo = $item->branch->is_fullsizelogo;
}
		
$rtexts = \App\Responcetext::where("slug","other_info_image_1")->get();


$sign_url = "";
$licence_no = "";
$experience = [];
$qualification = [];

if($item->surveyor && $item->surveyor->signature){
	$sign_url = $item->surveyor->signature->file_url;
}else{
	$rf_sign = \App\Responcetext::where("slug","signature")->first();
	
	if($rf_sign && $rf_sign->refefile &&  $rf_sign->refefile->file_url != ""){
		$sign_url = $rf_sign->refefile->file_url;
	}
}	

if($item->surveyor){
	$licence_no = $item->surveyor->licence_no;
	$qualification = $item->surveyor->qualification; 
	$experience = $item->surveyor->experience;  
	
}	

$hidprice = 0;
if(Request::get('price') == 1){
	$hidprice = 1;
}
$_category_id = 0;
if(Request::has('category_id')){
	$_category_id = Request::get('category_id');
}
$free_text = 0;
if(Request::has('free_text')){
	$free_text = 1;
}

$survey_date = \Carbon\Carbon::parse($item->survey_date)->format("d/m/Y");
if(isset($item) && $item->parent_id && $item->parent_id >0 && $item->survey_date_option){
	$survey_date = \Carbon\Carbon::parse($item->created_at)->format("d/m/Y");	
}

?>

@extends('layouts.apex')

@section('title',trans('survey.label.all_detail'))

@section('content')
@push('css')
<link href="https://fonts.googleapis.com/css?family=David+Libre&display=swap" rel="stylesheet">
<style>
 body{
	 font-family: 'David Libre', serif !important;
 }
 .display_in_print{
    display: none !important;
}
.content-wrapper{
    display: block !important;
	background-color:transparent;
	
}
.footer {
    position: inherit !important;
}
p{
	text-align:right;
}




@page:left{
  @bottom-left {
    content: "Page " counter(page) " of " counter(pages);
  }
}



@media print {
	.new-page{
		page-break-before: always;	
	}
	
	img {
        page-break-inside: avoid;
    }
    .hide-on-print{
        visibility: hidden;
		heigh:0px;
		margin:0px;
		padding: 0px !important; 
		margin: 0px !important;
	}
    .displaynone_on_print , .footer{
        display: none !important;
		padding: 0px !important; 
		margin: 0px !important;
	}
    .display_in_print{
        display: block !important;
		padding: 0px !important; 
		margin: 0px !important;
	}
    .app-sidebar{
        display: none !important;
		padding: 0px !important; 
		margin: 0px !important;
	}
	.content-wrapper , .main-content, .main-panel , .margin0-on-print , .content-header , .card , .pl-3, .px-3{
		padding: 0px !important; 
		margin: 0px !important;
	}
	pre {
		border:0;
	}
   
   body {-webkit-print-color-adjust: exact; }
   
   html, body {
     background-color: #fff;
    }
}


</style>
@endpush


@include ('admin.survey.assignmodel',['isReload'=>1])

<section id="about">
    <div class="row hide-on-print">
        <div class="col-12">
            <div class="content-header">@lang('survey.label.all_detail')</div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header hide-on-print">
                        <div class="form-group">
                            
							<a href="{{ url('/admin/survey') }}" title="@lang('common.label.back')">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
								<i class="fa fa-angle-left"></i> @lang('common.label.back')
							</button>
							 </a>
							 
							 
							 <a href="#" title="PDF" class="print_report">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
								<i class="fa fa-angle-left"></i> @lang('survey.label.export_pdf') 
							</button>
							 </a>
							 
							 
							
							 
							@if($item->status == "closed" || request()->has('force'))
							<a href="{{ url('/admin/survey/'.$item->id.'/edit') }}" title="@lang('tooltip.common.icon.edit')">
								<button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">
								<i class="fa fa-pencil"></i>
								@lang('tooltip.common.icon.edit')
								</button>
							</a>
							
							<a href='javascript:void(0);' data-surveyor-id="{{$item->surveyor_id}}" @if($item->surveyor) data-surveyor-name="{{$item->surveyor->full_name}}" @endif data-id="{{$item->id}}" title='@lang('tooltip.common.icon.edit')' class='to_local'>
							<button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
							<i class='fa ft-download'></i>
							@lang('survey.label.local_sync')
							</button>
							</a>
							
							@endif
							
							{!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/survey', $item->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> '.trans('tooltip.common.icon.delete'), array(
                            'type' => 'submit',
                            'class' => 'btn btn-raised btn-danger btn-min-width mr-1 mb-1',
                            'title' => trans('comman.label.delete'),
                            'onclick'=>"return confirm('".trans('common.js_msg.confirm_for_delete_data')."')"
                            ))!!}
                            {!! Form::close() !!}
							
							<a href="{{ url('/admin/survey/'.$item->id.'/export-advance') }}" class="price_option" title="@lang('tooltip.common.icon.export_advance')">
                            <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1">
								<i class="ft-file-text"></i> @lang('tooltip.common.icon.export_advance')
							</button>
							 </a>
							
                            <a href="{{ url('/admin/survey/'.$item->id.'/export') }}" class="price_option" title="@lang('tooltip.common.icon.export_record')">
                            <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1">
								<i class="ft-file-text"></i> @lang('tooltip.common.icon.export_record')
							</button>
							 </a>
							 
							 <a href="{{ url('/admin/survey/'.$item->id.'/export-by-place') }}" class="price_option" title="Exporty by placename">
                            <button type="button" class="btn btn-raised btn-secondary btn-min-width mr-1 mb-1">
								<i class="ft-file-text"></i> @lang('survey.label.export_by_place')
							</button>
							 </a>
							 
							  <a href="{{ url('/admin/issues-create/'.$item->id.'') }}" title="@lang('issue.label.add_issue')">
								<button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">
									<i class="fa fa-plus"></i> @lang('issue.label.add_issue')
								</button>
							 </a>
                        </div>
	                 <div class="next_previous pull-right">
                   
                      </div>  
                        
	            </div>

         		<div class="card-body card-white-board card-center" style="max-width: 842px; margin: 0 auto;">
                    <div class="card-block"  >
						@if($is_fullsizelogo)
						<div class="topfixed-header-board clearfix" style="text-align:center;">
							<div class="logo-thumb-div" style="margin-bottom: 15px;">
								<img alt="Logo" class="logo-img01" width="100%"  src="{{$logo}}" />
							</div>
						</div>	
						@else
						<div class="topfixed-header-board clearfix" style="text-align:center;">
							<div class="logo-thumb-div" style="margin-bottom: 15px;">
								<img alt="Logo" class="logo-img01" height="100" src="{{$logo}}" />
							</div>
						</div>
						@if($item->branch)
						<div class="card-content" style="margin-b">
							<p style="width: 100%;">
							
							<span style="padding: 0 10px; display: inline-block; width: 49%; text-align: left;">{{ $item->branch->title_2 }}</span>
							<span style=" text-align:right; width: 49%; padding: 0 10px; display: inline-block;">{{ $item->branch->title_1 }}</span>
							</p>
							<p style="width: 100%;">
							
							<span style="padding: 0 10px; display: inline-block; width: 49%; text-align: left;"> {{$item->branch->phone_2}}</span>
							<span style=" text-align:right; width: 49%; padding: 0 10px; display: inline-block;"> {{ $item->branch->phone_1 }}</span>
							</p>
							
							
							<p style="width: 100%;"> 
							<span style="padding: 0 10px; display: inline-block; width: 49%; text-align: left;">{{ $item->branch->email_2 }}</span>
							<span style=" text-align:right; width: 49%; padding: 0 10px; display: inline-block;"> {{$item->branch->email_1 }}</span>
							</p>
						</div>
						@endif
						@endif
						<hr>
						<?php 
						$document_title = trans('report.label.document_title_default');
						if($item->document_title && $item->document_title !=""){ $document_title = $item->document_title; }
						?>
						<table  style="width:100%;"><tr><td>
						<div class="date-and-to" style="display: flex; justify-content: space-between; flex-direction: row; margin-top: 15px;">
							<div class="to-content">
							</div>
							<div class="date-content">
							<p><strong>@lang('survey.label.created') :  <a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=created" id="created"> {{ \Carbon\Carbon::now()->format("d/m/Y") }} </a> </strong></p>
							</div>
						</div>
						</td></tr>
						<tr><td>
						<div class="reports-title">
								<h3 style="font-size: 30px; text-align: center; font-weight: 600; color: #004d73; text-decoration: underline; margin-bottom: 30px;">
								<a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=document_title" id="document_title">
								<span style="display:block"> {{ $document_title }}</span>
								</a></h3>
								 
								 
								 
								
							</div>
						</td></tr>
						</td></tr>
						<tr><td>
							<div class="to-content">
								<table  style="width:100%;margin-top: 100px;">
								<tr>
									<td style="width:100px;vertical-align: initial;">@lang('survey.label.owner_name') :</td>
									<td>
									@if($item->owner_name && $item->owner_name !="")
									<a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=owner_name" id="owner_name">{{$item->owner_name}} </a></br>
									@endif
									@if($item->address && $item->address !="")
									<a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=address" id="address">{{$item->address}} </a></br>
									@endif
									@if($item->owner_email && $item->owner_email !="")
									<a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=owner_email" id="owner_email">{{$item->owner_email}} </a></br>
									@endif
									@if($item->owner_phone && $item->owner_phone !="")
									<a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=owner_phone" id="owner_phone"> {{$item->owner_phone}} </a> </br>
									@endif
									
									</td>
									
								</tr>
								</table>
								
								

								
							</div>
						</td></tr>
						</table>
						<br/><br/><br/><br/><br/>
						<div class = "new-page"></div>
						
						
						<div class="date-and-to" style="display: flex; justify-content: space-between; flex-direction: row; margin-top: 15px;">
							<div class="to-content">
							<p style="margin-t0p: 20px;">@lang('report.label.report_above_header')</p>
							</div>
							
							<div class="date-content">
							<p><strong>@lang('survey.label.created') :  <a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=created" id="created"> {{ \Carbon\Carbon::now()->format("d/m/Y") }} </a> </strong></p>
							</div>
							
							
						</div>

						<div class="reports-title">
							<h3 style="font-size: 30px; text-align: center; font-weight: 600; color: #004d73; text-decoration: underline; margin-bottom: 30px;"><a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=professional_opinion" id="survey_phone">
							הנדון:</span> {{$document_title}} ב{{$item->address}} <span style="display:block">@lang('survey.label.professional_opinion')</span></a></h3>
							
						</div>
						@if($item->enable_court)
						<table  style="width:100%;">
						<tr>
							<td style="width:100px;vertical-align: initial;">
								<p style="width: 100%; display: table;">
								  <span style="display: table-cell; width: 50px;"><b> @lang('report.label.case_number'): </b> </span>
								  <span style="display: table-cell; width: 150px;border-bottom: 1px solid black;">  {{$item->case_number}} </span>
								</p>
							</td>
							<td style="width:100px;vertical-align: initial;"> 
								<p style="width: 100%; display: table;">
								  <span style="display: table-cell; width: 70px;"><b> &nbsp;&nbsp; @lang('report.label.name_of_court'): </b> </span>
								  <span style="display: table-cell; width: 100px;border-bottom: 1px solid black;"> {{$item->name_of_court}} </span>
								</p> 
							</td>
							<td style="width:100px;vertical-align: initial;"> 
								<p style="width: 100%; display: table;">
								  <span style="display: table-cell; width: 40px;"><b> &nbsp;&nbsp; @lang('report.label.court_date'): </b> </span>
								  <span style="display: table-cell; width: 110px;border-bottom: 1px solid black;"> @if($item->court_date) {{ \Carbon\Carbon::parse($item->court_date)->format("d/m/Y")}} @endif </span>
								</p> 
							</td>
						</tr>
						</table>
						<br/><br/><br/>
						@endif
						
						@if($item->surveyor)
						<table  style="width:100%;margin-top: 100px;">
						<tr>
							<td style="width:150px;vertical-align: initial;">@lang('survey.label.surveyor_name') :</td>
							<td>
							@if($item->surveyor->full_name && $item->surveyor->full_name !="")
							{{$item->surveyor->full_name}} </br>
							@endif
							@if($item->surveyor->address && $item->surveyor->address !="")
							{{$item->surveyor->address}} </br>
							@endif
							@if($item->surveyor->email && $item->surveyor->email !="")
							{{$item->surveyor->email}} </br>
							@endif
							
							</td>
							
						</tr>
						</table>
						<br/><br/><br/>	
						@endif	
								
						<div style="report-content;text-align: right;" >
						<ul style="list-style: none; text-align: right;">
							<li style="text-align: right; ">@lang('report.declartaion.l1',["licence_no"=>$licence_no])</li>
							<li style="text-align: right; ">@lang('report.declartaion.l2',["name"=>$item->owner_name])</li>
							<li style="text-align: right; ">@lang('report.declartaion.l3',["date"=>$survey_date])</li>
							<li style="text-align: right; ">@lang('report.declartaion.l4')</li>
							<li style="text-align: right; ">@lang('report.declartaion.l5')</li>
							<li style="text-align: right; ">@lang('report.declartaion.l6')</li>
							
							<li style="text-align: right">
										<p style="text-align: right;">	@lang('report.declartaion.l6_a')</p>
										<ul style="list-style: disc !important; text-align: right;">
										
										@foreach($qualification as $qual)
										
										<li style="text-align: right; padding-right: 15px;">
										{{ $qual->desc }}
										</li>	
										
										@endforeach
										</ul>
							</li>
							<li style="text-align: right">
										<p style="text-align: right;">	@lang('report.declartaion.l6_b') </p>
										<ul style="text-align: right; list-style: disc !important;">
											@foreach($experience as $qual)
											
											<li style="text-align: right; padding-right: 15px;">
											{{ $qual->desc }}
											</li>	
											
											@endforeach
																
										</ul>
							</li>

							<li style="text-align: right; ">@lang('report.declartaion.l7')
								<br/>
								&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {!! $item->getPerposeOfVisit() !!}
							</li>
							<li style="text-align: right; ">@lang('report.declartaion.l8')
							
									<?php
										$selected_q = [];
										if(1){
											foreach($item->getaccessories() as $val){
												if($val->desc && $val->desc != ""){
													$selected_q[] = $val->desc;
												}
											}
										}
									?>

									<ul style="list-style: none !important; display: inline-block; width: 100%;">
									@foreach($selected_q as $k => $q)
										<li style="text-align: right; padding-right: 15px; width: 50%; float: left; "> -{{$q}}   &nbsp;&nbsp;&nbsp;</li>
									@endforeach	
									</ul>
							</li>

							<li class = "new-page" style="text-align: right; ">@lang('report.declartaion.l9')
					</ul>
							<?php
								$selected_pm = [];
								if(1){
									foreach($item->getpfmaterials() as $val){
										if($val->desc && $val->desc != ""){
											$selected_pm[] = $val->desc;
										}
									}
								}
							?>
							
							<?php 
							$cat_tr = "";
							?>
							@if($selected_pm && count($selected_pm))
								@foreach($selected_pm as $k=>$pm)
									@if($k%2 == 0)
									<?php 
									
									$pm2 = (isset($selected_pm[$k+1]))? $selected_pm[$k+1] : "";
									$cat_tr.= '<tr><td style="width:50%"> -'.$pm.' </td><td style="width:50%"> -'.$pm2.'</td></tr>';
									?>
									@endif
								@endforeach
							<?php 
							
							$table = '<table cellpadding="10" border="1" style="width: 100%; border: 2px #000000 solid;"><tbody>'.$cat_tr.'</tbody></table>';;
		 
							echo $table."<br/>";
							
							?>	
							@else
								
							@foreach($rtexts as $rd)
								@php($rf = $rd->refefile)
								@if($rf && $rf->file_url != "")
									<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px;">
										<img src="{{$rf->file_url}}" alt="img-tbale" width="100%;">
									</div>
								@endif
							@endforeach
							
							@endif
							
							<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px; text-align: left;">
								<p style="font-weight: 600;">הריני מצהיר בזאת כי אין כל עניין אישי בנכס הנידון.</p>
								<img src="{{$sign_url}}" alt="img-tbale" height="100" width="auto;">
							</div>
								
							<div class="new-page"></div>
							@if($item->general_info && $item->general_info != "")
							<h3 style="font-weight: 500; font-size: 18px;"><a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=general_info" id="general_info">@lang('survey.label.general_info')</a></h3>
							@php( $general_info = str_replace(trans('survey.label.general_info_text2'),"",$item->general_info) )
							<div style="white-space: pre-wrap;">{!! $item->general_info !!}
								@if($item->is_consultants)  @lang('survey.label.general_info_text2') @endif
							</div>
							@endif	
							
							@if($item->property_desc && $item->property_desc != "")
								<p style="font-weight: 600; font-size: 16px;"><a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=property_desc" id="property_desc">@lang('survey.label.property_desc')</a></p>
								<div style="white-space: pre-wrap;">{!! $item->property_desc !!}</div>
							@endif	

							@if($item->legal_thing_isa && $item->legal_thing_isa != "")
								<p style="font-weight: 600; font-size: 16px; "><a href="{{url('/admin/survey/'.$item->id.'/edit')}}?hashurl=legal_thing_isa" id="legal_thing_isa">@lang('survey.label.legal_thing_isa')</a></p>
								<div style="white-space: pre-wrap;">{!! $item->legal_thing_isa !!}</div>
							@endif		
							
							@php( $other_info = str_replace(trans('survey.label.general_info_text1'),"",$item->other_info) )
							<div style="white-space: pre-wrap;">{!! $other_info !!}
								@if($item->is_the_sale_law_1973) @lang('survey.label.general_info_text1') @endif
							</div>
							
							@if($item->is_the_sale_law_1973)
							
							@if($item->getspinfoa() )
								<div style="white-space: pre-wrap;">{!! $item->getspinfoa()->desc !!}</div>
							@else
							<?php  $rtexts = \App\Responcetext::where("slug","other_info_image_3")->get(); ?>	
							@foreach($rtexts as $rd)
								@php($rf = $rd->refefile)
								@if($rf && $rf->file_url != "")
									<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px;">
										<img src="{{$rf->file_url}}" alt="img-tbale" width="100%;">
									</div>
								@endif
							@endforeach
							
							@endif
							
							
							@if($item->getspinfob())
								<div style="white-space: pre-wrap;">{!! $item->getspinfob()->desc !!}</div>
							@else
							<?php  $rtexts = \App\Responcetext::where("slug","other_info_image_2")->get(); ?>
							@foreach($rtexts as $rd)
								@php($rf = $rd->refefile)
								@if($rf && $rf->file_url != "")
									<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px;">
										<img src="{{$rf->file_url}}" alt="img-tbale" width="100%;">
									</div>
								@endif
							@endforeach
							
							@endif
								
								
							@endif	
							
							<?php $rd = \App\Responcetext::where("slug","report_focus_note")->first(); ?>	
								@if($item->legal_thing_a != "")
								<p style="font-weight: 600; font-size: 16px; margin-top: 15px;">@lang('constant.slug.report_focus_note')</p>
								
								<div style="white-space: pre-wrap;">{!! $item->legal_thing_a !!}</div>

								@endif
							</li>

						</ul>
						<div class = "new-page"></div>
						<div style="display: block; page-break-before: always; ">
							@if($free_text !=1)
							<h3 style="font-weight: 600; margin-bottom: 15px; margin-top: 15px; text-align: center;">@lang('report.label.findings')</h3>
							
							<?php
							
							$cat_id = 0;
							$cat_cntr = 0;
							$cat_sub_cntr = 0;
							
							$grp_location = "";
							$total_cost = 0;
							$cat_cost = [];
							$sub_cat_cost = [];
							
							$cat_group = array();
							foreach($item->issue as $k => $issue){
								if(isset($issue->getsubcategory) && isset($issue->category) ){
									//Specific category data
									if($_category_id == 0 || $_category_id == $issue->category->id){
										$_CATID = $issue->getsubcategory->id;
										
										if(!isset($cat_group[$issue->category_id]) && isset($issue->category)){
											$cat_group[$issue->category_id] = ['name'=>$issue->category->name,'items'=>[]];
										}
										
										if(isset($cat_group[$issue->category_id]['items'][$_CATID]) && isset($issue->getsubcategory)){
											$cat_group[$issue->category_id]['items'][$_CATID]['items'][] = $issue;
										}else if(isset($issue->getsubcategory)){
											$cat_group[$issue->category_id]['items'][$_CATID]['name'] = $issue->getsubcategory->name;
											$cat_group[$issue->category_id]['items'][$_CATID]['items'][] = $issue;
										}
									}
								}
							}
							
							$k = 9;
							$d = 0;
							$e = 0;
							foreach($cat_group as  $grp){
								$k++;
								?>
								<p style="text-align: right;  font-weight: 600; ">
								{{$k}}.  <span style="text-decoration: underline; margin-right:12px;">  {{$grp['name']}}</span>
								</p>
								<?php
								$d=0;
								foreach($grp['items'] as  $gr){
									$d++;
									$e = 0;
									$total_issue_c = count($gr['items']);
									foreach($gr['items'] as  $issue){
										$_CATID = $issue->getsubcategory->id;
										$otherJson = json_decode($issue->other_json_data,true);
										$e++;
										?>
										@if($e == 1 && $issue->issue_detail && $issue->issue_detail != "")
											 <span style="margin-right:30px;font-weight: 600;">{{$k}}.{{$d}}. </span> 
											<p style="margin-right:30px;font-weight: 600;white-space: pre-wrap;"> <a href="{{url('/admin/issues/'.$issue->id.'/edit')}}?hashurl=issue_detail_{{$issue->id}}" id="issue_detail_{{$issue->id}}"> {!! $issue->issue_detail !!} </a></p>
										@endif
										
										 <span style="margin-right:30px;white-space: pre-wrap;"> {{$k}}.{{$d}}.{{$e}}. <a href="{{url('/admin/issues/'.$issue->id.'/edit')}}?hashurl=location_{{$issue->id}}" id="location_{{$issue->id}}">{!! $issue->location !!} </a></span> 
										 
										@if($issue->refefile->count())
						
											@foreach($issue->refefile as $rf)
												@if($rf->file_url && $rf->file_url != "")
												<div class="reports-images relative-container"  id="ref{{$rf->id}}" style="margin-top: 20px; margin-bottom: 30px;">
												<img src="{{$rf->file_url}}?uid={{ time() }}" alt="img-tbale" width="100%;">
												<a href="{{url('admin/reference-file/'.$rf->id.'/rotate')}}?dir=left" title="Rotate"  class="rotate right35 btn btn-danger btn-sm" ><i class='ft-corner-up-left' aria-hidden='true'></i>  </a>
												<a href="{{url('admin/reference-file/'.$rf->id.'/rotate')}}" title="Rotate"  class="rotate btn btn-danger btn-sm" ><i class='ft-corner-up-right' aria-hidden='true'></i>  </a>
												</div>
												@endif
											@endforeach
											
										@endif
										
										
										
										<p style="margin-right:30px;white-space: pre-wrap;"> {!! $issue->note !!} </p>
										@if($issue->total_cost > 0 && !$hidprice)
										
											@if($issue->costImage && $issue->show_default_price_detail)
												<img src="{{$issue->costImage->file_url}}?uid={{ time() }}" alt="img-tbale" width="100%;">
											@elseif($issue->cost_detail && $issue->cost_detail != "" && $issue->show_default_price_detail)
												<p style="margin-right:30px;white-space: pre-wrap;"> {!! $issue->cost_detail !!} </p>
											@endif
											<p style="margin-right:30px;font-weight: 600;white-space: pre-wrap;"><a href="{{url('/admin/issues/'.$issue->id.'/edit')}}?hashurl=note_{{$issue->id}}" id="note_{{$issue->id}}"> {{$k}}.{{$d}}.{{$e}}. : @lang('report.label.cost') {{$issue->total_cost}} ש"ח  @if(isset($otherJson['flag_show_unit_price_in_report'])) ( {{$issue->unit_cost}}X{{$issue->number_of_unit}}) @endif</a></p>
										@endif
										<?php 
										$total_cost = $total_cost + $issue->total_cost;
					
										if(isset($cat_cost[$issue->category_id]) && isset($cat_cost[$issue->category_id]['cost'])){
											$cat_cost[$issue->category_id]['cost'] = $cat_cost[$issue->category_id]['cost'] + $issue->total_cost;
										}else{
											$cat_cost[$issue->category_id] = ['cost'=>$issue->total_cost,'name'=>$issue->category->name];
										}
										
										if(isset($sub_cat_cost[$_CATID]) && isset($sub_cat_cost[$_CATID]['cost'])){
											$sub_cat_cost[$_CATID]['cost'] = $sub_cat_cost[$_CATID]['cost'] + $issue->total_cost;
										}else{
											$sub_cat_cost[$_CATID] = ['cost'=>$issue->total_cost,'name'=>$issue->getsubcategory->name];
										}
										
										
										if($total_issue_c == $e){
						
											if($issue->quote && $issue->quote != ""){
												?> 
												{{-- <span style="margin-right:30px;font-weight: 600;">ציטוט: </span>  --}}
												<p style="margin-right:30px;"> {!! $issue->quote !!}</p> 
												<?php
											}
											if($issue->getsubcategory){
												$pluck = $issue->getsubcategory->refefile->pluck("id");
												if($issue->img_hint && $issue->img_hint != ""){
													
													$img_quote = [];
				
													if($issue->getsubcategory->quote_desc && $issue->getsubcategory->quote_desc != ""){
														$img_quote = json_decode($issue->getsubcategory->quote_desc,true);
													}
		
													//$img_hint = json_decode($issue->img_hint,true);
													$img_hint = $issue->hintImages();
													$imgc = 0;
													foreach($img_hint as $z => $hint){
														if(isset($hint['img_id']) && in_array($hint['img_id'],$pluck->toArray())){
															$rf = \App\Refefile::whereId($hint['img_id'])->first();
															if($rf && $rf->file_url && $rf->file_url != ""){
																if($imgc ==0){
																	
																}
																?> 
																<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px;">
																	<img src="{{$rf->file_url}}" alt="img-tbale" width="100%;">
																	@if(isset($img_quote[$rf->id]))
																		<p style="margin-right:30px;"> {{ $img_quote[$rf->id] }}</p> 
																	@endif
																</div> 
																<?php
																$imgc++;
															}
														}
													}
													
												}
												
											
											}
											if($issue->recommendation && $issue->recommendation != ""){
												?>
												 <span style="margin-right:30px;font-weight: 600;">@lang('issue.label.recommendation') </span> 
													<div style="margin-right:30px;white-space: pre-wrap;"> {!! $issue->recommendation !!}</div>
												<?php
											}
											?>
											@if($sub_cat_cost[$_CATID]['cost'] > 0 && !$hidprice)
												<p style="margin-right:30px;font-weight: 600;"><a href="{{url('/admin/issues/'.$issue->id.'/edit')}}?hashurl=total_cost_{{$issue->id}}" id="total_cost_{{$issue->id}}"> {{$k}}.{{$d}}.  @lang('issue.label.total_cost') : {{$sub_cat_cost[$_CATID]['cost']}} ש"ח  </a></p>
											@endif
											<?php
										}
										?>
										
										
										
										<?php
									}
								}
								?>
								
							<div class = "new-page"></div>
								<?php
							}
					
							$supervision_charge = 10;
							$gdp_charge = 15;
							$vat_charge = 17;
							
							$gdp_per = round($gdp_charge*$total_cost/100);
							$supervision_per = round($supervision_charge*$total_cost/100);
							
							$sub_total = $total_cost + $gdp_per + $supervision_per;
							$vat_per = round($vat_charge*$sub_total/100);
							$net_toal = $vat_per + $sub_total;
							
							$cat_tr = "";
							$index = 1;
							foreach($cat_cost as $k=> $cs){
								if($cs['cost'] >0){
									if(!$hidprice){
										$_price = $cs['cost'].' ש"ח';
									}else{
										$_price = "";
									}
									$cat_tr.= '<tr><td style="text-align: center; "> '.$index.'</td><td style="text-align: center; "> '.$cs['name'].'</td><td style="text-align: center; ">'.$_price.'</td></tr>';
									$index++;
								}
							}
							
							if(!$hidprice){
								$_gdp_per = $gdp_per.' ש"ח';
								$_supervision_per = $supervision_per.' ש"ח';
								$_total_cost = $total_cost.' ש"ח';
								$_sub_total = $sub_total.' ש"ח';
								$_vat_per = $vat_per.' ש"ח';
								$_net_toal = number_format(round($net_toal)).' ש"ח';
							}else{
								$_gdp_per = "";
								$_supervision_per = "";
								$_total_cost = "";
								$_sub_total = "";
								$_vat_per = "";
								$_net_toal = "";
							}
							$table = '<table style="width: 100%; text-align: center; margin-top: 30px; margin-bottom: 30px;" border="1">'.
            '<thead>'.
                '<tr style="text-align: center; font-weight: bold; ">'.
                    '<th style="width: 33.33%; text-align:center;">'.trans('survey.label.tax').'</th>'.
                    '<th style="width: 33.33%; text-align:center;">'.trans('survey.label.section').'</th>'.
					'<th style="width: 33.33%; text-align:center; ">'.trans('survey.label.price').'</th>'.
                '</tr>'.
            '</thead>'.
            '<tbody>'.$cat_tr.
                '<tr><td > </td><td style="text-align: center; "> '.trans('survey.label.all_problem_price').'</td><td style="text-align: center; "> '.$_total_cost.'</td></tr>'.
                '<tr><td >  </td><td style="text-align: center; "> '.trans('survey.label.gdp')." ".$gdp_charge."%".'</td><td style="text-align: center; "> '.$_gdp_per.'</td></tr>'.
                '<tr><td > </td><td style="text-align: center; "> '.trans('survey.label.supervision')." ".$supervision_charge."%".'</td><td style="text-align: center; "> '.$_supervision_per.'</td></tr>'.
                '<tr><td ></td><td style="text-align: center; ">'.trans('survey.label.total').'</td><td style="text-align: center; "> '.$_sub_total.'</td></tr>'.
                '<tr><td ></td><td style="text-align: center; ">'.trans('survey.label.vat')." ".$vat_charge."%".'</td><td style="text-align: center; "> '.$_vat_per.'</td></tr>'.
                '<tr><th ></th><th style="text-align: center; font-weight: bold; ">'.trans('survey.label.total_payment_including_VAT').'</th><td style="text-align: center; font-weight: bold; "> '.$_net_toal.'</td></tr>'.
            '</tbody>'.
         '</table>';;
							?>

							

							<div class = "new-page"></div>
							<h3 style="font-weight: 600; font-size: 18px; margin-top: 20px; margin-bottom: 15px; text-align: center;">@lang('survey.label.estimated_repair_costs')</h3>
							@if(!$hidprice)
							{!! $table !!}
							@endif
							@endif
							
							
								@if($item->reviews_and_summary && $item->reviews_and_summary!="")
								<p style="font-weight: 600;">@lang('survey.label.reviews_and_summary')</p>
								<div style="margin-right:30px;white-space: pre-wrap;"> {!! $item->reviews_and_summary !!}</div>
								@endif
								<br/>
								@if($item->not_tested && $item->not_tested!="")
								<p style="font-weight: 600;">@lang('survey.label.not_tested')</p>
								<div style="margin-right:30px;white-space: pre-wrap;"> {!! $item->not_tested !!}</div>	
							 	@endif
								@if($item->checklist && $item->checklist != "")
									@php( $checklist = json_decode($item->checklist,true) )
									@if($checklist && count($checklist) > 0)
										@foreach($checklist as $chl)
											
											<p style="font-weight: 600;">{{ $chl['title'] }}</p>
											<div style="margin-right:30px;white-space: pre-wrap;"> {!! $chl['desc'] !!}</div>
										@endforeach
									@endif
								@endif
								<div class="reports-images" style="margin-top: 20px; margin-bottom: 30px; text-align: left;">
									<p style="font-weight: 600;">הריני מצהיר בזאת כי אין כל עניין אישי בנכס הנידון.</p>
									<img src="{{$sign_url}}" alt="img-tbale" height="100" width="auto;">
									
								</div>
									

							


						
						</div>
						</div>

					</div>
				</div>				

            </div>
        </div>
    </div>
</section>



@endsection

@push('js')


<script type="text/javascript" >

            
$(".print_report").click(function () {
    window.print();
});

@if(Request::has('print') && Request::get('print') == 1)
window.print();
@endif
</script>

@endpush
     