@extends('layouts.apex')
@section('title',trans('survey.label.SurveyMerge'))

@section('content')


    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('survey.label.SurveyMerge') </div>
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                </div>
	            <div class="card-body">
	                <div class="px-3">
					
	                    {!! Form::open(['url' => '/admin/survey-merge', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                          
						  
<div class="row ">


    <div class="col-md-12">


		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.master_survey')
            </label>
            <div >
                <input type="text" name="master_survey" class="filter form-control" id="master_survey" style="">
				{!! $errors->first('master_survey', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>

		<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.survey')
            </label>
            <div >
				 {!! Form::text('sub_survey', old('surveyor_note',null) , ['id'=>'sub_survey','class' => 'form-control',"multiple"=>'true',"autocomplete"=>"off","autocorrect"=>"off"]) !!}
				{!! $errors->first('sub_survey', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>



        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-success']) !!}
      
        </div>


    </div>


</div>





                            {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

</section>

   
@endsection


@push('js')
<script>

	
	
	
	$("#master_survey").select2({
        placeholder: "@lang('survey.msg.search_master_survey')",
        allowClear: true,

        @if(isset($surveyor) && $surveyor)
            initSelection: function (element, callback) {
                callback({id: {{$surveyor->id}}, text: "{{$surveyor->full_name}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif

        ajax: {
            url: "{{ url('admin/survey-search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    search: params, // search term
                };
            },
            results: function (result) {
                var data = result.data;
				console.log(data);
                return {
                    results: $.map(data, function (obj) {
						var dc = obj.id +" - "+obj.address;
                        return {id: obj.id, text: dc};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);
	

	$("#sub_survey").select2({
        placeholder: "@lang('survey.msg.search_master_survey')",
        allowClear: true,
		multiple:true,
        @if(Session::has('filter_services'))
            initSelection: function (element, callback) {
				var selected_service = <?php echo json_encode($selected_service); ?>;
                callback(selected_service);
            },
        @else   
            initSelection: function (element, callback) {
              //  callback({id: [], text: "" });
            },
        @endif
        
        ajax: {
            url: "{{ url('admin/survey-search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    search: params, // search term
					master_survey: $("#master_survey").val()
                };
            },
            results: function (result) {
                var data = result.data;
                return {
                    results: $.map(data, function (obj) {
                        var dc = obj.id +" - "+obj.address;
                        return {id: obj.id, text: dc};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);
	
	$('.filter').change(function() {
        $("#sub_survey").select2("val", "");
    });


</script>


@endpush
