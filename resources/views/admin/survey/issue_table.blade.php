

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('issue.label.issue_in_survey') </div>
       
    </div>
</div>

 <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    <div class="col-6">
					@if($item->merge_ids && $item->merge_ids != "")
						@php( $id_array = explode("," ,$item->merge_ids)	 )
						  <select name="filter_survey" class="filter form-control" id="filter_survey" style="max-width: 300px;">
							<option value="">@lang('issue.label.from_survey_id')</option>
							@foreach($id_array as $id)
							<option value="{{$id}}">{{$id}}</option>
							@endforeach
						  </select>
                    @endif        
						</div>
                    <div class="col-6">
                          
                            @include("admin.filter_deleted") 
							
						</div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <table class="table table-striped table-bordered base-style responsive datatable">
                            
                          <thead>
                            <tr>
                                <th data-priority="1" >@lang('common.label.id')</th>
								@if($item->merge_ids && $item->merge_ids != "")
									<th data-priority="2" >@lang('issue.label.from_survey_id')</th>
								@endif
                                <th data-priority="2" >@lang('issue.label.location')</th>
								<th data-priority="4" >@lang('issue.label.unit_cost') </th>
								<th data-priority="5" >@lang('issue.label.number_of_unit') </th>
								<th data-priority="5" >@lang('issue.label.total_cost') </th>
                                <th data-priority="7" >@lang('issue.label.issue_detail')</th>
                                
								<th>@lang('survey.label.status')</th>
                                <th>@lang('survey.label.created')</th>
                                <th>@lang('common.label.action')</th>
							</tr>
                            </thead>
							
							<tfoot>
								<tr>
									<th></th>
									@if($item->merge_ids && $item->merge_ids != "")
									<th></th>
									@endif
									<th></th>
									<th></th>
									<th></th>
									<th> {{$item->getNetCost() }}</th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
	



@push('js')
<script>

	var statues = <?php echo json_encode(trans('survey.status')); ?>;
	
	var survey_url = "{{ url('/admin/survey') }}";
	var url ="{{ url('/admin/issues') }}";
    var edit_url = "{{ url('/admin/issues') }}";
    var auth_check = "{{ Auth::check() }}";
		
	var auth_uid = {{\Auth::user()->id}};
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('common.datatable.emptyTable')",
            "infoEmpty":"@lang('common.datatable.infoEmpty')",
            "search": "@lang('common.datatable.search')",
            "sLengthMenu": "@lang('common.datatable.show') _MENU_ @lang('common.datatable.entries')",
            "sInfo": "@lang('common.datatable.showing') _START_ @lang('common.datatable.to') _END_ @lang('common.datatable.of') _TOTAL_ @lang('common.datatable.small_entries')",
            paginate: {
                next: '@lang('common.datatable.paginate.next')',
                previous: '@lang('common.datatable.paginate.previous')',
                first:'@lang('common.datatable.paginate.first')',
                last:'@lang('common.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: true,
        order: [0, "DESC"],
        columns: [
                { 
					"data": null,
					"name":"id",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						var content = o.id;
						if( o.duplicate_from && o.duplicate_from != "" && o.duplicate_from > 0){
							content = content + " <br/> @lang('issue.label.copy_from') <a href='"+edit_url+"/"+o.duplicate_from+"'> "+ o.duplicate_from + "</a>&nbsp;";
							if(o.dupfrom){
								content = content + " [ <a href='"+survey_url+"/"+o.dupfrom.survey_id+"'> "+ o.dupfrom.survey_id + "</a>&nbsp; ]";
							}
						}
						return content;
					}
				},
				
				@if($item->merge_ids && $item->merge_ids != "")
					{ 
						"data": null,
						"name":"dupfrom.survey_id",
						"searchable": true,
						"orderable": true,
						"render": function (o) {
							var content = "";
							if( o.from_survey_id && o.from_survey_id != "" && o.from_survey_id > 0){
								content = content + " <a href='"+survey_url+"/"+o.from_survey_id+"'> "+ o.from_survey_id + "</a>&nbsp; ";
							}
							return content;
						}
					},
				@endif
				
                { data: 'location',name : 'location',"searchable": true, "orderable": true},
                { data: 'unit_cost',name : 'unit_cost',"searchable": true, "orderable": true},
                { data: 'number_of_unit',name : 'number_of_unit',"searchable": true, "orderable": true},
				{ data: 'total_cost',name : 'total_cost',"searchable": true, "orderable": true},
                { 
					"data": null,
					"name":"issue_detail",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						var content = o.issue_detail;
						if(content && content.length > 80){
							content = content.substr(0, 80) + " ...";
						}
						return "<span class='line-break'>"+content+"</span>";
					}
				},
				{ 
					"data": null,
					"name":"status",
					"searchable": true,
					"orderable": true,
					"render": function (o) {
						return (o.status in statues) ? statues[o.status] : o.status;
					}
				},
				{ 
					"data": null,
					"name":"created_at",
					"searchable": false,
					"orderable": true,
					"render": function (o) {
						return o.created_a;
					}
				},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='@lang('tooltip.common.icon.eye')' ><i class='fa fa-eye' ></i></button></a>&nbsp;";
						@if($item->status == "closed")
                        e = "<a href='"+edit_url+"/"+o.id+"/edit?hashurl=total_costly_"+o.id+"' value="+o.id+" data-id="+o.id+" title='@lang('tooltip.common.icon.edit')' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i></a>&nbsp;";
						@endif
                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-log' title='@lang('tooltip.common.icon.delete')' data-deleted='0' data-id="+o.id+" ><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";
						
						if(o.deleted_at && o.deleted_at!=""){
							d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-log' title='@lang('tooltip.common.icon.delete')' data-deleted='1' data-id="+o.id+" ><i class='fa fa-trash' aria-hidden='true'></i></a>&nbsp;";
						  return "<a href='javascript:void(0);' class='recover-item btn btn-info btn-sm' moduel='issue' data-id="+o.id+" title='@lang('common.label.recover')'><i class='fa fa-repeat '></i></a>" + d;
						}else{
							return v+e+d;	
						}
                        
                    }

                }
         ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/issues-data') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                d.survey_id = "{{$item->id}}";
				@if($item->merge_ids && $item->merge_ids != "")
					d.from_survey_id = $("#filter_survey").val();
				@endif
				@if(request()->has('force'))
					d.enable_deleted = ($('#is_deleted_record').is(":checked")) ? 1 : 0;
				@endif
            }
        }
    });

    $('.filter').change(function() {
		datatable.fnDraw();
    });
	
	$('#is_deleted_record').change(function() {
		datatable.fnDraw();
    });

    $(document).on('click', '.del-log', function (e) {
			var id = $(this).attr('data-id');
			if($(this).attr('data-deleted') =="1"){
				var r = confirm("@lang('common.js_msg.confirm_for_delete_forever')");
			}else{
				var r = confirm("@lang('common.js_msg.confirm_for_delete_data')");
			}
			if (r == true) {
				$.ajax({
					type: "DELETE",
					url: "{{ url('/admin/issues') }}" + "/" + id,
					headers: {
						"X-CSRF-TOKEN": "{{ csrf_token() }}"
					},
					success: function (data) {
						datatable.fnDraw();
						toastr.success("@lang('common.js_msg.action_success')", data.message)
					},
					error: function (xhr, status, error) {
						toastr.error("@lang('common.js_msg.action_not_procede')",erro)
					}
				});
			}
		});
		
	$(document).on('click', '.recover-item', function (e) {
        var id = $(this).attr('data-id');
        var moduel = $(this).attr('moduel');
        var r = confirm("@lang('common.js_msg.confirm_for_delete_recover',['item_name'=>'Issue'])");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/recover-item') }}",
				data: {item:moduel,id:id},
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });	


</script>


@endpush
