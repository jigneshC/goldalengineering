@extends('layouts.apex')

@section('title',trans('survey.label.survey'))

@section('content')
 
    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('survey.label.survey') #{{$item->id}}</div>
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                          <a href="{{ url('/admin/survey/'.$item->id) }}" title="@lang('survey.label.survey_detail')">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
								<i class="fa fa-angle-left"></i> @lang('survey.label.survey_detail')
							</button>
							 </a>
	                <div class="actions pull-right">
                          
                        </div>
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
					
					
	                     {!! Form::open(['url' => 'admin/survey-duplicate', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                                <input type="hidden" name="unique_id" value="{{$item->unique_id}}">	
								<div class="form-group {{ $errors->has('duplicate_option') ? 'has-error' : ''}}">
									<label for="duplicate_option" class="">
										<span class="field_compulsory">*</span>@lang('survey.label.duplicate_option')
									</label>
									{!! Form::select('duplicate_option',trans('survey.duplicate_option'), null,['id'=>'duplicate_option','class' => 'form-control']) !!}
									{!! $errors->first('duplicate_option', '<p class="help-block text-danger">:message</p>') !!}
								</div>
								
								<div class="form-group">
								{!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('survey.label.duplicate_survey'), ['class' => 'btn btn-primary']) !!}
							
								</div>

                        {!! Form::close() !!}
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection
