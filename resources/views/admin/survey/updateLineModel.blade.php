<div class="modal fade text-left update_line_modal" id="update_line_modal"  role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel34">@lang('survey.label.local_sync')</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label>@lang('user.label.select_surveyor') </label>
                <div class="form-group position-relative">
				  {!! Form::hidden('itmez_id',0, ['class' => 'form-control','id'=>'itmez_id']) !!}	
				  {!! Form::textarea("modal_li", null , ['id'=>'modal_li','rows'=>3,'class' => 'form-control']) !!}
                </div>
            </div>
            <div class="modal-footer">
               
                {!! Form::button(trans('common.label.update'), ['class' => 'btn btn-light update_li_btn']) !!}
				<button type="button" class="btn btn-primary" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('common.label.cancel')</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')

<script>

   
/***************action model******************/
var s2 = null;
var _model = "#update_line_modal";
$(document).on('click', '.update_rt_list', function (e) {
    var id=$(this).attr('data-id');
    var value=$(this).attr('data-value');
    
   $("#itmez_id").val(id);
   $("#modal_li").val(value);
   $(_model).modal('show');
    return false;
});
	
$(document).on('click', '.update_li_btn', function (e) {
   var id=$("#itmez_id").val();
   var value= $("#modal_li").val();
   
   $("#rtcb_list_"+id).val(value);
   $("#rt_list_"+id).html(value);
   
   $(_model).modal('hide');
   return false;
});

   
</script>
@endpush









