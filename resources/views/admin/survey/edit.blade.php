@extends('layouts.apex')
@section('title',trans('survey.label.survey'))

@section('content')

	@include("admin.survey.adtitle")
    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('survey.label.survey') </div>
                {{-- @include('partials.page_tooltip',['model' => 'user','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/survey') }}" title="@lang('common.label.back')">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
								<i class="fa fa-angle-left"></i> @lang('common.label.back')
							</button>
							 </a>
	                
                          
                        
						<a href="#" title="@lang('common.label.save')" onclick="event.preventDefault(); document.getElementById('survey_form').submit();" >
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1"> @lang('common.label.save') </button>
                        </a>	
						
				
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
					
	                    {!! Form::model($item, [
                                'method' => 'PATCH',
                                'url' => ['/admin/survey', $item->id],
                                'class' => 'form-horizontal',
                                'files' => true,
                                'autocomplete'=>'off',
                                'id'=>'survey_form'
                            ]) !!}

                            @include ('admin.survey.form', ['submitButtonText' => trans('common.label.update')])

                            {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>

@include ('admin.survey.updateLineModel', ['slug_s' => 'general_info'])
@endsection


