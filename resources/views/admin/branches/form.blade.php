

<div class="row ">

    
    <div class="col-md-6">
      
        
 
<div class="form-group  {{ $errors->has('name') ? ' has-error' : ''}}">
    <label for="name" >
        <span class="field_compulsory">*</span>
        @lang('branches.label.name')
    </label>
    <div class="">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block text-danger">:message</p>') !!}
    </div>
</div> 

<div class="form-group  {{ $errors->has('title_1') ? ' has-error' : ''}}">
    <label for='title_1' >
        <span class="field_compulsory">*</span>
        @lang('branches.label.title_1')
    </label>
    <div class="">
        {!! Form::text('title_1', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title_1', '<p class="help-block text-danger">:message</p>') !!}
    </div>
</div> 
<div class="form-group  {{ $errors->has('title_2') ? ' has-error' : ''}}">
    <label for="name" >
        <span class="field_compulsory">*</span>
        @lang('branches.label.title_2')
    </label>
    <div class="">
        {!! Form::text('title_2', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title_2', '<p class="help-block text-danger">:message</p>') !!}
    </div>
</div> 
<div class="form-group  {{ $errors->has('phone_1') ? ' has-error' : ''}}">
    <label for="name" >
        <span class="field_compulsory">*</span>
        @lang('branches.label.phone_1')
    </label>
    <div class="">
        {!! Form::text('phone_1', null, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_1', '<p class="help-block text-danger">:message</p>') !!}
    </div>
</div> 
<div class="form-group  {{ $errors->has('phone_2') ? ' has-error' : ''}}">
    <label for="name" >
        <span class="field_compulsory">*</span>
        @lang('branches.label.phone_2')
    </label>
    <div class="">
        {!! Form::text('phone_2', null, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_2', '<p class="help-block text-danger">:message</p>') !!}
    </div>
</div> 
<div class="form-group  {{ $errors->has('email_1') ? ' has-error' : ''}}">
    <label for="name" >
        <span class="field_compulsory">*</span>
        @lang('branches.label.email_1')
    </label>
    <div class="">
        {!! Form::text('email_1', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email_1', '<p class="help-block text-danger">:message</p>') !!}
    </div>
</div> 
<div class="form-group  {{ $errors->has('email_2') ? ' has-error' : ''}}">
    <label for="name" >
        <span class="field_compulsory">*</span>
        @lang('branches.label.email_2')
    </label>
    <div class="">
        {!! Form::text('email_2', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email_2', '<p class="help-block text-danger">:message</p>') !!}
    </div>
</div> 


<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
    <label for="role" >
        <span class="field_compulsory"></span>@lang('branches.label.desc')
    </label>
    <div >
        {!! Form::textarea('desc', old('desc',null) , ['class' => 'form-control','id'=>'desc']) !!}
		{!! $errors->first('desc', '<p class="help-block text-danger">:message</p>') !!}
    </div>
</div>
		
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
	<label for="logo"> @lang('branches.label.logo_image') </label>
	<div class="">
		@if(isset($item) && $item->refefile->count())
		<div class="row">
		@foreach($item->refefile as $rf)
		@if($rf->file_thumb_url && $rf->file_thumb_url != "")
		<div class="col-sm-2 relative-container">
			<a href="{{url('admin/reference-file/'.$rf->id.'/delete')}}" onclick="return confirm('@lang('common.js_msg.confirm_for_delete',['item_name'=>trans('common.label.file')])')" class="close btn btn-danger btn-sm"><i class='fa fa-trash' aria-hidden='true'></i></a>
			<a class="example-image-link " href="{!! $rf->file_url !!}" data-lightbox="example-2" data-title="{{$rf->refe_file_real_name}}">
			<img src="{!! $rf->file_thumb_url !!}" height="80" />
			</a>
		</div>
		
		@endif
		@endforeach
		</div>
		<br/>
		@endif
		{!! Form::file('image',  ['class' => 'form-control']) !!}
		
		{!! $errors->first('image', '<p class="help-block text-danger">:message</p>') !!}
	</div>
</div>      

<div class="form-group {{ $errors->has('is_fullsizelogo') ? ' has-error' : ''}}">
    <label for="role" >
        @lang('branches.label.is_fullsizelogo')
    </label>
    <div >
        <input type="checkbox" name="is_fullsizelogo" class=" pull-left switchery"  id="is_fullsizelogo" value='1' {{ (isset($item) && $item->is_fullsizelogo) ? 'checked' : '' }}>
    </div>
</div>
		
        
	
        

        
      
        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('common.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
   
        
    </div>
   
    
</div>




