@extends('layouts.apex')

@section('title',trans('branches.label.branches')))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('branches.label.branches') </div>
               
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                       
						
							<a href="{{ url('/admin/branches') }}" title="@lang('common.label.back')">
                            <button type="button" class="btn btn-raised btn-success btn-min-width mr-1 mb-1">
								<i class="fa fa-angle-left"></i> @lang('common.label.back')
							</button>
							 </a>
							 
							
							<a href="{{ url('/admin/branches/'.$item->id.'/edit') }}" title="@lang('tooltip.common.icon.edit')">
								<button type="button" class="btn btn-raised btn-warning btn-min-width mr-1 mb-1">
								<i class="fa fa-pencil"></i>
								@lang('tooltip.common.icon.edit')
								</button>
							</a>
							
							{!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/branches', $item->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> '.trans('tooltip.common.icon.delete'), array(
                            'type' => 'submit',
                            'class' => 'btn btn-raised btn-danger btn-min-width mr-1 mb-1',
                            'title' => trans('comman.label.delete'),
                            'onclick'=>"return confirm('".trans('common.js_msg.confirm_for_delete_data')."')"
                            ))!!}
                            {!! Form::close() !!}
							
	                 <div class="next_previous pull-right">
                   
                      </div>  
                          
                        
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                   <div class="table-responsive custom-table-responsive">
                                        <table class="table table-striped">
                                        <tbody>
											<tr>
												<th># @lang('common.label.id') </th>
												<td> {{$item->id}} </td>
											</tr>
											<tr>
												<th>@lang('branches.label.name')</th>
												<td> {{ $item->name }} </td>
											</tr>
											
											
											<tr>

                                                <th>@lang('branches.label.logo_image')</th>
                                                <td> 
													@if($item->refefile->count())
															<div class="row">
															@foreach($item->refefile as $rf)
															@if($rf->file_thumb_url && $rf->file_thumb_url != "")
															<div class="col-sm-2 relative-container">
																<a href="{{url('admin/reference-file/'.$rf->id.'/delete')}}" onclick="return confirm('@lang('common.js_msg.confirm_for_delete',['item_name'=>trans('common.label.file')])')" class="close btn btn-danger btn-sm"><i class='fa fa-trash' aria-hidden='true'></i></a>
																<a class="example-image-link " href="{!! $rf->file_url !!}" data-lightbox="example-2" data-title="{{$rf->refe_file_real_name}}">
																<img src="{!! $rf->file_thumb_url !!}" class="pull-right" height="80" />
																</a>
															</div>
															
															@endif
															@endforeach
															</div>
															<br/>
															@endif
												</td>
                                            </tr>
											
											<tr>
												<th>@lang('branches.label.desc')</th>
												<td> {{ $item->desc }} </td>
											</tr>
										   
										</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection


     
	 
