@extends('layouts.apex')

@section('title',trans('constant.label.prefill_image_value'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('constant.label.prefill_image_value') </div>
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/prefill-images') }}" title="Back">
                            <button class="btn btn-raised btn-success round btn-min-width mr-1 mb-1"><i class="fa fa-angle-left" aria-hidden="true"></i> @lang('common.label.back')
                            </button>
                        </a>
	                <div class="actions pull-right">
                          
                        </div>
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
					
					
	                     {!! Form::open(['url' => '/admin/prefill-images', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                                @include ('admin.prefill-images.form')

                                {!! Form::close() !!}
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection
