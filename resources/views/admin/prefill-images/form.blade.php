

<div class="row ">

    
    <div class="col-md-6">
      
        
       @if(!isset($item))

        <div class="form-group {{ $errors->has('slug') ? 'has-error' : ''}}">
            <label for="slug" class="">
                <span class="field_compulsory">*</span>@lang('constant.label.slug')
            </label>
			
                {!! Form::select('slug', trans('constant.static_image'), null,['class' => 'form-control']) !!}
				{!! $errors->first('slug', '<p class="help-block text-danger">:message</p>') !!}

        </div>
		@else
			{!! Form::hidden('slug',$item->slug) !!}
		@endif
		
		{!! Form::hidden('responce_type','static_images') !!}
		
		<div class="form-group  {{ $errors->has('title') ? ' has-error' : ''}}">
			<label for="title" >
				<span class="field_compulsory">*</span>
				@lang('check-list.label.title')
			  
			</label>
			<div class="">
				{!! Form::text('title', null, ['class' => 'form-control']) !!}
				{!! $errors->first('title', '<p class="help-block text-danger">:message</p>') !!}
			</div>
		</div>

        

		<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
			<label for="logo"> @lang('constant.label.image_info') </label>
			<div class="">
				@if(isset($item) && $item->refefile)
				<div class="row">
				@php ( $rf=$item->refefile )
				@if($rf->file_thumb_url && $rf->file_thumb_url != "")
				<div class="col-sm-2 relative-container">
					<a href="{{url('admin/reference-file/'.$rf->id.'/delete')}}" onclick="return confirm('@lang('common.js_msg.confirm_for_delete',['item_name'=>trans('common.label.file')])')" class="close btn btn-danger btn-sm"><i class='fa fa-trash' aria-hidden='true'></i></a>
					<a class="example-image-link " href="{!! $rf->file_url !!}" data-lightbox="example-2" data-title="{{$rf->refe_file_real_name}}">
					<img src="{!! $rf->file_thumb_url !!}" height="80" />
					</a>
				</div>
				
				@endif
				
				</div>
				<br/>
				@endif
				{!! Form::file('image',  ['class' => 'form-control']) !!}
				{!! $errors->first('image', '<p class="help-block text-danger">:message</p>') !!}
			</div>
		</div>
		
		<?php
			$selected_q = [];
			$selected_pm = [];
			$other_information_a = "";
			$other_information_b = "";
			if(isset($item)){
				foreach($item->accessories as $val){
					if($val->desc && $val->desc != ""){
						$selected_q[] = $val->desc;
					}
				}
				foreach($item->pfmaterials as $val){
					if($val->desc && $val->desc != ""){
						$selected_pm[] = $val->desc;
					}
				}
				if($item->spinfoa){
					$other_information_a = $item->spinfoa->desc;
				}
				if($item->spinfob){
					$other_information_b = $item->spinfob->desc;
				}
			}
			if(count($selected_q) <=0){ $selected_q = [""]; }
			if(count($selected_pm) <=0){ $selected_pm = [""]; }
			

		?>
		
		@if($item && $item->slug == "accessories")
			
		<div class="multi-group {{ $errors->has('accessories') ? 'has-error' : ''}}">
			@foreach($selected_q as $k => $q)
			@if($k%2 == 0)
			<div class="form-group ">
				<label for="password" class="title">
				   @lang('survey.label.accessories')
				</label> 
				 <label  class="action_btn pull-right">
				   <a href="#" class="add_row" ><i style="padding: 8px;background-color: black;border-radius: 50%;" class='fa fa-plus'></i></a>
				   <a href="#" class="remove_row" ><i style="padding: 8px;background-color: black;border-radius: 50%;" class='fa fa-minus'></i></a>
				</label> 
				<div class="row">
				<div class="col-md-6">
				{!! Form::text('accessories[]', $q,['placeholder'=>trans('survey.label.accessories'),'class' => 'form-control']) !!}
				</div>
				<div class="col-md-6">
				{!! Form::text('accessories[]', (isset($selected_q[$k+1]))? $selected_q[$k+1] : "",['placeholder'=>trans('survey.label.accessories'),'class' => 'form-control']) !!}
				</div>
				</div>
				
			</div>
			@endif
			@endforeach
			
			{!! $errors->first('accessories', '<p class="help-block text-danger">:message</p>') !!}
		</div>
        @endif
		
		@if($item && $item->slug == "other_info_image_1")
		
		<div class="multi-group {{ $errors->has('professional_materials') ? 'has-error' : ''}}">
			@foreach($selected_pm as $k => $q)
			
			<div class="form-group ">
				<label for="password" class="title">
				   @lang('survey.label.def_professional_materials')
				</label> 
				 <label  class="action_btn pull-right">
				   <a href="#" class="add_row" ><i style="padding: 8px;background-color: #7235a2;border-radius: 50%;" class='fa fa-plus'></i></a>
				   <a href="#" class="remove_row" ><i style="padding: 8px;background-color: #7235a2;border-radius: 50%;" class='fa fa-minus'></i></a>
				</label> 
				<div class="row">
				<div class="col-md-12">
				{!! Form::text('professional_materials[]', $q,['placeholder'=>trans('survey.label.professional_materials'),'class' => 'form-control']) !!}
				</div>
				</div>
				
			</div>
		
			@endforeach
			
			{!! $errors->first('professional_materials', '<p class="help-block text-danger">:message</p>') !!}
		</div>
        
		@endif
		
		
		@if($item && $item->slug == "other_info_image_3")
			
		<div class="form-group{{ $errors->has('other_information_a') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.def_other_information_a')
            </label>
            <div >
				
                {!! Form::textarea('other_information_a', ($other_information_a)? $other_information_a : null , ['id'=>'other_information_a','class' => 'form-control']) !!}
				{!! $errors->first('other_information_a', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>
		
		@endif
		
		
		@if($item && $item->slug == "other_info_image_2")
			
		<div class="form-group{{ $errors->has('other_information_b') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('survey.label.def_other_information_b')
            </label>
            <div >
                {!! Form::textarea('other_information_b', ($other_information_b)? $other_information_b : null , ['id'=>'other_information_b','class' => 'form-control']) !!}
				{!! $errors->first('other_information_b', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>

        @endif
      
        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('common.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
   
        
    </div>
   
    
</div>



@push('js')
<script>

	var urla ="{{ url('/admin/check-list') }}";
	
	$(document).on('click', '.add_row', function (e) {
        var rootc = $(this).parents('.multi-group');
        var rowdata = $(this).parents('.form-group');
		$(rootc).append("<div class='form-group'>"+rowdata.html()+"</div>");
		$(rootc).children(".form-group").last().find("input[type=text]").val("");
		return false;
    });

	$(document).on('click', '.remove_row', function (e) {
        var rowdata = $(this).parents('.form-group');
		rowdata.remove();
		return false;
    }); 
   
</script>


@endpush

