@extends('layouts.apex')

@section('title',trans('location.label.location_text')))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('location.label.location_text') </div>
               
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/check-list') }}" title="@lang('common.label.back')">
                            <button class="btn btn-raised btn-success round btn-min-width mr-1 mb-1"><i class="fa fa-angle-left" aria-hidden="true"></i> @lang('common.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                      </div>  
                          
                        
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                   <div class="table-responsive custom-table-responsive">
                                        <table class="table table-striped">
                                        <tbody>
											<tr>
												<th># @lang('common.label.id') </th>
												<td> {{$item->id}} </td>
											</tr>
											<tr>
												<th>@lang('constant.label.type')</th>
												<td> {{ $item->responce_type }} </td>
											</tr>
											<tr>
												<th>@lang('check-list.label.title')</th>
												<td> {{ $item->title }} </td>
											</tr>
											<tr>
												<th>@lang('constant.label.value')</th>
												<td> <pre> {{ $item->desc }} </pre></td>
											</tr>
											<tr>
												<th>@lang('common.label.create')</th>
												<td> {{ $item->created_exp }} </td>
											</tr>
										   
										</tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection


     
	 
