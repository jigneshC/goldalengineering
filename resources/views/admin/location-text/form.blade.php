

<div class="row ">

    
    <div class="col-md-6">
      
        
       

        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
            <label for="first_name" class="">
                <span class="field_compulsory">*</span>@lang('constant.label.type')
            </label>
                {!! Form::select('responce_type', ['location'=>trans('common.responce_type.location')], null,['class' => 'form-control']) !!}
				{!! $errors->first('responce_type', '<p class="help-block text-danger">:message</p>') !!}

        </div>
        <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('constant.label.value')
            </label>
            <div >
                {!! Form::textarea('desc', old('desc',null) , ['class' => 'form-control','id'=>'desc']) !!}
				{!! $errors->first('desc', '<p class="help-block text-danger">:message</p>') !!}
            </div>
        </div>

        
        

        
      
        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('common.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('common.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
   
        
    </div>
   
    
</div>



@push('js')
<script>

	var urla ="{{ url('/admin/constants') }}";
   
</script>


@endpush

