<!-- Global stylesheets -->

<link href="{!! asset('public/apex/fonts/feather/style.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('public/apex/fonts/simple-line-icons/style.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('public/apex/fonts/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('public/apex/vendors/css/perfect-scrollbar.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('public/apex/vendors/css/prism.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('public/apex/vendors/css/switchery.min.css') !!}" rel="stylesheet" type="text/css">
<link href="{!! asset('public/apex/vendors/css/sweetalert2.min.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<link href="{!! asset('public/apex/css/app.css') !!}" rel="stylesheet" type="text/css"> 
<link href="{!! asset('public/apex/css/custome.css') !!}" rel="stylesheet" type="text/css"> 


<link href="{!! asset('public/apex/stylesheets/plugins/select2/select2.css') !!}" media="all" rel="stylesheet"  type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{!! asset('public/apex/vendors/css/tables/datatable/datatables.min.css') !!}">

{{-- <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet"> --}}
<link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<link href="{!! asset('public/css/bootstrap-datetimepicker.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{!! asset('public/apex/build/css/intlTelInput.css')!!}">
<link href="{!! asset('public/apex/stylesheets/demo.css') !!}" media="all" rel="stylesheet" type="text/css"/>
<link href="{!! asset('public/assets/lightbox/css/lightbox.css') !!}" media="all" rel="stylesheet" type="text/css"/>


