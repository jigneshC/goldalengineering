@extends('email.mailtemplate.cit')

@section('body')
    @if(isset($user))
        <h2 class="title">@lang('email.label.hello_name',['name'=>$user->full_name],'he') </h2>
    @endif

    <p>
        
         @if($action == "accepted")
			@lang('email.slug.your_assign_surve_request_accepted',['name'=>$user->full_name],'he')
		@else
			@lang('email.slug.your_assign_surve_request_declined',['name'=>$user->full_name],'he')
		@endif
		<br/><br/>
		<b> @lang('survey.label.survey_detail') </b>	
		 <p class="pr1">
		@lang('common.label.id'):  {{ $item->id }} <br/>
		@lang('survey.label.name'): {{ $item->owner_name }} <br/>
		@lang('survey.label.email'): {{ $item->owner_email }} <br/>
		@lang('survey.label.address'): {{ $item->address }} <br/>
		</p>
		<br/>
		
        
		
		
		<br/>
    </p>
    
    <hr>
    
@endsection