@extends('email.mailtemplate.cit')

@section('body')
    @if(isset($creator))
        <h2 class="title">@lang('email.label.hello_name',['name'=>$creator->full_name],'he') </h2>
    @endif

    <p>
        
        @lang('email.slug.you_have_been_assign_survey',['name'=>$user->full_name],'he')
		<br/><br/>
		<b> @lang('survey.label.survey_detail') </b>	
		 <p class="pr1" style="direction: rtl;text-align:right;">
		@lang('common.label.id'):  {{ $item->id }} <br/>
		@lang('survey.label.name'): {{ $item->owner_name }} <br/>
		@lang('survey.label.email'): {{ $item->owner_email }} <br/>
		@lang('survey.label.address'): {{ $item->address }} <br/>
		</p>
		<br/>
		
		@lang('email.slug.assign_further_action',[],'he')
        
		
    </p>
    
    <hr>
    
@endsection