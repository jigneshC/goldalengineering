@extends('email.mailtemplate.cit')

@section('body')
    
    <h2 class="title">@lang('email.label.hello_name',['name'=>$user->full_name],'he') ,  @lang('email.label.welcome_to_app',['app'=>\config('admin.APP_NAME')],'he')   </h2>
	<p>
	@lang('email.label.your_app_account_created',['app'=>\config('admin.APP_NAME')],'he')
	</p>

<h4 style="margin-top: 15px;margin-bottom: 10px;"><b>@lang('email.label.your_login_credencial',[],'he')</b></h4>
<div class="table-responsive">
<table class=" email-table">
    <tr style="background-color:#f2f2f2" >
        <th>@lang('user.label.email',[],'he') </th>
        <td>{{ $alldata['email'] }}</td>
    </tr>
   
    <tr>
        <th>@lang('user.label.password',[],'he')</th>
        <td>{{ $alldata['password'] }}</td>
    </tr>
     
   
</table>
</div>
	
	
    
    
    
@endsection

