<?php

return [


    'label'=>[
        'branches'=>'Branches',
        'branch'=>'Branche',
        'create_branch'=>'Create branch',
		'edit_branch'=>'Edit branch',
        'logo_image'=>'Logo Image',
        'name'=>'name',
        'desc'=>'Description',
        'title_1'=>'Title 1',
        'title_2'=>'Title 2',
        'phone_1'=>'Phone 1',
        'phone_2'=>'Phone 2',
        'email_1'=>'Email 2',
        'email_2'=>'Email 2',
        'is_fullsizelogo'=>'Enable full width',
	]
];