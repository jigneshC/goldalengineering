<?php

return [


    'subject'=>[
        'reset_password'=>'Forgot Password!',
        'surveyour_account_detail'=>'surveyour account detail',
		'new_survey_assign_request'=>"New request to assign survey",
		'accept_survey_assign_request'=>"You survey assign request accepted",
		'decline_survey_assign_request'=>"You survey assign request declined",
      
    ],
	'slug'=>[
		'you_have_request_to_forgot_password'=>'You have recently request to reset password  for :app account.',	
		'your_temp_password_to_reset_password_is'=>'You can login with new password :password . After login you can change/reset password as you want.',	
		'your_otp_to_reset_password_is'=>'Your one time password (otp) to reset your password is',	
		'if_you_did_not_request'=>"If you didn't request this then ignor this email or let us know.",
		
		'you_have_been_assign_survey'=>" :name has assigned you a survey",	
		'assign_further_action'=>" To continue with the survey , you can accept assign request and download it from the app.",	
		'your_assign_surve_request_accepted'=>" :name  accepted survey request,assigned by you.",	
		'your_assign_surve_request_declined'=>" :name  declined survey request,assigned by you.",	
	],
	'label'=>[
		'hello_name'=>"Hello :name ",
		'welcome_to_app'=>"Welcome to :app ",
		'your_app_account_created'=>"Your :app surveyour account created succesfully",
		'your_login_credencial'=>"Your login credencial",
	]
	
];