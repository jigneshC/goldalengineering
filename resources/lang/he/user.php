<?php

return [


    'label'=>[
        'users'=>'משתמשים',
        'user'=>'משתמש',
        'id'=>'מספר סידורי',
        'name'=>'שם',
        'email'=>'דוא"ל',
        'role'=>'תפקיד',
        'password'=>'סיסמה',
        'create_new_user'=>'צור משתמש חדש',
        'create_user'=>'צור משתמש',
        'edit_user'=>'ערוך משתמש',
        'show_user'=>'הצג משתמש',
        'select_user'=>'בחר משתמש',
        'profile'=>'פרופיל',
		'logout'=>'להתנתק',
		'first_name' => 'שם פרטי',
        'last_name' => 'שם משפחה',
		'my_profile' => 'הפרופיל שלי',
        'edit_profile' => 'ערוך פרופיל',
        'update_profile' => 'עדכן פרופיל',
        'change_password' => 'שנה סיסמא',
        'current_password' => 'סיסמה נוכחית',
        'password_confirmation' => 'אימות סיסמה',
		'language' => 'שפה',
        'joined' => 'הצטרף',
		'select_surveyor' => 'בחר מודד',
		'view_survey' => 'הצג סקר',
		'all_surveyor' => 'כל מודד',
		'signature' => 'חתימה',
		'crop_signature' => 'אשר קובץ חתימה כאן!',
		'licence_no' => 'מספר רישיון',
		'qualification' => 'הכשרה',
		'experience' => 'ניסיון',
		'address' => 'כתובת',
	],
	'responce_msg' =>[
        'password_changed_success'=>'סיסמה שונתה בהצלחה.',
		'password_not_match_with_current'=>'הזן את הסיסמה הנוכחית הנכונה.',
    ],
];



