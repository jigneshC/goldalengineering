<?php

return [


    'label'=>[
        'categories'=>'פרקים',
        'category'=>'פרק',
        'no_parent'=>'ללא הורה',
        'child_categories'=>'סעיפי הפרק',
        'parent_categories'=>'רשימת פרקים',
        'create_categories'=>'צור פרק / סעיף',
        'edit_categories'=>'ערוך פרק',
        'show_categories'=>'הצג פרק',
		'display_order'=>'סדר תצוגה',
		 'category_images'=>'קטגוריה תמונות',
		 'default_price'=>'מחיר ברירת מחדל',
		 'quote_desc'=>'צטט פירוט תמונה',
		 'create_survey_category'=>'צור קטגוריית סקר',
		'category_name'=>'שם קטגוריה',
		'create_survey_subchapter'=>'סעיף חריג חדש',
		'sub_chapter'=>'תת-פרק',
    ],
	
];