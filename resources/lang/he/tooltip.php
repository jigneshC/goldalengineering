<?php

return [

    'common'=>[
        'label'=>[
            'action'=>'מחיקת עריכה',
            'description'=>'תיאור',
        ],
        'icon' =>[
            'edit'=>'ערוך רשומה',
            'delete'=>'מחק את הרשומה',
            'eye'=>'צפה בפרטים',
            'recover'=>'שחזר את הרשומה שנמחקה',
			'export_record'=>' יצא ל WORD',
			'export_advance'=>'דוחות נוספים',
        ],
    ]

];
