<?php

return [


    'label'=>[
        'constant_text'=>'עריכת ערכים קבועים',
		'prefill_image_value'=>'הוסף מראש תמונות לתמונות',
		'image_info'=>'תמונה עם מידע',
        'type'=>'סוג',
        'slug'=>'נושא',
        'value'=>'תוכן',
		'responce_type'=>'סוג',
        'desc'=>'תוכן',
		'select_slug'=>'בחר נושא',
		'allow_customize'=>'לאפשר התאמה אישית?',
    ],
	'slug'=>[
		'property_description'=>'תיאור הנכס',	
		'legal_thing_a'=>'דבר משפטי ',	
		'legal_thing_isa'=>'מידע תומך מבדק',	
		'surveyor_note'=>'הערה',	
		'reviews_and_summary'=>'הערות וסיכומים',	
		'not_tested'=>'לא נבחן',	
		'general_info'=>'כללי הבניה מידע כללי',	
		'other_info'=>'מידע אחר',	
		'report_declaration'=>'הצהרה בדו"ח',	
		//'report_general_info'=>'מידע כללי בדו"ח',
		'report_focus_note'=>'הערות מקדמיות',
		'check_title_1'=>'כותרת 1',
		'check_title_2'=>'כותרת 2',
		'check_title_3'=>'כותרת 3',
		'check_title_4'=>'כותרת 4',
	],
	'static_image'=>[
		'other_info_image_1'=>'תמונה 1',
		'other_info_image_2'=>'תמונה 2',
		'other_info_image_3'=>'תמונה 3',
		'signature'=>'תמונת חתימה',
	],
	'responce_msg' => [
		'success_default' => 'הצלחה!',
        'success_login' => 'התחברות הצלחה.',
        'success_logout' => 'יצאת בהצלחה!',
        'success_forgot_password_send_to_email' => 'סיסמה אחת (otp) לאיפוס הסיסמה נשלחה לכתובת הדוא"ל הרשומה שלך.',
		'success_new_password_generated' => 'סיסמה חדשה יש דבורה שנוצר! ונשלח לכתובת הדוא"ל הרשומה שלך.',
        'success_forgot_password_send_to_phone_no' => 'סיסמה אחת נשלחה על מספר הטלפון שלך.',
        'success_password_changed' => 'הצלחה! סיסמתך שונתה!',
        'success_register' => 'משתמש רשום בהצלחה.',
        'success_profile_updated' => 'הפרופיל עודכן בהצלחה.',
        
		
        'warning_user_data_not_found' => 'נתוני המשתמש לא נמצאו.',
        'warning_no_data_found' => 'לא נמצאו רשומות.',
        'warning_your_account_not_activated_yet' => 'החשבון שלך לא הופעל עדיין.',
        'warning_incorrect_email_or_password' => 'האימייל או הסיסמא שגויים',
        'warning_require_unique_phone_number' => 'מספר הטלפון כבר קיים עם חשבון אחר. נסה עם מספר אחר',
        'warning_require_unique_user_name' => 'שם משתמש כבר קיים. נסה עוד אחד.',
		'warning_current_password_incorrect' => 'הסיסמה הנוכחית שלך אינה תואמת את הרשומות שלנו.',
		
		
        'error_default'=>'משהו השתבש!',
		
		
	],
];