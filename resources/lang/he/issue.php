<?php

return [


    'label'=>[
		'copy_from'=>'תעתיק מ',
		'from_survey_id'=>'מהסקר',
        'issue'=>'נושא',
        'issues'=>'נושאים',
        'issue_detail'=>'פירוט הבעיה',
        'issue_images'=>'הנפקה של תמונות',
        'issue_image'=>'תמונת בעיה',
        'category'=>'פרק',
        'child_category'=>'סעיף',
        'is_consultants'=>'האם יש יועצים',
        'unit_cost'=>'מחיר ליחידה',
        'number_of_unit'=>'מספר יחידות',
		'total_cost_bck'=>'עלות כוללת',
		'total_cost'=>'סך הכל עלות כוללת לסעיף',
		'total_of_the_issue'=>'סך הכל :place',
        'location'=>'מקום',
        'recommendation'=>'נדרש',
        'quote'=>'ציטוט',
        'note'=>'הערה',
        'image'=>'תמונה',
        'images'=>'תמונות',
        'images.*'=>'תמונות',
		'issue_in_survey'=>'סוגיות בסקר',
		'issue_edit'=>'ערוך סעיף',
		'add_issue'=>'הוסף סעיפים',
		'cost_detail'=>'סעיף דקל ',
		'issue_image_update'=>'עדכן תמונות גיליון',
        'select_image_to_update'=>'בחר תמונה לעדכון',
        'update_image'=>'עדכן תמונה',
        'add_circle'=>'הוסף מעגל',
        'add_arrow'=>'הוסף חץ',
        'add_text'=>'הוסף טקסט',
        'add_time'=>'הוסף זמן',
        'change_dimension'=>'שינוי ממד',
		'image_height'=>'גובה התמונה',
        'image_width'=>'רוחב תמונה',
        'enter_text'=>'הזן טקסט',
		'show_default_price_detail'=>'טען נתוני ברירת מחדל',
		'cost_detail_report_flag'=>'להראות בדוח?',
		'show_in_report'=>'להראות בדוח?'
	]
	
];