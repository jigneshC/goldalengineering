<?php

return [
    'label' => [
        'Users' => 'משתמשים',
        'Roles' => 'תפקידים',
        'Constants' => 'שדות קבועים',
        'Categories' => 'רשימת הסעיפים',
		'Survey' => 'סקר',
		'SurveyMerge' => 'מיזוג סקר',
		'Branches' => 'חברה',
		'PrefillValues' => 'עריכת ערכים מוגדרים מראש',
		'Location' => 'מיקום',
		'Create' => 'הוסף חדש',
        'List' => 'רשימה',
        'check-list' => "צ'ק ליסט",
		'prefill-images' => 'תמונות ממולאות מראש',
    ],

];