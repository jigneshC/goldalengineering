

    The AAAA must be accepted.
    The AAAA is not a valid URL.
    The AAAA must be a date after DDATE.
    The AAAA must be a date after or equal to DDATE.
    The AAAA may only contain letters.
    The AAAA may only contain letters, numbers, dashes and underscores.
    The AAAA may only contain letters and numbers.
    The AAAA must be an array.
    The AAAA must be a date before DDATE.
    The AAAA must be a date before or equal to DDATE.
    The AAAA must be between MMIN and MMAX.
    The AAAA must be between MMIN and MMAX kilobytes.
    The AAAA must be between MMIN and MMAX characters.
    The AAAA must have between MMIN and MMAX items.
    
    The AAAA field must be true or false.
    The AAAA confirmation does not match.
    The AAAA is not a valid date.
    The AAAA does not match the format FFORMAT.
    The AAAA and OOTHER must be different.
    The AAAA must be DDIGITS digits.
    The AAAA must be between MMIN and MMAX digits.
    The AAAA has invalid image dimensions.
    The AAAA field has a duplicate value.
    The AAAA must be a valid email address.
    The selected AAAA is invalid.
    The AAAA must be a file.
    The AAAA field must have a value.
    
    The AAAA must be greater than VVALUE.
    The AAAA must be greater than VVALUE kilobytes.
    The AAAA must be greater than VVALUE characters.
    The AAAA must have more than VVALUE items.
    
    
    The AAAA must be greater than or equal VVALUE.
    The AAAA must be greater than or equal VVALUE kilobytes.
    The AAAA must be greater than or equal VVALUE characters.
    The AAAA must have VVALUE items or more.
    
    The AAAA must be an image.
    The selected AAAA is invalid.
    The AAAA field does not exist in OOTHER.
    The AAAA must be an integer.
    The AAAA must be a valid IP address.
    The AAAA must be a valid IPv4 address.
    The AAAA must be a valid IPv6 address.
    The AAAA must be a valid JSON string.
        
    The AAAA must be less than VVALUE.
    The AAAA must be less than VVALUE kilobytes.
    The AAAA must be less than VVALUE characters.
    The AAAA must have less than VVALUE items.
    
        
    The AAAA must be less than or equal VVALUE.
    The AAAA must be less than or equal VVALUE kilobytes.
    The AAAA must be less than or equal VVALUE characters.
    The AAAA must not have more than VVALUE items.
    
        
    The AAAA may not be greater than MMAX.
    The AAAA may not be greater than MMAX kilobytes.
    The AAAA may not be greater than MMAX characters.
    The AAAA may not have more than MMAX items.
    
    The AAAA must be a file of type: VVALUEs.
    The AAAA must be a file of type: VVALUEs.
    
    The AAAA must be at least MMIN.
    The AAAA must be at least MMIN kilobytes.
    The AAAA must be at least MMIN characters.
    The AAAA must have at least MMIN items.
    
    The selected AAAA is invalid.
    The AAAA format is invalid.
    The AAAA must be a number.
    The AAAA field must be present.
    The AAAA format is invalid.
    The AAAA field is required.
    The AAAA field is required when OOTHER is VVALUE.
    The AAAA field is required unless OOTHER is in VVALUEs.
    The AAAA field is required when VVALUEs is present.
    The AAAA field is required when VVALUEs is present.
    The AAAA field is required when VVALUEs is not present.
    The AAAA field is required when none of VVALUEs are present.
    The AAAA and OOTHER must match.
    
    The AAAA must be SSIZE.
    The AAAA must be SSIZE kilobytes.
    The AAAA must be SSIZE characters.
    The AAAA must contain SSIZE items.
    
    The AAAA must be a string.
    The AAAA must be a valid zone.
    The AAAA has already been taken.
    The AAAA failed to upload.
    The AAAA format is invalid.

    


