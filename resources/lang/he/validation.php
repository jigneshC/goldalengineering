<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'יש לקבל את ה - :attribute.',
    'active_url'           => 'ה- :attribute אינו כתובת אתר חוקית.',
    'after'                => 'ה- :attribute חייב להיות תאריך לאחר :date.',
    'after_or_equal'       => 'ה- :attribute חייב להיות תאריך לאחר או שווה ל- :date.',
    'alpha'                => 'ה- :attribute יכול להכיל רק אותיות.',
    'alpha_dash'           => 'ה- :attribute יכול להכיל רק אותיות, מספרים, מקפים וקווים תחתונים.',
    'alpha_num'            => 'ה- :attribute יכול להכיל רק אותיות ומספרים.',
    'array'                => 'ה- :attribute חייב להיות מערך.',
    'before'               => 'ה- :attribute חייב להיות תאריך לפני :date.',
    'before_or_equal'      => 'ה- :attribute חייב להיות תאריך לפני או שווה ל- :date.',
    'between'              => [
        'numeric' => 'ה- :attribute חייב להיות בין :min ו- :max.',
        'file'    => ':attribute חייב להיות בין :min ו :max קילובייט.',
        'string'  => 'ה- :attribute חייב להיות בין תווים :min ו- :max.',
        'array'   => 'ה- :attribute חייב להיות בין פריטים :min ו- :max.',
    ],
    'boolean'              => 'שדה :attribute חייב להיות אמיתי או שקר.',
    'confirmed'            => 'אישור :attribute אינו תואם.',
    'date'                 => 'ה- :attribute אינו תאריך חוקי.',
    'date_format'          => ':attribute אינו תואם את פורמט :format.',
    'different'            => ':attribute ו אחרים חייב להיות שונה.',
    'digits'               => 'ה- :attribute חייב להיות ספרות :digits.',
    'digits_between'       => 'ה- :attribute חייב להיות בין ספרות :min ו- :max.',
    'dimensions'           => 'ל- :attribute יש ממדי תמונה לא חוקיים.',
    'distinct'             => 'בשדה :attribute יש ערך כפול.',
    'email'                => 'ה- :attribute חייב להיות כתובת דוא"ל חוקית.',
    'exists'               => 'ה- :attribute שנבחר אינו חוקי.',
    'file'                 => 'ה- :attribute חייב להיות קובץ.',
    'filled'               => 'שדה :attribute חייב להיות בעל ערך.',
    'gt'                   => [
        'numeric' => '    ה- :attribute חייב להיות גדול מ- :value.',
        'file'    => '    ה- :attribute חייב להיות גדול מ- :value קילו-בתים.',
        'string'  => '    ה- :attribute חייב להיות גדול מתווים :value.',
        'array'   => '    ה- :attribute חייב לכלול יותר מ- :value פריטים.',
    ],
    'gte'                  => [
        'numeric' => '    ה- :attribute חייב להיות גדול מ- :value או שווה לו.',
        'file'    => '    :attribute חייב להיות גדול או שווה :value קילו-בתים.',
        'string'  => '    ה- :attribute חייב להיות גדול או שווה לתווי :value.',
        'array'   => '    :attribute חייב להיות פריטים :value או יותר.',
    ],
    'image'                => 'ה- :attribute חייב להיות תמונה.',
    'in'                   => 'ה- :attribute שנבחר אינו חוקי.',
    'in_array'             => 'שדה :attribute אינו קיים ב- :other.',
    'integer'              => 'ה- :attribute חייב להיות מספר שלם.',
    'ip'                   => 'ה- :attribute חייב להיות כתובת IP חוקית.',
    'ipv4'                 => 'ה- :attribute חייב להיות כתובת IPv4 חוקית.',
    'ipv6'                 => 'ה- :attribute חייב להיות כתובת IPv6 חוקית.',
    'json'                 => 'ה- :attribute חייב להיות מחרוזת JSON חוקית.',
    'lt'                   => [
        'numeric' => 'ה- :attribute חייב להיות פחות מ- :value.',
        'file'    => 'ה- :attribute חייב להיות פחות מ- :value קילו-בתים.',
        'string'  => 'ה- :attribute חייב להיות פחות מ- :value תווים.',
        'array'   => ':attribute חייב להיות פחות פריטים :value.',
    ],
    'lte'              => [
        'numeric' => 'ה- :attribute חייב להיות פחות או שווה ל- :value.',
        'file'    => ':attribute חייב להיות פחות או שווה :value קילו-בתים.',
        'string'  => 'ה- :attribute חייב להיות פחות או שווה לתווי :value.',
        'array'   => 'ל- :attribute אסור שיהיה יותר מ- :value פריטים.',
    ],
    'max'              => [
        'numeric' => 'ייתכן שה- :attribute לא יעלה על :max.',
        'file'    => 'ייתכן שה- :attribute לא יעלה על קילו-בתים של :max.',
        'string'  => 'ייתכן שה- :attribute לא יעלה על תווים :max.',
        'array'   => ':attribute אולי לא יותר מאשר פריטים :max.',
    ],
    'mimes'                => 'ה- :attribute חייב להיות קובץ מסוג: :values.',
    'mimetypes'            => 'ה- :attribute חייב להיות קובץ מסוג: :values.',
    'min'              => [
        'numeric' => 'ה- :attribute חייב להיות לפחות :min.',
        'file'    => 'ה- :attribute חייב להיות לפחות קילומטר :min.',
        'string'  => 'ה- :attribute חייב להיות לפחות תווים :min.',
        'array'   => ':attribute חייב להיות לפחות פריטים :min.',
    ],
    'not_in'               => 'ה- :attribute שנבחר אינו חוקי.',
    'not_regex'            => 'פורמט :attribute אינו חוקי.',
    'numeric'              => 'ה- :attribute חייב להיות מספר.',
    'present'              => 'שדה :attribute חייב להיות נוכח.',
    'regex'                => 'פורמט :attribute אינו חוקי.',
    'required'             => 'שדה :attribute נדרש.',
    'required_if'          => 'שדה :attribute נדרש כאשר :other הוא :values.',
    'required_unless'      => 'שדה ה- :attribute נדרש, אלא אם ה- :other נמצא ב- :values.',
    'required_with'        => 'שדה :attribute נדרש כאשר :values קיים.',
    'required_with_all'    => 'שדה :attribute נדרש כאשר :values קיים.',
    'required_without'     => 'שדה :attribute נדרש כאשר :values אינו קיים.',
    'required_without_all' => 'שדה :attribute נדרש כאשר לא קיימים :values.',
    'same'                 => 'את :attribute ו אחרים חייב להתאים.',
    'size'                 => [
        'numeric' => 'ה- :attribute חייב להיות :size.',
        'file'    => 'ה- :attribute חייב להיות :size קילובייט.',
        'string'  => 'ה- :attribute חייב להיות :size תווים.',
        'array'   => 'ה- :attribute חייב להכיל פריטים :size.',
    ],
    'string'               => 'ה- :attribute חייב להיות מחרוזת.',
    'timezone'             => 'ה- :attribute חייב להיות אזור חוקי.',
    'unique'               => ':attribute כבר נלקח.',
    'uploaded'             => 'טעינת ה- :attribute נכשלה.',
    'url'                  => 'פורמט :attribute אינו חוקי.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
