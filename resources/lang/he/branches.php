<?php

return [


    'label'=>[
        'branches'=>'חברות',
        'branch'=>'חברה',
        'create_branch'=>'צור חברה',
        'edit_branch'=>'ערוך חברה',
        'logo_image'=>'תמונת לוגו',
        'name'=>'שם',
        'desc'=>'תיאור',
		'title_1'=>'כותרת 1',
        'title_2'=>'כותרת 2',
        'phone_1'=>'טלפון 1',
        'phone_2'=>'טלפון 2',
        'email_1'=>'דוא"ל 1',
        'email_2'=>'דוא"ל 2',
		'is_fullsizelogo'=>'אפשר רוחב מלא',
	]
];