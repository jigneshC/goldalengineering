<?php

return [


    'label'=>[
		'all'=>'את כל',
		'print'=>'יצא להדפסה',
        'add_new'=>'הוסף חדש',
        'action'=>'פעולה',
        'cancel'=>'בטל',
        'save'=>'שמור',
        'update'=>'עדכון',
        'submit'=>'שלח',
        'create'=>'צור',
        'status'=>'סטטוס', 
        'back'=>'חזרה לרישום',
        '_back'=>'חזור',
        'clear_form'=>'אפס',
        'filter_reset'=>'לאפס',
        'delete'=>'מחק',
        'created'=>'נוצר',
        'edit' => "ערוך",
        'logo_image' => "תמונת לוגו",
		'quote_image' => "תמונת ציטוט",
        'name' => "שם",
        'id' => "מספר סדורי",
        'file' => "קובץ",
        'remember_me'=> 'זכור אותי',
        'login' => 'התחברות',
        'email'=> 'דואר אלקטרוני',
        'password' => 'סיסמה',
        'signin' => 'היכנס',
        'select_type'=>"בחר סוג",
		'yes'=>"כן",
		'no'=>"לא",
		'go_home'=>"חזור לדף הבית",
		'all_rights'=>"כל הזכויות",
		'Opps'=>"אופס ...",
		'saveandnext'=>"שמור & הבא",
		'next'=>"הבא",
		'previous'=>"קודם",
		'max_limit'=>"אנא בחר תמונות בגודל מקסימלי של 8 מגה בייב ובממד המרבי של 2000 * 2000",
		'deleted_decord'=>'הצג נמחק',
		'recover'=>'שחזור נתונים'
        
    ],
	'active_status'=>[
		'active'=>'פעיל',
		'inactive'=>'לא פעיל',
	],
	'country'=>[
		'english'=>'אנגלית',
		'hebrew'=>'עברית',
	],
	'language'=>[
		'en'=>'אנגלית',
		'he'=>'עברית',
	],
	'responce_type'=>[
        'error'=>'שגיאה',
        'success'=>'הצלחה',
        'notification'=>'הודעה',
        'constant'=>'קבוע',
		'location'=>'מקום',
		'check_list'=>"צ'ק ליסט",
    ],
	'datatable' =>[
        "emptyTable"=> "אין נתונים זמינים בטבלה",
        "infoEmpty"=>"מציג 0 עד 0 מתוך 0 רשומות",
        'search' => 'לחפש',
        'show' => 'הצג',
        'entries' => 'רשומות',
        'showing' => 'מראה', // Showing _START_ to _END_ of _TOTAL_ entries
        'to' => 'ל',
        'of' => 'מתוך',
        'small_entries' => 'רשומות',
        'paginate' => [
            'next' => 'הבא',
            'previous' => 'קודם',
            'first'=>'ראשון',
            'last'=>'אחרון',
        ],

    ],
    'responce_msg' =>[
        'something_went_wrong'=>'משהו השתבש, נסה שוב מאוחר יותר.',
        'something_went_wr'=>'משהו השתבש',
        'you_have_no_permision_to_delete_record'=>'אין לך הרשאה למחוק את הרשומה הזו',
        'record_deleted_succes'=>'הרשומה שנמחקה רשומה',
        'record_updated_succes'=>'הסקירה עודכנה בהצלחה',
        'record_created_succes'=>'הסקירה נוצרה בהצלחה',
        'item_created_success'=>'הפריט נוצר בהצלחה',
		'record_restored_succes'=>'שיא הצלחה משוחזרת',
        'data_not_found'=>'מידע לא נמצא !',
		'page_not_found'=>'מצטערים, הדף שאותו אתה מחפש לא נמצא!',
		'data_upload_success'=>'העלית בהצלחה נתונים',
		'survey_assign_success'=>'הסקר הוקצה בהצלחה!',
		'saction_success'=>'Action success!',
		'image_rotate_success'=>'תמונה סובב הצלחה!',
		
        
    ],
    'js_msg' =>[
        'confirm_for_delete'=>'האם את הבטוח במחיקת הפריט?',
		'confirm_for_delete_data'=>'האם אתה בטוח למחוק נתונים אלה',
		'confirm_for_delete_forever'=>"בלחיצה על הכפתור את מוחק סופית את הפרוייקט מבלי יכולת שיחזור.",
		'action_success'=>'הצלחה!',
        'action_not_procede'=>'פעולה לא פרוצדורה!',
		'confirm_for_local_sync'=>'האם אתה בטוח לאפשר סינכרון מקומי?',
		'confirm_for_image_rotate'=>'האם אתה בטוח לסובב את התמונה הזו?',
		'confirm_for_delete_recover'=>'האם אתה בטוח לשחזר נתונים אלה?'
    ]
];
