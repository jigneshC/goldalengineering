<?php

return [


    'subject'=>[
        'reset_password'=>'שכחת את הסיסמא!',
		'surveyour_account_detail'=>'פרטי חשבון',
		'new_survey_assign_request'=>"בקשה חדשה להקצאת הסקר",
		'accept_survey_assign_request'=>"בקשת הקצאת הסקרים התקבלה",
		'decline_survey_assign_request'=>"בקשת הקצאת הסקר נדחתה",
      
    ],
	'slug'=>[
		'you_have_request_to_forgot_password'=>'לאחרונה ביקשת לאפס סיסמה עבור חשבון :app .',	
		'your_temp_password_to_reset_password_is'=>'אתה יכול להתחבר עם סיסמה חדשה :password. לאחר הכניסה אתה יכול לשנות / לאפס סיסמה כרצונך.',	
		'your_otp_to_reset_password_is'=>'הסיסמה שלך פעם אחת (otp) כדי לאפס את הסיסמה שלך',	
		'if_you_did_not_request'=>'אם לא ביקשת זאת, התעלם מדוא"ל זה או הודע לנו על כך.',	
		'you_have_been_assign_survey'=>" :name הקצה לך סקר",
		'assign_further_action'=>" כדי להמשיך בסקר, תוכל לקבל את בקשת ההקצאה ולהוריד אותה מהאפליקציה.",	
		'your_assign_surve_request_accepted'=>" :name קיבל את בקשת הסקר, שהוקצתה על ידך.",	
		'your_assign_surve_request_declined'=>" :name דחה את בקשת הסקר שהוקצתה על ידך.",	
	],
	'label'=>[
		'hello_name'=>"שלום :name",
		'welcome_to_app'=>"ברוכים הבאים אל  :app ",
		'your_app_account_created'=>"חשבון הסקר של :app נוצר בהצלחה",
		'your_login_credencial'=>"פרטי הכניסה שלך",
	]
	
];