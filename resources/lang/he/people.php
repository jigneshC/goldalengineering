<?php

return [
    'label' => [
        'first_name' => 'שם פרטי',
        'last_name' => 'שם משפחה',
        'my_profile' => 'הפרופיל שלי',
        'edit_profile' => 'ערוך פרופיל',
        'update_profile' => 'עדכן פרופיל',
        'change_password' => 'שנה סיסמא',
        'current_password' => 'סיסמה נוכחית',
        'password_confirmation' => 'אימות סיסמה',
        'language' => 'שפה',
        'joined' => 'הצטרף',
    ],
    
];