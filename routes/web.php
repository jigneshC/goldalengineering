<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('php-info', 'Admin\TestController@phpinfoo');


Route::post('sign-in', 'Auth\LoginController@loginA');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
	Route::get('/','ProfileController@index');
	
	/*Route::get('/profile', 'ProfileController@index');
    Route::get('/profile/edit', 'ProfileController@edit');
    Route::patch('/profile/edit', 'ProfileController@update');
	
	Route::get('/profile/change-password', 'ProfileController@changePassword');
    Route::patch('/profile/change-password', 'ProfileController@updatePassword');*/
	
}); 

Route::get('translate', 'Admin\SettingsController@getLangTranslation');

Route::get('survey/{id}/export', 'Admin\SurveyController@export');


//Route::get('survey/{id}/test', 'Admin\TestController@test');



Route::group(['prefix' => 'admin','middleware' => ['auth', 'roles'],'roles' => 'AU'], function () {
	Route::get('/', 'Admin\AdminController@index');
	Route::get('renamefiles', 'Admin\AdminController@renamefiles');
	Route::get('export', 'Admin\AdminController@export');
	Route::get('reset-file-order', 'Admin\AdminController@fileOrder');
	Route::get('reset-unique-id', 'Admin\AdminController@resetUniqueId');
	Route::get('compare-survey/{id}/{id2}', 'Admin\AdminController@compare');
	
	Route::post('recover-item', 'Admin\AdminController@recoverItem');
	
	Route::get('/categories/img-view', 'Admin\CategoriesController@catImgView');
	Route::get('/categories/search', 'Admin\CategoriesController@search');
	Route::get('/categories/export', 'Admin\CategoriesController@export');
	Route::resource('/categories', 'Admin\CategoriesController');
	
	Route::get('/reference-file/{id}/delete', 'Admin\ReferenceController@destroy');
	Route::get('/reference-file/{id}/rotate', 'Admin\ReferenceController@rotate');
	
	Route::get('/issues-create/{id}', 'Admin\IssueController@create');
	Route::get('/issues-image-edit/{id}', 'Admin\IssueController@editImages');
	Route::post('/issues-image-update', 'Admin\IssueController@updateImages');
	Route::resource('/issues', 'Admin\IssueController');
    Route::get('/issues-data', 'Admin\IssueController@datatable');
	
	Route::post('survey-category', 'Admin\SurveyCatController@store');
	Route::post('survey-local-sync', 'Admin\SurveyControllern@localSync');
	Route::post('survey-duplicate', 'Admin\AdminController@doSurveyDublicate');
	Route::get('survey-duplicate/{id}', 'Admin\AdminController@surveyDublicate');
	Route::get('survey-merge', 'Admin\SurveyMergeController@merge');
	Route::post('survey-merge', 'Admin\SurveyMergeController@mergeSubmit');
	Route::get('survey-search', 'Admin\SurveyMergeController@search');
	Route::get('survey/{id}/detail', 'Admin\SurveyControllern@detail');
	Route::get('survey/{id}/detail', 'Admin\SurveyControllern@detail');
	Route::get('survey/{id}/detail-report', 'Admin\SurveyControllern@detailReport');
	Route::get('survey/{id}/export-advance', 'Admin\SurveyControllern@exportAdvance');
	Route::get('survey/{id}/export', 'Admin\SurveyControllern@export');
	Route::get('survey/{id}/export-by-place', 'Admin\TestController@exportPlace');
	Route::get('survey/{id}/export-pdf', 'Admin\SurveyControllern@exportPdf');
	Route::get('survey/{id}/test-export', 'Admin\TestController@export');
	Route::resource('/survey', 'Admin\SurveyControllern');
    Route::get('/survey-data', 'Admin\SurveyControllern@datatable');
	
	
	Route::resource('/branches', 'Admin\BranchesController');
    Route::get('/branches-data', 'Admin\BranchesController@datatable');
	
	Route::get('/prefill-location', 'Admin\LocationsController@search');
	Route::resource('/locations', 'Admin\LocationsController');
    Route::get('/locations-data', 'Admin\LocationsController@datatable');
	
	Route::resource('/check-list', 'Admin\CheckPointsController');
    Route::get('/check-list-data', 'Admin\CheckPointsController@datatable');
	
	Route::resource('/constants', 'Admin\ResponcetextController');
    Route::get('/constants-data', 'Admin\ResponcetextController@datatable');
	Route::post('/constants-add-title', 'Admin\ResponcetextController@addTitle');
	
	Route::resource('/prefill-images', 'Admin\PrefillImagesController');
    Route::get('/prefill-images-data', 'Admin\PrefillImagesController@datatable');
	
	
	Route::get('roles/datatable', 'Admin\RolesController@datatable');
    Route::resource('/roles', 'Admin\RolesController');
	
	Route::get('/users/search', 'Admin\UsersController@search');
    Route::get('/users/datatable', 'Admin\UsersController@userDatatable');
    Route::resource('/users', 'Admin\UsersController');
	
	Route::resource('permissions', 'Admin\PermissionsController');
	
	
	Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
    Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
    Route::patch('/profile/edit', 'Admin\ProfileController@update');
        //
    Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
    Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');
	
	Route::resource('pages', 'Admin\PagesController');
	Route::resource('activitylogs', 'Admin\ActivityLogsController')->only(['index', 'show', 'destroy']);
	Route::resource('settings', 'Admin\SettingsController');
	Route::get('/prefill-experience', 'Admin\SettingsController@experience');
	
	
	Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
	Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
});  


Route::get('/daily-cron', function()
{
    Artisan::queue('dailyUpdate:run');
});

