<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RefeTextCheckbox extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'refe_constant_text';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['refe_table_field_name', 'refe_field_id', 'desc','refe_code','refe_type','priority'];
	
	


   


}
