<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branches extends Model
{
	use SoftDeletes;
	protected $table = 'branches';

    
    protected $primaryKey = 'id';
	
    protected $guarded = [];

	protected $appends = ['file_url','file_thumb_url'];
    
	
	public function getFileUrlAttribute()
    {
		if($this->refeimg){
			return $this->refeimg->file_url;
		}else{
			return "";
		}
	}
	public function getFileThumbUrlAttribute()
    {
		if($this->refeimg){
			return $this->refeimg->file_thumb_url;
		}else{
			return "";
		}
	}
	
	public function refefile()
    {
        return $this->hasMany('App\Refefile', 'refe_field_id', 'id')->where('refe_table_field_name', 'branch_id');
    }
	public function refeimg()
    {
        return $this->hasOne('App\Refefile', 'refe_field_id', 'id')->where('refe_table_field_name', 'branch_id');
    }
}
