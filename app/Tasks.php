<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
	protected $table = 'tasks';
	
	
    protected $primaryKey = 'id';

    protected $guarded = ['id'];
	
	public function user()
    {
        return $this->belongsTo('App\User');
    }
	
	protected $appends = ['created'];
	
	public function getCreatedAttribute()
    {
        if($this->created_at != "" && $this->created_at){
            return \Carbon\Carbon::parse($this->created_at)->format(session('setting.date_format',\config('settings.date_format_on_app')));
        }
        return $this->created_at;
    }
	
}
