<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Responcetext extends Model
{
	use SoftDeletes;
	protected $table = 'responce_text';
	
	
    protected $primaryKey = 'id';

    protected $guarded = ['id'];
	protected $appends = ['created_exp'];
	
	public function getCreatedExpAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->format("d/m/Y");
        }
		return $this->created_at;
    }
	
	
	public function scopeChecklistonly($query)
    {
        return $query->where('responce_type','=','check_list');
    }
	public function scopeConstantonly($query)
    {
        return $query->where('responce_type','=','constant');
    }
	public function scopeLocationonly($query)
    {
        return $query->where('responce_type','=','location');
    }
	
	public static function rtext($slug){
		
		$rtext=Responcetext::whereSlug($slug)->first();
		if($rtext){
			return $rtext->desc;
		}else{
			return \Lang::get('constant.responce_msg.'.$slug);
		}
		
	}
	public function refefile()
    {
        return $this->hasOne('App\Refefile', 'refe_field_id', 'id')->where('refe_table_field_name', 'rt_id');
    }
	
	
	///////// image to text ////////////
	public function accessories()
    {
        return $this->hasMany('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'rt_id')->where('refe_type', 'default_accessories');
    }
	public function pfmaterials()
    {
        return $this->hasMany('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'rt_id')->where('refe_type', 'default_pf_materials');
    }
	// support info image
	public function spinfoa()
    {
        return $this->hasOne('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'rt_id')->where('refe_type', 'default_spinfoa');
    }
	public function spinfob()
    {
        return $this->hasOne('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'rt_id')->where('refe_type', 'default_spinfob');
    }
	
	public function textChecklist()
    {
        return $this->hasMany('App\RefeTextCheckbox', 'refe_field_id', 'id')->where('refe_table_field_name', $this->getTable())->orderby("priority","asc");
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
