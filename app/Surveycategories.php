<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Surveycategories extends Model
{
	use SoftDeletes;
	protected $table = 'survey_categories';

    
    protected $primaryKey = 'id';
	
    protected $guarded = [];
	
	protected $appends = ['order_label'];
	
	public function getOrderLabelAttribute()
    {
		if($this->parent){
			return $this->parent->display_order.".".$this->display_order;
		}else{
			return $this->display_order;
		}
	}
	
	
	public function getIdAttribute($value) {
        return $this->unique_id;
    }
    
	public function survey()
    {
        return $this->belongsTo('App\Survey', 'survey_unique_id', 'unique_id');
    }
	public function parent()
    {
        return $this->belongsTo('App\Categories', 'parent_id', 'id');
    }
	public function refefile()
    {
        return $this->hasMany('App\Refefile', 'refe_field_id', 'id')->where('refe_table_field_name', 'cat_id');
    }
}
