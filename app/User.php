<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','status','utype','status','phone_number','language','device_token','device_type','otp_token','licence_no','qualification','experience','address'
    ];
	
	protected $appends = ['full_name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
	
	public function getFullNameAttribute()
    {
		return $this->first_name." ".$this->last_name;
	}
	
	public function activetasks()
    {
        return $this->hasMany('App\Survey', 'surveyor_id', 'id')->where('status', 'open');
    }
	public function closedtasks()
    {
        return $this->hasMany('App\Survey', 'surveyor_id', 'id')->where('status', 'closed');
    }
	public function tasks()
    {
        return $this->hasMany('App\Survey', 'surveyor_id', 'id');
    }
	
	public function signature()
    {
        return $this->hasOne('App\Refefile', 'refe_field_id', 'id')->where('refe_table_field_name', 'user_id');
    }
	
	
	
	public function qualification()
    {
        return $this->hasMany('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'user_id')->where('refe_type', 'qualification');
    }
	public function experience()
    {
        return $this->hasMany('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'user_id')->where('refe_type', 'experience');
    }
	
}
