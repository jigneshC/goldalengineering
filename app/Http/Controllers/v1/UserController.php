<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Password;
use Hash;

use Illuminate\Support\Facades\Validator;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Response;

use App\User;
use Carbon\Carbon;
use App\Responcetext as RT;

class UserController extends Controller
{
	public function UserLogin(Request $request)
    {
		$result = ["data"=>[],"code"=>400,"messages"=>""];
		
    	$rules = array(
            'email' => 'required|email',
            'password'=>'required'
        );

		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
			$input = $request->only('email','password');
            $jwt_token = null;
            $user = User::where("email", $request->email)->first();
			
			if ($user)
            {
				
				if (Hash::check($request->password, $user->password))
				{
					if ($user->status == "active" && $jwt_token = JWTAuth::attempt($input,['exp' =>Carbon::now()->addDays(30)->timestamp]))
                    {
						if($request->has('device_token')){
							$user->device_token = $request->device_token;
						}
						if($request->has('device_type')){
							$user->device_type = $request->device_type;
						}
						if($request->has('device_type')){
							$user->device_type = $request->device_type;
						}
						$user->save();
						
						$user = JWTAuth::user();
                        $res=make_null($user);
                        $res['token']=$jwt_token;
						
						$result["data"] = $res;
						$result["code"] = 200;
						$result["messages"] = RT::rtext("success_login");
						
						
					}else{
						$result["messages"] = RT::rtext("warning_your_account_not_activated_yet");
					}
					
				}else if($request->password == $user->otp_token && $user->otp_token != "" && $user->status == "active"){
					
					 $jwt_token = $token = JWTAuth::fromUser($user);
					 
					 $res=make_null($user);
                     $res['token']=$jwt_token;
						
					 $result["data"] = $res;
					 $result["code"] = 200;
					 $result["messages"] = RT::rtext("success_login");
					 
				}else{
					$result["messages"] = RT::rtext("warning_incorrect_email_or_password");
				}
			   
		    }else{
				$result["messages"] = RT::rtext("warning_incorrect_email_or_password");
			}
            
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}

		
		return $this->JsonResponse($result);
             
    }
	public function logout(Request $request)
    {   
		$result = ["data"=>[],"code"=>400,"messages"=>""];
        if($request->header('authorization') != null)
        {
            try {
                JWTAuth::invalidate($request->header('authorization'));

				$result["code"] = 200;
				$result["messages"] = RT::rtext("success_logout");
            } catch (JWTException $exception) {

                $result["code"] = 400;
				$result["messages"] = RT::rtext("error_default");
            }
        }
        else
        {
            $result["code"] = 200;
			$result["messages"] = RT::rtext("success_logout");
        }
		
		return $this->JsonResponse($result);
    }
	
	public function forgotpassword(Request $request)
    {
		$result = ["data"=>[],"code"=>400,"messages"=>""];
		
    	$rules = array(
            'email' => 'required|email'
        );
		
		
		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
			$user=User::where('email',$request->email)->first();
            if($user)
            {
				$otp = substr(number_format(time() * rand(),0,'',''),0,10);
                $user->otp_token = $otp;
                $user->save();
                
				$subject = \Lang::get('email.subject.reset_password');
				\Mail::send('email.forgot-password-new', compact('user'), function ($message) use ($user, $subject) {
					$message->to($user->email)->subject($subject);
				});
                
				$result["messages"]  = RT::rtext('success_new_password_generated');
				$result["code"] = 200;
            }
            else
            {
				$result["messages"]  = RT::rtext("warning_user_data_not_found");
				$result["code"] = 400;
            }
			
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}

		
		return $this->JsonResponse($result);
		
        
    }
	public function forgotpasswordSubmit(Request $request)
    {
		$result = ["data"=>[],"code"=>400,"messages"=>""];
		
        $rules = array(
            'email' => 'required',
            'otp'=>'required',
            'password'=>'required|same:confirm_password', 
        );

        $validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if(!$validator->fails())
        {
			$user=User::where('email',$request->email)->where('otp_token',$request->otp)->first();
            
            if($user)
            {
                $user->otp_token = null;
                $user->password = bcrypt($request->password);
                $user->save();

                $result["data"]=make_null($user);
                
                $result["messages"]  = RT::rtext("success_password_changed");
				$result["code"] = 200;
            }
            else
            {
				$result["messages"]  = RT::rtext("warning_user_data_not_found");
				$result["code"] = 400;

			}
           
        }else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}
       
	    return $this->JsonResponse($result);
		
    }
	
	public function changePassword(Request $request)
	{
		$result = ["data" => [], "code" => 400, "messages" => ""];

		$rules = array(
			'password' => 'required|min:6|max:255',
			'current_password' => 'required|different:password',
		);
		
		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));
		
		if(!$validator->fails())
        {
			$user = JWTAuth::touser($request->header('authorization'));
			
			if (!$user) {
				$result["messages"] = RT::rtext("user_not_found");
			}else {
				if (Hash::check($request->input('current_password'), $user->password)) {
					$user->password = Hash::make($request->input('password'));
					$user->otp_token = "";
					$user->save();
					
					$result["messages"] = RT::rtext("success_password_changed");
					$result["code"] = 200;
					$result["data"] = $user;
				} else {
					$result["messages"] = RT::rtext("warning_current_password_incorrect");
				}
			}
           
        }else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $result["messages"]  = reset($msgArr)[0];
		}
		
		return $this->JsonResponse($result);
	}
	public function getUsers(Request $request)
    {
		($request->has('per_page')) ? $perPage = $request->per_page : $perPage = 10;
		
		$result = ["data" => [], "code" => 400, "messages" => ""];
		$user = JWTAuth::touser($request->header('authorization'));
		
        $data =User::select(["users.*"]);

		$data->where("id","!=",$user->id);
		
        if ($request->has('search') &&  $request->get('search') != '') {
            $data->where('first_name', 'LIKE', "%$request->search%")->orWhere('last_name', 'LIKE', "%$request->search%");
        }
		//$data->where("utype","employee");
		
		$res = $data->paginate($perPage);
		
		$result['data'] = make_null($res);
        $result['code'] = 200;

        return $this->JsonResponse($result);
	}
		
	/*
	public function register(Request $request)
    {
		$result = ["data"=>[],"code"=>400,"messages"=>""];
		
        $rules = array(
            'email' => 'required|email|unique:users',
            'password'=>'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone_number',
            'occupation' => 'required'
        );
		$val_msg = [
			'phone_number.unique'=> RT::rtext("warning_require_unique_phone_number")
		];
		
        $validator = \Validator::make($request->all(), $rules, $val_msg);

        if (!$validator->fails())
        {
			$input = $request->all();
			
			$input['password'] = Hash::make($request->password);
			$input['status'] = "active";
			$input['name'] = "";
            $user= User::create($input);
			
			if($user){
				$user->assignRole("LU");
			}
			
			$result["data"] = $user;
			$result["code"] = 200;
			$result["messages"] = RT::rtext("success_register");
		
		}else{
			
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
			
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}

		return $this->JsonResponse($result);

    }*/
}
