<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Carbon\Carbon;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Response;

use App\User;
use App\Categories;
use App\Branches;
use App\Setting;
use App\Survey;
use App\Issue;
use App\Logs;

use App\Responcetext as RT;

class SettingsController extends Controller
{
	
	public function getSettings(Request $request)
    {   
		$result = ["data"=>[],"code"=>400,"messages"=>""];
       
		$last_update = 0;
		$last_update_req = 0;
		$need_update = 1;
		$data = [];
		
		$settings = Setting::get();
		foreach($settings as $setting){
			$data[$setting->key] =  $setting->value;
			if($setting->key == "last_update" && $setting->value != ""){
				$last_update = Carbon::createFromFormat("Y-m-d H:i:s",$setting->value);
			}
		}
		
		if($request->has('last_update') && $request->last_update != ""){
			$last_update_req = Carbon::createFromFormat("Y-m-d H:i:s",$request->last_update);
			if($last_update && $last_update > $last_update_req){
				$need_update = 1;
			}else{
				$need_update = 0;
			}
		}
	   
	   
		if($need_update){
			$categories = Categories::select(['id','name','slug','parent_id','display_order'])->with('child')->Parentonly()->orderby('display_order','asc')->get();
			$branches = Branches::select(['id','name'])->get();
			$constants = RT::select(['id','slug','desc'])->constantonly()->get();
			$checklist = RT::select(['id','slug','title','desc'])->checklistonly()->get();
			$locations = RT::select(['id','desc'])->locationonly()->get();
			
			$categories = make_null($categories);
			
			
			$categories = array_map(function($v) {
							if($v['order_label'] != ""){
								$v['lable_name'] = $v['order_label']." ".$v['name'];
							}else{
								$v['lable_name'] = $v['name'];
							}
							//echo "<pre>";print_r($v);
							if(isset($v['child']) && is_array($v['child']) && !empty($v['child'])){
								$v['child'] = array_map(function($k) {
									if($k['order_label'] != ""){ $k['name'] = $k['order_label']." ".$k['name']; }
									return $k;
								}, $v['child']);
							}
							return $v;
						}, $categories);
			
		
				$data['branches'] = getBranchwithImages($branches);
				$data['categories'] = $categories;
				$data['checklist'] = make_null($checklist);
				$data['constants'] = make_null($constants);
				$data['locations'] = make_null($locations);
				
			$result["code"] = 200;
		}else{
			$result["code"] = 402;
		}
		$result['data'] = $data;
		return $this->JsonResponse($result);
    }
	
	public function singleUpload(Request $request){
		
	}
	public function syncData(Request $request)
    {
	
		$log_survey_id = 0;
		$lang ="he";
		
		try{
			
		$user = JWTAuth::touser($request->header('authorization'));
		$result = ["data"=>[],"code"=>400,"messages"=>trans('common.responce_msg.something_went_wrong')];
        $requestData = $request->all();
		$date_format = "d/m/Y";
		$survey_imgs = [];
		$issue_imgs = [];
		$issue_create = [];
		$imgs_priority = [];
		$checklist = [];
		$survey = null;
		$steps = "";
		$now = Carbon::now();
		
		$sync_data = json_decode($request->sync_data,true);
		
		if(!is_array($sync_data)){
			$steps = $steps." need-second-time-json-encode ,";
			$sync_data = json_decode($sync_data,true);
		}
		
		
		
		
		
		
		
		if(isset($sync_data['survey'])){
			$steps = $steps." survey-exist-request ,";
			$input =  [];
			
			try{
				$survey_date = Carbon::createFromFormat($date_format,$sync_data['survey']['survey_date'])->format('Y-m-d');
				$created = Carbon::createFromTimestamp($sync_data['survey']['created'])->format('Y-m-d H:i:s');
				$input['created'] =  $created;
				$input['survey_date'] =  $survey_date;
			}catch(\Exception $e){
				$steps = $steps." try-catch-survey-date : ".$e->getMessage();
			}
			$input['owner_name'] =  $sync_data['survey']['owner_name'];
			$input['owner_email'] =  (isset($sync_data['survey']['owner_email']))? $sync_data['survey']['owner_email'] : "";
			$input['owner_phone'] =  (isset($sync_data['survey']['owner_phone']))? $sync_data['survey']['owner_phone'] : "";
			$input['property_desc'] =  $sync_data['survey']['property_desc'];
			$input['address'] =  $sync_data['survey']['address'];
			$input['legal_thing_a'] =  $sync_data['survey']['legal_thing_a'];
			$input['legal_thing_isa'] =  $sync_data['survey']['legal_thing_isa'];
			$input['is_the_sale_law_1973'] =  $sync_data['survey']['is_the_sale_law_1973'];
			$input['is_consultants'] =  $sync_data['survey']['is_consultants'];
			$input['not_tested'] =  $sync_data['survey']['not_tested'];
			$input['general_info'] =  $sync_data['survey']['general_info'];
			$input['other_info'] =  $sync_data['survey']['other_info'];
			$input['surveyor_note'] =  $sync_data['survey']['surveyor_note'];
			$input['reviews_and_summary'] =  $sync_data['survey']['reviews_and_summary'];
			$input['unique_id'] =  $sync_data['survey']['unique_id'];
			$input['surway_logo'] =  $sync_data['survey']['surway_logo'];
			
			$input['last_sync_at'] =  $now;
			$input['surveyor_id'] =  $user->id;
			
			
			if(isset($sync_data['survey']['id'])){
				$input['local_id'] =  $sync_data['survey']['id'];
			}
			if($request->has('checklist'))
			{
			
				$checklist = json_decode($request->checklist,true);
				
				if(!is_array($checklist)){
					$checklist = json_decode($checklist,true);
				}
				if(!is_array($checklist)){
					$checklist = json_decode($checklist,true);
				}
				
				if(is_array($checklist) && isset($checklist['checklist'])){
					$checklist = $checklist['checklist'];
					$new_checklist = array_filter($checklist, function ($var) use($input) {
						return (isset($var['unique_id']) && $var['unique_id'] == $input['unique_id'] && ( $var['check'] == 1 || $var['check'] == "1"));
					});
					$input['checklist'] =  json_encode($new_checklist);
				}
			}
			
			
			$exist = Survey::where('unique_id',$sync_data['survey']['unique_id'])->first();
			if(!$exist){
				$input['status'] =  "new";
				$steps = $steps." survey-exist-db ,";
			}
			
			if($request->has('is_complete') && ( $request->is_complete == "1" || $request->is_complete == 1 ) ){
				$s_status = "closed";
				$input['status'] =  "closed";
			}
			$input['enable_local_sync'] =  0;
			$survey =Survey::updateOrCreate(['unique_id'=>$sync_data['survey']['unique_id']],$input);
			$log_survey_id = $survey->id;
			if($survey && isset($sync_data['survey']['images']) && $sync_data['survey']['images'] !="")
			{
				
				$files =explode(",",$sync_data['survey']['images']);
				foreach($files as $k=> $file){
					$pathinfo = pathinfo($file);
					if(isset($pathinfo['extension'])){
					 $survey_imgs[$pathinfo['filename'].'.'.$pathinfo['extension']] = $survey->id;
					}
				}
			}
			
			
			if($survey){
				app(\App\Http\Controllers\v1\SettingsNewController::class)->makeSection($sync_data['survey'],$survey);
				$result['messages'] = trans('common.responce_msg.record_created_succes',[],$lang);
				$result['code'] = 200;
				$steps = $steps." survey-created-or-updated-done ,";
			}else{
				$steps = $steps." survey-created-or-updated-failed ,";
			}
		
			
			
		}else{
			$steps = $steps." survey-ob-not-exitst-in request ,";
		}
		
		if(isset($sync_data['issues'])){
			
			foreach($sync_data['issues'] as $i => $val){
			
				$input =  [];
				try{
				$created = Carbon::createFromTimestamp($sync_data['issues'][$i]['created'])->format('Y-m-d');
				$input['created'] =  $created;
				
				}catch(\Exception $e){
					$steps = $steps." try-catch-issue-date : ".$e->getMessage();
				}
				$input['survey_id'] =  $survey->id;
				$input['surveyor_id'] =  $user->id;
				if(isset($sync_data['issues'][$i]['child_category_id']) && $sync_data['issues'][$i]['child_category_id'] && $sync_data['issues'][$i]['child_category_id'] >0){
					
					$input['child_category_id'] =  $sync_data['issues'][$i]['child_category_id'];
					$input['child_unique_id'] = null;
					
		
				}else{
					$input['child_unique_id'] = $sync_data['issues'][$i]['sub_cat_unique_id'];
					$input['child_category_id'] =  0;
					
					$newscat = [
								"name"=>$sync_data['issues'][$i]['sub_cat_unique_name'],
								"unique_id"=>$sync_data['issues'][$i]['sub_cat_unique_id'],
								"slug"=>str_slug($sync_data['issues'][$i]['sub_cat_unique_name']),
								"survey_unique_id"=>$survey->unique_id,
								"display_order"=>0,
								"parent_id"=>$sync_data['issues'][$i]['category_id'],
								"created_by"=>$user->id,
								"updated_by"=>$user->id,
							];
					$new_ct =\App\Surveycategories::updateOrCreate(['unique_id'=>$sync_data['issues'][$i]['sub_cat_unique_id']],$newscat);
				}
				
				
				if(isset($sync_data['issues'][$i]['category_id'])){
					$input['category_id'] =  $sync_data['issues'][$i]['category_id'];
				}
				
				$input['location'] =  $sync_data['issues'][$i]['location'];
				$input['issue_detail'] =  $sync_data['issues'][$i]['issue_detail'];
				$input['recommendation'] =  (isset($sync_data['issues'][$i]['recommendation']))? $sync_data['issues'][$i]['recommendation'] : "";
				$input['quote'] =  (isset($sync_data['issues'][$i]['quote']))? $sync_data['issues'][$i]['quote'] : "";
				$input['note'] =  (isset($sync_data['issues'][$i]['note']))? $sync_data['issues'][$i]['note'] : "";
				$input['unit_cost'] =  $sync_data['issues'][$i]['unit_cost'];
				$input['total_cost'] =  ($sync_data['issues'][$i]['total_cost'] == "")? 0 : (float)  str_replace(',', '', $sync_data['issues'][$i]['total_cost']);
				$input['number_of_unit'] = ($sync_data['issues'][$i]['number_of_unit'] == "" )? 0 : $sync_data['issues'][$i]['number_of_unit'];
				
				
				if(isset($sync_data['issues'][$i]['id'])){
					$input['local_id'] =  $sync_data['issues'][$i]['id'];
				}
				
				$image_selection = [];
				if(isset($sync_data['issues'][$i]['cat_img']) && $sync_data['issues'][$i]['cat_img'] !="" ){
					
					$files =explode(",",$sync_data['issues'][$i]['cat_img']);
					foreach($files as $p=> $valp){
						$image_selection[] = ["img_id"=>$valp,"hint"=>""];
					}
				}
				$input['img_hint'] =  json_encode($image_selection);
				$input['unique_id'] = $sync_data['issues'][$i]['unique_id'];
			
				$exist = Issue::where('unique_id',$sync_data['issues'][$i]['unique_id'])->first();
				if(!$exist){
					$input['status'] =  "new";
				}
			
				$issue = Issue::where("unique_id",$sync_data['issues'][$i]['unique_id'])->where("survey_id", $survey->id)->first();
				if($issue){
					$issue->update($input);
					foreach($issue->refefile as $rf){
						removeRefeImage($rf);
					}
				}else{
					$issue =Issue::create($input);
				}
				$issue_create[] = $issue->id;
			
				
				if($issue && isset($sync_data['issues'][$i]['images']) && $sync_data['issues'][$i]['images'] != "")
				{
					
						$files =explode(",",$sync_data['issues'][$i]['images']);
						foreach($files as $k=> $file){
							$pathinfo = pathinfo($file);
							if(isset($pathinfo['extension'])){
							$issue_imgs[$pathinfo['filename'].'.'.$pathinfo['extension']] = $issue->id;
							}
						}
				}
				if($issue && isset($sync_data['issues'][$i]['image_priority']) && $sync_data['issues'][$i]['image_priority'] != "")
				{
					foreach($sync_data['issues'][$i]['image_priority'] as $n=> $faileob){
						$files =explode(",",$faileob['image_path']);
						foreach($files as $k=> $file){
							$pathinfo = pathinfo($file);
							if(isset($pathinfo['extension'])){
							$imgs_priority[$pathinfo['filename'].'.'.$pathinfo['extension']] = $faileob['priority'];
							}
						}
					}
				}
			
			}
			
			if($survey){
				$this->deleteOtherIssue($issue_create,$survey);
				$survey->issue_count = $survey->issue->count();
				$survey->save();
				
				$result['messages'] = trans('common.responce_msg.record_created_succes',[],$lang);
				$result['code'] = 200;
				
				
				$r_data = ["updated_at"=>$survey->updated_at->format('Y-m-d H:i:s'),"id"=>$survey->id];
				$result['data'] = $r_data;
				$result['data']['surveyor_id'] =  $user->id;
				$result['data']['local_id'] =  $sync_data['survey']['id'];
				
			}else{
				$result['messages'] = trans('common.responce_msg.something_went_wrong',[],$lang);
				$result['code'] = 400;
				$steps = $steps." survey-ob-not-found-in-issue loop ,";
			}
		}else{
			$steps = $steps." no-issue-found ,";
		}
		
		if($request->hasFile('images'))
		{
			$survey_imgs_ob = [];
			$issue_imgs_ob = [];
			
			$files = $request->file('images');
			if($files && count($files) >0){
				foreach ($files as $i => $file) {
					
					$real_name = $file->getClientOriginalName();
					if(isset($survey_imgs[$real_name])){
						$dfiles = [$file];
						uploadModalReferenceFile($dfiles,'uploads/survey/'.$survey_imgs[$real_name],'survey_id',$survey_imgs[$real_name],'survey_image',$imgs_priority);
					}else if(isset($issue_imgs[$real_name])){
						$dfiles = [$file];
						uploadModalReferenceFile($dfiles,'uploads/issues/'.$issue_imgs[$real_name],'issue_id',$issue_imgs[$real_name],'issue_image',$imgs_priority);
					}
				}
			}
		}
			
		
		}catch(\Exception $e){
			$result['messages'] = $e->getMessage();
			$result['code'] = 400;
			$steps = $steps." try-catch : ".$e->getMessage();
		}
		
		
		if($request->has('sync_data')){
			$file_obs = "";
			try{
				$files = [];
			if($request->hasFile('images'))
			{
				$files = $request->file('images');
				if($files && count($files) >0){
					foreach ($files as $i => $file) {
						$file_obs = $file_obs." +++ ".$file->getClientOriginalName();
					}
				}
			}	
			$log = new Logs();
			$log->log_name = "sync_upload";
			$log->description = $request->is_complete . " file_obs => " .$file_obs;
			$log->properties = json_encode($request->all());
			$log->subject_type = $steps;
			$log->survey_id = $log_survey_id;
			$log->save();
			
			}catch(\Exception $e){
				$steps = $steps." try-catch : ".$e->getMessage();
			}
			/*\Mail::send('email.test', ['data'=>$sync_data,'requestData'=>$requestData,'steps'=>$steps], function ($message) use($now) {
			  $message->to("jitendra.citrusbug@gmail.com")->cc(['jignesh.citrusbug@gmail.com'])
				->subject("Sync api test ".$now);
			});*/
		} 
		
		
		return $this->JsonResponse($result);

        
    }
	
	public function deleteOtherIssue($issue_create,$survey){
		$issues = Issue::whereNotIn("id",$issue_create)->where("local_id","!=",0)->where("survey_id", $survey->id)->get();
		foreach($issues as $issue){
			/*foreach($issue->refefile as $rf){
				removeRefeImage($rf);
			}*/
			$issue->delete();
		}
		
	}
	public function getUpdatetime(Request $request)
    {   
		$result = ["data"=>[],"code"=>400,"messages"=>""];
       
	   
		$settings = Setting::get();
		$data =[];
		
		foreach($settings as $setting){
			$data[$setting->key] = $setting->value;
		}

		$result['data'] = $data;

		$result["code"] = 200;
		return $this->JsonResponse($result);
    }
	
	
}
