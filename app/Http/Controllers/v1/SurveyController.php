<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Password;
use Hash;

use Illuminate\Support\Facades\Validator;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Response;
use Carbon\Carbon;

use App\User;
use App\Categories;
use App\Branches;
use App\Setting;
use App\Survey;
use App\Issue;
use App\Logs;

use App\Responcetext as RT;

class SurveyController extends Controller
{
	public function surveyDublicate(Request $request)
    {
		$_newid = 0;
		$result = ["data" => [], "code" => 400, "messages" => ""];
		
		$rules = array(
            'unique_id' => 'required',
        );

		$ress = [];
		$validator = \Validator::make($request->all(), $rules, [],trans('survey.label'));
		
		if ($validator->fails())
        {
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
			
		}else{
			 $survey = Survey::where("unique_id",$request->unique_id)->first();
			 
			if(!$survey){
				 $result['code'] = 400;
				 $result['messages'] = trans('common.responce_msg.data_not_found');
			}else{
				$newS = $survey->replicate();
				$newS->unique_id = $survey->unique_id."_".rand(0,999);
				$newS->parent_id = $survey->id;
				//$newS->created = Carbon::now();
				//$newS->survey_date = Carbon::now();
				$newS->created_at = Carbon::now();
				$newS->save();
				
				$_newid = $newS->id;
				
				foreach($survey->textChecklist as $tc){
					$newTc = $tc->replicate();
					$newTc->refe_field_id = $newS->id;
					$newTc->save();
				}
				foreach($survey->issue as $issue){
					$cunitqe_id = null;
					if($issue->child_category_id && $issue->child_category_id > 0){
						
					}else if($issue->child_unique_id && $issue->child_unique_id !="" && $issue->surveycategory){
						
						$newcat = $issue->surveycategory->replicate();
						$cunitqe_id = $newcat->unique_id."_".rand(0,999);
						$newcat->unique_id =  $cunitqe_id;
						$newcat->survey_unique_id =  $newS->unique_id;
						$newcat->save();
					}
					
					$newI = $issue->replicate();
					$newI->survey_id = $newS->id;
					$newI->duplicate_from = $issue->id;
					$newI->child_unique_id = $cunitqe_id;
					$newI->unique_id = $issue->unique_id."_".rand(0,999);
					$newI->created = Carbon::now();
					$newI->save();
					
					foreach($issue->refefile as $ref){
						$newR = $ref->replicate();
						$newR->refe_field_id = $newI->id;
						$newR->refe_code = $newS->id;
						$newR->save();
						
						//$ress[]= "<br/>-s=".$newS->id." i=".$newI->id." r=".$newR->id." : os=".$survey->id." oi=".$issue->id." or=".$ref->id;
						
						resetFolder($newR,$ref->refe_field_id);
					}
				}
				
				
				$res = Survey::with(['issue'])->where("id",$newS->id)->first();
				if($res){
					 $res->created = Carbon::createFromFormat("Y-m-d H:i:s",$res->created)->timestamp;
					
					 foreach($res->issue as $issue){
						$reffile = $issue->refefile;
						$issue->categoryName = "";
						$issue->subCategoryName = "";
						if($issue->category){
							$issue->categoryName = $issue->category->order_label." ".$issue->category->name;
						}
						if($issue->subcategory){
							$issue->subCategoryName = $issue->subcategory->order_label." ".$issue->subcategory->name;
						}
						unset($issue->subcategory);
						unset($issue->category);
						if($issue->img_hint && $issue->img_hint != ""){
							$img_hint = json_decode($issue->img_hint,true);
							$ids = array_column($img_hint, 'img_id');
							$issue->img_hint = implode(",",$ids);
						}
						
						$issue->created = Carbon::createFromFormat("Y-m-d H:i:s",$issue->created)->timestamp;
						
					 }
				}
				$result['code'] = 200;
				//$result['res'] = $ress;
				$result['data'] = make_null($res);
			}
		}
		// Rest order of images
		Survey::fileOrder($_newid);
		
		$request->request->add(['survey_id' => $_newid]);
		
		return $this->getSurveyDetail($request);
	}
	public function getSurveyA(Request $request)
    {
		$result = ["data" => [], "code" => 400, "messages" => ""];
		$user = JWTAuth::touser($request->header('authorization'));
			
        $data =Survey::select(["id","assign_to_id","surveyor_id","unique_id","owner_name","owner_email","owner_phone","address","property_desc","created_at","survey_date"]);

        if ($request->has('type') &&  $request->get('type') == 'assign') {
            $data->where("assign_to_id",$user->id)->where("enable_local_sync","!=",1);
        }else if ($request->has('type') &&  $request->get('type') == 'local_sync') {
            $data->where("assign_to_id",$user->id)->where("enable_local_sync",1);
        }else{
			$data->where("surveyor_id",$user->id);
		}
		
		$res = $data->get();
		
		foreach($res as $survey){
			if($survey->surveyor){
				$survey->assign_by = $survey->surveyor->full_name;
				$survey->message = trans('survey.msg.assign_by',['name'=>$survey->surveyor->full_name]);
			}else{
				$survey->assign_by = "";
				$survey->message = "";
			}
			unset($survey->surveyor);
		}
		
		$result['data'] = make_null($res);
        $result['code'] = 200;

        return $this->JsonResponse($result);
	}
	public function getSurveyDetail(Request $request)
    {
		
		$result = ["data" => [], "code" => 400, "messages" => ""];
		//$user = JWTAuth::touser($request->header('authorization'));
			
        $data =Survey::with(["issue"]);
		

        if ($request->has('survey_id') &&  $request->get('survey_id') != '') {
            $data->where("id",$request->survey_id);
        }else{
			$data->where("id",0);
		}
		
		$res = $data->first();
		
		if(!$res){
			 $result['code'] = 400;
			 $result['messages'] = trans('common.responce_msg.data_not_found');
		}else{
			//*****************Add- check list*********************//
			$seckey=["general_info","property_desc","legal_thing_a","legal_thing_isa","not_tested","reviews_and_summary","surveyor_note","other_info"];	
			
			foreach($seckey as  $key){
				$_checklist = [];
				$_checklist_checked = [];
				
				if($res->textChecklist->count()){
					$fildsection = $res->textChecklist->where("refe_code",$key);
					if($fildsection->count()){
						$_checklist = $fildsection->pluck("desc","priority")->toArray();
						$_checklist_checked = $fildsection->pluck("refe_type","priority")->toArray();
					}
				}
				
				if($key == "property_desc"){ 
					$const = RT::where("slug","property_description")->first();
				}else{
					$const = RT::where("slug",$key)->first();
				}
				
				if($const && $const->textChecklist->count()){
					$_redata = [];
					foreach($const->textChecklist as $tc){
						$_redata[] = [
							"id"=> $tc->id,
							"refe_field_id"=>$tc->refe_field_id,
							"desc"=> (isset($_checklist[$tc->id]))? $_checklist[$tc->id] : $tc->desc,
							"refe_type"=> (isset($_checklist_checked[$tc->id]))? $_checklist_checked[$tc->id] : 0,
							"is_editable"=> 1,
						];
					}
					$res->$key = json_encode($_redata);
				}
			}
			//*****************Add- check list End*********************//
			
			 $res->created = Carbon::createFromFormat("Y-m-d H:i:s",$res->created)->timestamp;
			// $res->survey_date = Carbon::createFromFormat("Y-m-d H:i:s",$res->survey_date)->format('d/m/Y');
			 foreach($res->issue as $issue){
				$reffile = $issue->refefile;
				if($issue->category){
					$issue->categoryName = $issue->category->order_label." ".$issue->category->name;
				}
				
				if($issue->subcategory){
					$issue->subCategoryName = $issue->subcategory->order_label." ".$issue->subcategory->name;
					$issue->sub_cat_unique_name = "";
					$issue->sub_cat_unique_id = "";
				}else if($issue->surveycategory){
					$issue->sub_cat_unique_name = $issue->surveycategory->name;
					$issue->sub_cat_unique_id = $issue->surveycategory->unique_id;
				}else{
					$issue->subCategoryName = "";
					$issue->sub_cat_unique_name = "";
					$issue->sub_cat_unique_id = "";
				}
				
				unset($issue->surveycategory);
				unset($issue->subcategory);
				unset($issue->category);
				if($issue->img_hint && $issue->img_hint != ""){
					$img_hint = json_decode($issue->img_hint,true);
					$ids = array_column($img_hint, 'img_id');
					$issue->img_hint = implode(",",$ids);
				}
				
				$issue->created = Carbon::createFromFormat("Y-m-d H:i:s",$issue->created)->timestamp;
				
			 }	
			 $result['code'] = 200;
			 $result['data'] = make_null($res);
		}
		
       

        return $this->JsonResponse($result);
	}
	public function assignSurvey(Request $request)
    {
		$result = ["data" => [], "code" => 400, "messages" => ""];
		$user = JWTAuth::touser($request->header('authorization'));
		$assignee = null;
			
        $data =Survey::with("issue");

        if ($request->has('survey_id') &&  $request->get('survey_id') != '') {
            $data->where("id",$request->survey_id);
        }else if ($request->has('unique_id') &&  $request->get('unique_id') != '') {
            $data->where("unique_id",$request->unique_id);
        }else{
			$data->where("id",0);
		}
		$item = $data->first();
		
		if ($request->has('assign_to_id') &&  $request->get('assign_to_id') != '') {
            $assignee =User::where("id",$request->assign_to_id)->first();
        }
		
		
		
		if($item && $assignee){
			$item->assign_to_id = $request->assign_to_id;
			$item->save();
			
			$subject = trans('email.subject.new_survey_assign_request');
			$to = "jignesh.citrusbug@gmail.com";//$assignee->email;
			
		/*	\Mail::send("email.assigne-survey", compact('item','assignee','user'), function ($message) use ($subject,$to) {
					$message->to($to)->subject($subject);
			}); */
			
			$result['code'] = 200;
			$result['messages'] = trans('common.responce_msg.survey_assign_success');
		}else{
			$result['code'] = 400;
			$result['messages'] = trans('common.responce_msg.data_not_found'); 
		}
		
       

        return $this->JsonResponse($result);
	}
	public function checkIsLocalSync(Request $request)
    {
		$result = ["data" => ['is_enable'=>0], "code" => 200, "messages" => ""];
		$user = JWTAuth::touser($request->header('authorization'));
		
		if ($request->has('unique_id') &&  $request->get('unique_id') != '') {
            $item = \App\LocalSybc::where("unique_id",$request->unique_id)->where("user_id",$user->id)->first();
			if($item && $item->enable_local_sync){
				$result['data']['is_enable'] = 1;
			}
        }
		return $this->JsonResponse($result);
	}
	public function actionOnAssign(Request $request)
    {
		$result = ["data" => [], "code" => 400, "messages" => ""];
		$user = JWTAuth::touser($request->header('authorization'));
		
        $item =null;

        if ($request->has('survey_id') &&  $request->get('survey_id') != '') {
            $item = Survey::where("id",$request->survey_id)->first();
        }
		
		if($item && $item->assign_to_id == $user->id){
			$creator = User::where("id",$item->surveyor_id)->first();
			$subject = trans('email.subject.accept_survey_assign_request');
			$action = "accepted";
			if ($request->has('action_type') &&  $request->get('action_type') == 'accept') {
				$assign_to_id = $item->assign_to_id;
				$item->surveyor_id = $assign_to_id;
				$item->assign_to_id = 0;
				$item->enable_local_sync = 0;
				$action = "accepted";
				$subject = trans('email.subject.accept_survey_assign_request');
			}else if ($request->has('action_type') &&  $request->get('action_type') == 'decline') {
				$item->assign_to_id = 0;
				$item->enable_local_sync = 0;
				$subject = trans('email.subject.decline_survey_assign_request');
				$action = "decline";
			}
		
		
			
			$to = $creator->email;
			/*
			\Mail::send("email.assigne-survey-action", compact('item','creator','user','action'), function ($message) use ($subject,$to) {
					$message->to($to)->subject($subject);
			});
		*/
			
			$item->save();
			
			$result['code'] = 200;
			$result['messages'] = trans('common.js_msg.action_success');
		}else{
			
			$result['code'] = 400;
			$result['messages'] = trans('common.responce_msg.data_not_found'); 
		}
		
       

        return $this->JsonResponse($result);
	}
	
}
