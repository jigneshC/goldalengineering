<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Branches;
use App\Setting;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class BranchesController extends Controller
{
    
    public function index(Request $request)
    {
        return view('admin.branches.index');
    }
    public function datatable(Request $request) {
        $record = Branches::with('refeimg');
        return Datatables::of($record)->make(true);
    }

    
    public function create()
    {
		return view('admin.branches.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'name' => 'required|min:2|max:150',
			'images.*' => 'mimes:jpeg,png,jpg,gif,svg|max:5000',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:5000'
        ],[],trans('branches.label'));


        $requestData = $request->except(['image','images','file','files']);
		$requestData['created_by'] = \Auth::user()->id;
		$requestData['updated_by'] = \Auth::user()->id;
		$requestData['is_fullsizelogo'] = 0;
		if($request->has('is_fullsizelogo'))
		{
			$requestData['is_fullsizelogo'] = 1;
		}
        $module = Branches::create($requestData);
		
        if($module){
			if($request->hasFile('images'))
			{
				$files = $request->file('images');
				uploadModalReferenceFile($files,'uploads/branches/'.$module->id,'branch_id',$module->id,'branch_image',[]);
			}
			if($request->hasFile('image'))
			{
				$files = [$request->file('image')];
				uploadModalReferenceFile($files,'uploads/branches/'.$module->id,'branch_id',$module->id,'branch_image',[]);
			}
			
			Setting::updateOrCreate(['key'=>'last_update'],['key'=>'last_update','value'=>\Carbon\Carbon::now()]);
            $result['message'] = trans('common.responce_msg.record_created_succes');
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/branches');
        }

    }
	public function show($id,Request $request)
    {
		$item = Branches::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/branches');
		}
		return view('admin.branches.show',compact('item'));
	}
	
    public function edit($id,Request $request)
    {
        $result = array();
        $item = Branches::findOrFail($id);

        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            return view('admin.branches.edit', compact('item'));
        }
		

    }

    public function update($id, Request $request)
    {
        $result = array();

       $this->validate($request, [
			'name' => 'required|min:2|max:150',
			'images.*' => 'mimes:jpeg,png,jpg,gif,svg|max:5000',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:5000'
        ],[],trans('branches.label'));

        $item = Branches::where("id",$id)->first();
        $requestData = $request->except(['image','images','file','files']);
		
		$requestData['is_fullsizelogo'] = 0;
		if($request->has('is_fullsizelogo'))
		{
			$requestData['is_fullsizelogo'] = 1;
		}
        
        if($item){
            $item->update($requestData);
			
			if($request->hasFile('images'))
			{
				$files = $request->file('images');
				uploadModalReferenceFile($files,'uploads/branches/'.$item->id,'branch_id',$item->id,'branch_image',[]);
			}
			if($request->hasFile('image'))
			{
				if($item->refeimg){
					removeRefeImage($item->refeimg);
					$item->refeimg->delete();
				}
				$files = [$request->file('image')];
				uploadModalReferenceFile($files,'uploads/branches/'.$item->id,'branch_id',$item->id,'branch_image',[]);
			}
			
			Setting::updateOrCreate(['key'=>'last_update'],['key'=>'last_update','value'=>\Carbon\Carbon::now()]);
            $result['message'] = trans('common.responce_msg.record_updated_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/branches');
        }
        
    }
    

    public function destroy($id,Request $request)
    {
        $item = Branches::where("id",$id)->first();

        $result = array();

        if($item){
            $item->delete();
            $result['message'] = trans('common.responce_msg.record_deleted_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			return redirect('admin/branches');
        }
    }


}
