<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Survey;
use App\Issue;
use App\Categories;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon\Carbon;

class SurveyCatController extends Controller
{
    
    public function index(Request $request)
    {
	//	return redirect('admin/survey');
	
    }
	
	public function store(Request $request)
    {
        $result = array();

        
		
		$rules = [
            'survey_unique_id' => 'required',
            'cat_parent_id' => 'required',
            'category_name' => 'required',
            'images.*' => 'mimes:jpeg,png,jpg,gif,svg|max:5000',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:5000',
        ];

		$validator = \Validator::make($request->all(), $rules, [],trans('categories.label'));

        if ($validator->fails())
        {
            $validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json(['message' =>$messages,'success' => false,'status' => 400],400);
        }
		
        $input = [];
		$input['name'] = $request->category_name;
		$input['unique_id'] = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 5).uniqid();
		$input['slug'] = str_slug($input['name']);
		$input['survey_unique_id'] = $request->survey_unique_id;
		$input['display_order'] = $request->display_order;
		$input['parent_id'] = $request->cat_parent_id;
		$input['created_by'] =  \Auth::user()->id;
		$input['updated_by'] =  \Auth::user()->id;
		
		
		$item =\App\Surveycategories::create($input);
        
		$result['message'] = \Lang::get('common.responce_msg.item_created_success',['item'=>"Category"]);
		$result['data'] = $item;
        $result['code'] = 200;
			
        return response()->json($result, $result['code']);

    }
	
   
    


}
