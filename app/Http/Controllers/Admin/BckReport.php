<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Survey;
use App\Branches;
use App\Issue;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;
use App\User;
use App\Refefile;

class BckReport extends Controller
{
    
    public function index(Request $request)
    {
		$surveyor = null;
		if ($request->has('surveyor_id') && $request->get('surveyor_id') != '' && $request->get('surveyor_id') != 0) {
            $surveyor = User::where("id",$request->surveyor_id)->first();
        }
		
        return view('admin.survey.index',compact('surveyor'));
    }
    public function datatable(Request $request) {
        $record = Survey::where("id",">",0);
		
		if ($request->has('status') && $request->get('status') != 'all' && $request->get('status') != '' ) {
            $record->where('status',$request->get('status'));
        }
		if ($request->has('surveyor_id') && $request->get('surveyor_id') != '' && $request->get('surveyor_id') != 0) {
            $record->where('surveyor_id',$request->get('surveyor_id'));
        }
        return Datatables::of($record)->make(true);
    }

    public function show($id,Request $request)
    {
		$item = Survey::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
		}
		return view('admin.survey.show',compact('item'));
	}
	public function detail($id,Request $request)
    {
		$item = Survey::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
		}
		return view('admin.survey.detail',compact('item'));
	}
	
    public function edit($id,Request $request)
    {
        $result = array();
        $item = Survey::findOrFail($id);
		
		$branches = Branches::pluck('name', 'id');

        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
			
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
        }
        
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            return view('admin.survey.edit', compact('item','branches'));
        }
		

    }

    public function update($id, Request $request)
    {
        $result = array();

        
		
		$varr = [
            'owner_name' => 'required|min:2|max:150',
            'owner_email' => 'required|min:2|max:150',
            'address' => 'required',
            'property_desc' => 'required',
            'legal_thing_a' => 'required',
            'legal_thing_isa' => 'required',
            'surveyor_note' => 'required',
            'reviews_and_summary' => 'required',
            'images.*' => 'mimes:jpeg,png,jpg,gif,svg|max:5000',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:5000',
        ];

		$this->validate($request,$varr,[],trans('survey.label'));
		
        $item = Survey::where("id",$id)->first();
        $requestData = $request->except(['image','images','file','files']);
        
        if($item){
            $item->update($requestData);
			
			$item->issue_count = $item->issue->count();
			$item->save();
			
			if($request->hasFile('images'))
			{
				$files = $request->file('images');
				uploadModalReferenceFile($files,'uploads/survey/'.$item->id,'survey_id',$item->id,'logo_image',[]);
			}
			if($request->hasFile('image'))
			{
				$files = [$request->file('image')];
				uploadModalReferenceFile($files,'uploads/survey/'.$item->id,'survey_id',$item->id,'logo_image',[]);
			}
            $result['message'] = trans('common.responce_msg.record_updated_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect()->back();
        }
        
    }
    

    public function destroy($id,Request $request)
    {
        $item = Survey::where("id",$id)->first();

        $result = array();

        if($item){
			$issues = Issue::where("survey_id",$id)->get();
			foreach($issues as $issue){
				foreach($issue->refefile as $rf){
					removeRefeImage($rf);
				}
				$issue->forceDelete();
			}
			foreach($item->refefile as $rf){
				removeRefeImage($rf);
			}
            $item->forceDelete();
            $result['message'] = trans('common.responce_msg.record_deleted_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			return redirect('admin/survey');
        }
    }
	public function export($id,Request $request)
    {
	//	\App::setLocale("he");
		
		$logo = public_path('assets/images/logo.png');
		
		$item = Survey::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
		}
		$align = "left";
		$opalign = "right";
		
		$lb = 1;
		$fontStyleh = new \PhpOffice\PhpWord\Style\Font();
		if(\App::getLocale() == 'he'){
			$fontStyleh->setRTL(true);
			$fontStyleh->setLang('he-IL');
			$align = "right";
			$opalign = "left";
			$lb = 2;
		}else{
			$fontStyleh->setRTL(false);
			$fontStyleh->setLang('en');
			$align = "left";
			$opalign = "right";
			$lb = 1;
		}
        
		$fontStyleh->setBold(true);
		$fontStyleh->setName('Tahoma');
		$fontStyleh->setSize(10);
		
		$fontStyle = new \PhpOffice\PhpWord\Style\Font();
		if(\App::getLocale() == 'he'){
			$fontStyle->setRTL(true);
			$fontStyle->setLang('he-IL');
		}else{
			$fontStyle->setRTL(false);
			$fontStyle->setLang('en');
		}
        
		$fontStyle->setBold(false);
		$fontStyle->setName('Tahoma');
		$fontStyle->setSize(10);
		
		$bluetitle = new \PhpOffice\PhpWord\Style\Font();
		$bluetitle->setRTL(true);
		$bluetitle->setBold(false);
		$bluetitle->setName('Tahoma');
		$bluetitle->setSize(18);
		$bluetitle->setColor("00bfff");
	//	$bluetitle->setUnderline("single");
		
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		
				// Define styles
		$multipleTabsStyleName = 'multipleTab';
		$phpWord->addParagraphStyle(
			$multipleTabsStyleName,
			array(
				'tabs' => array(
					new \PhpOffice\PhpWord\Style\Tab('left', 1550),
					new \PhpOffice\PhpWord\Style\Tab('center', 3200),
					new \PhpOffice\PhpWord\Style\Tab('right', 5300),
				),
			)
		);
		$rightTabStyleName = 'rightTab';
		$phpWord->addParagraphStyle($rightTabStyleName, array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('right', 9090))));
		$leftTabStyleName = 'centerTab';
		$phpWord->addParagraphStyle($leftTabStyleName, array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('center', 4680))));

		$section = $phpWord->addSection();
		
		$subsequent = $section->addHeader();
		//$subsequent->addText("Subsequent pages in Section 1 will Have this!");
		$subsequent->addImage($logo, array('width' => 30, 'height' => 30,'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
		
		
		$table = $subsequent->addTable(array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
		$table->addRow();
		$table->addCell(7000)->addText("Inspection and Engineering Services             ביקורת מבנים ושירותי הנדסה");
		$table->addRow();
		$table->addCell(700)->addText('                                    052-6517770      /      058-7517771');
		$table->addRow();
		$table->addCell(700)->addText('                              sliorgl@gmail.com      /      salexgl@gmail.com');

		
		
		
		$section->addTextBreak();
		
		$section->addText(trans('survey.label.created')." : ".$item->created."  \t ".trans('survey.label.owner_name')." : ".$item->owner_name, $fontStyleh, $rightTabStyleName);
		
		$section->addTextBreak(1);
		
		$section->addText("  \t ".trans('survey.label.owner_email'),$fontStyleh,$rightTabStyleName);
		$section->addText("  \t ".$item->owner_email,$fontStyle,$rightTabStyleName);
		
		
		$section->addTextBreak(1);
		
		$section->addText("\t ".trans('survey.label.professional_opinion')." : ".$item->address, $bluetitle, $leftTabStyleName);
		
	//	$section = $section->addTextRun(array('alignment' => $align));
		
	
		$section->addTextBreak($lb);
		
		
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.surway_logo'),$fontStyleh);
		$section->addTextBreak();
		if($item->branch){
			$section->addText($item->branch->name,$fontStyle);
		}else{
			$section->addText("-",$fontStyle);
		}
		
		$section->addTextBreak($lb);

		$section->addText(trans('survey.label.surveyor_name'),$fontStyleh);
		$section->addTextBreak();
		if($item->surveyor){
			$section->addText($item->surveyor->email,$fontStyle);
		}else{
			$section->addText("-",$fontStyle);
		}
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.issue_count'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->issue_count,$fontStyle);
		$section->addTextBreak($lb);
		
		
	
		
		$section->addText(trans('survey.label.is_consultants'),$fontStyleh);
		$section->addTextBreak();
		if($item->is_consultants){
			$section->addText(trans('common.label.yes'),$fontStyle);
		}else{
			$section->addText(trans('common.label.no'),$fontStyle);
		}
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.is_the_sale_law'),$fontStyleh);
		$section->addTextBreak();
		if($item->is_the_sale_law_1973){
			$section->addText(trans('common.label.yes'),$fontStyle);
		}else{
			$section->addText(trans('common.label.no'),$fontStyle);
		}
		$section->addTextBreak($lb);
		
		if($item->refefile->count()){
			$section->addText(trans('survey.label.images'),$fontStyleh);
			$section->addTextBreak();
			foreach($item->refefile as $rf){
				if($rf->file_path && $rf->file_path != ""){
					$section->addImage($rf->file_path, array('width' => 210, 'height' => 210, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
					break;
				}
			}
			
		}
		$section->addTextBreak($lb);
		
		$section = $phpWord->addSection();
		$section = $section->addTextRun(array('alignment' => $align));
		
		$section->addText(trans('survey.label.address'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->address,$fontStyle);
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.property_desc'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->property_desc,$fontStyle);
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.legal_thing_a'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->legal_thing_a,$fontStyle);
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.legal_thing_isa'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->legal_thing_isa,$fontStyle);
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.surveyor_note'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->surveyor_note,$fontStyle);
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.reviews_and_summary'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->reviews_and_summary,$fontStyle);
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.not_tested'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->not_tested,$fontStyle);
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.general_info'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->general_info,$fontStyle);
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.other_info'),$fontStyleh);
		$section->addTextBreak();
		$section->addText($item->other_info,$fontStyle);
		
		$section = $phpWord->addSection();
		$section = $section->addTextRun(array('alignment' => $align));
		
		$section->addText(trans('issue.label.issue_detail'),$fontStyleh);
		
		
		
		
		foreach($item->issue as $k => $issue){
			$section->addTextBreak($lb);
			$section->addText(($k+1).".",$fontStyleh);
			$section->addTextBreak($lb);
			
			if($issue->category){
				$section->addText(trans('issue.label.category'),$fontStyleh);
				$section->addTextBreak();
				$section->addText($issue->category->order_label." ".$issue->category->name,$fontStyle);
				$section->addTextBreak($lb);
			}
			if($issue->subcategory){
				$section->addText(trans('issue.label.child_category'),$fontStyleh);
				$section->addTextBreak();
				$section->addText($issue->subcategory->order_label." ".$issue->subcategory->name,$fontStyle);
				$section->addTextBreak($lb);
				
				if($issue->img_hint && $issue->img_hint != ""){
					
					$section->addText(trans('categories.label.category_images'),$fontStyleh);
					$section->addTextBreak();
				
					$img_hint = json_decode($issue->img_hint,true);
					foreach($img_hint as $z => $hint){
						$rf = Refefile::whereId($hint['img_id'])->first();
						if($rf->file_path && $rf->file_path != ""){
							$section->addImage($rf->file_path, array('width' => 210, 'height' => 210, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
							$section->addTextBreak();
						}
					}
				}
				
			}
			$section->addText(trans('issue.label.unit_cost'),$fontStyleh);
			$section->addTextBreak();
			$section->addText($issue->unit_cost,$fontStyle);
			
			$section->addTextBreak($lb);
			
			$section->addText(trans('issue.label.number_of_unit'),$fontStyleh);
			$section->addTextBreak();
			$section->addText($issue->number_of_unit,$fontStyle);
			
			$section->addTextBreak($lb);
			
			$section->addText(trans('issue.label.total_cost'),$fontStyleh);
			$section->addTextBreak();
			$section->addText($issue->total_cost,$fontStyle);
			
			$section->addTextBreak($lb);
			
			$section->addText(trans('issue.label.location'),$fontStyleh);
			$section->addTextBreak();
			$section->addText($issue->location,$fontStyle);
			
			$section->addTextBreak($lb);
			
			$section->addText(trans('issue.label.issue_detail'),$fontStyleh);
			$section->addTextBreak();
			$section->addText($issue->issue_detail,$fontStyle);
			
			$section->addTextBreak($lb);
			
			if($issue->refefile->count()){
				$section->addText(trans('issue.label.issue_images'),$fontStyleh);
				$section->addTextBreak();
				foreach($issue->refefile as $rf){
					if($rf->file_path && $rf->file_path != ""){
						$section->addImage($rf->file_path, array('width' => 210, 'height' => 210, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
						$section->addTextBreak();
					}
				}
				
			}
			
			$section->addTextBreak($lb);
			
			$section->addText(trans('issue.label.recommendation'),$fontStyleh);
			$section->addTextBreak();
			$section->addText($issue->recommendation,$fontStyle);
			
			$section->addTextBreak($lb);
			
			$section->addText(trans('issue.label.quote'),$fontStyleh);
			$section->addTextBreak();
			$section->addText($issue->quote,$fontStyle);
			
			$section->addTextBreak($lb);
			
			
			$section->addText(trans('issue.label.note'),$fontStyleh);
			$section->addTextBreak();
			$section->addText($issue->note,$fontStyle);
			
			$section->addTextBreak($lb);
			
			$section->addText(trans('survey.label.created'),$fontStyleh);
			$section->addTextBreak();
			$section->addText($issue->created,$fontStyle);
			
			$section->addTextBreak($lb);
		}
		
		
	/*	$section->addText(
			'"Great achievement is usually born of great sacrifice, '
				. 'and is never the result of selfishness." '
				. '(Napoleon Hill)',
			$fontStyle
		);
		
		$fontStyleName = 'oneUserDefinedStyle';
		$phpWord->addFontStyle(
			$fontStyleName,
			array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
		);
		$section->addText(
			'"The greatest accomplishment is not in never falling, '
				. 'but in rising again after you fall." '
				. '(Vince Lombardi)',
			$fontStyleName
		);
		
		$fontStyle = new \PhpOffice\PhpWord\Style\Font();
		$fontStyle->setBold(true);
		$fontStyle->setName('Tahoma');
		$fontStyle->setSize(13);
		$myTextElement = $section->addText('"Believe you can and you\'re halfway there." (Theodor Roosevelt)');
		$myTextElement->setFontStyle($fontStyle);*/

		// Saving the document as OOXML file...
		
		
		$file_name = "survey_report_".str_slug($item->owner_name)."_".$item->id;
		
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		
		\File::exists(public_path('uploads/survey/')) or mkdir(public_path('uploads/survey/'), 0777, true);
		$loc = public_path('uploads/survey/'.$file_name.'.docx');
		
		if (\File::exists($loc)) {
            unlink($loc);
        }
		
		$objWriter->save($loc);
		
		return response()->download($loc);
	}


}
