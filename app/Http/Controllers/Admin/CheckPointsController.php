<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Responcetext;
use App\Setting;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class CheckPointsController extends Controller
{
    
    public function index(Request $request)
    {
        return view('admin.check-list.index');
    }
    public function datatable(Request $request) {
        $record = Responcetext::where("responce_type","=","check_list");
        return Datatables::of($record)->make(true);
    }

    
    public function create()
    {
		
		Session::flash('flash_error',"No Access !");
        return redirect('admin/check-list');
			
		return view('admin.check-list.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'responce_type' => 'required',
            'title' => 'required',
            'desc' => 'required',
        ],[],trans('constant.label'));


        $requestData = $request->all();
       
        $module = Responcetext::create($requestData);
		
        if($module){
			Setting::updateOrCreate(['key'=>'last_update'],['key'=>'last_update','value'=>\Carbon\Carbon::now()]);
            $result['message'] = trans('common.responce_msg.record_created_succes');
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/check-list');
        }

    }
	public function show($id,Request $request)
    {
		$item = Responcetext::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/check-list');
		}
		return view('admin.check-list.show',compact('item'));
	}
	
    public function edit($id,Request $request)
    {
        $result = array();
		$item = Responcetext::where("id",$id)->first();
        
        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
			return view('admin.check-list.edit', compact('item'));
        }
		

    }

    public function update($id, Request $request)
    {
        $result = array();

        $this->validate($request, [
            'responce_type' => 'required',
            'desc' => 'required',
            'title' => 'required',
        ],[],trans('constant.label'));

        $item = Responcetext::where("id",$id)->first();
        $requestData = $request->all();
        
        if($item){
            $item->update($requestData);
			Setting::updateOrCreate(['key'=>'last_update'],['key'=>'last_update','value'=>\Carbon\Carbon::now()]);
            $result['message'] = trans('common.responce_msg.record_updated_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/check-list');
        }
        
    }
    

    public function destroy($id,Request $request)
    {
        $item = Responcetext::where("id",$id)->first();

        $result = array();

        if($item){
            $item->delete();
            $result['message'] = trans('common.responce_msg.record_deleted_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			return redirect('admin/check-list');
        }
    }


}
