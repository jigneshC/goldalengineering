<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    

	public function getLangTranslation(Request $request) {

        
        $files = ['auth','branches','categories','common','constant','email','issue','menu','role','survey','tooltip','user','validation'];
        $csv_data = [];
	
		foreach ($files as $jj => $file) {
			
			$langs = \Lang::get($file,[],'he');
			$langs_en = \Lang::get($file,[],'en');
			
			foreach ($langs as $key => $val) {
				if (is_array($val)) {
					foreach ($val as $k => $s) {
						
						if (is_array($s)) {
							foreach ($s as $d => $j) {
								if (!is_array($j)){
									$csv_data[] = ["en_value"=>$langs_en[$key][$k][$d],"he_value"=>$j,"module"=>$file,"key"=>$key,"key_2"=>$k,"key_3"=>$d];
								}
							}
						}else {
							$datak = "";
							if($k == "images.*"){ $datak = "images.*"; }else{
								$datak = $langs_en[$key][$k];
							}
							$csv_data[] = ["en_value"=>$datak,"he_value"=>$s,"module"=>$file,"key"=>$key,"key_2"=>$k,"key_3"=>""];
						}
					}
				} else {
					$csv_data[] = ["en_value"=>$langs_en[$key],"he_value"=>$val,"module"=>$file,"key"=>$key,"key_2"=>"","key_3"=>""];
				}
			}
		}
		
		//$fp = fopen('file.csv', 'w');

		foreach ($csv_data as $fields) {
			echo $fields['he_value']."<br/>";
			//fputcsv($fp, $fields);
		}

		//fclose($fp);

        print "<pre>";
		print_r($csv_data);
		//  var_export($csv_data);
        
        
      
    }
	
	
	public function experience(Request $request)
    {
        $result = array();
		
		$res = [];
		$res[] = ["id"=>"","desc"=>$request->search];
		
        $result['data'] = $res;
        $result['code'] = 200;

        return response()->json($result, $result['code']);
    }

}
