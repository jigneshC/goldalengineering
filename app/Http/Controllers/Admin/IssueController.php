<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Survey;
use App\Issue;
use App\Categories;
use App\Surveycategories;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon\Carbon;

class IssueController extends Controller
{

    public function index(Request $request)
    {
		return redirect('admin/survey');
		return view('admin.issues.index');
    }

    public function datatable(Request $request) {
        $record = Issue::select(['issue.*','dupfrom.survey_id as from_survey_id'])->where("issue.id",">",0);
		
		if ($request->has('from_survey_id') && $request->get('from_survey_id') != '' ) {
			$record->join('issue as dupfrom','issue.duplicate_from','=','dupfrom.id');
            $record->where('dupfrom.survey_id',$request->get('from_survey_id'));
        }else{
			$record->leftjoin('issue as dupfrom','issue.duplicate_from','=','dupfrom.id');
        }
		

		if ($request->has('status') && $request->get('status') != 'all' && $request->get('status') != '') {
            $record->where('issue.status',$request->get('status'));
        }
		if ($request->has('survey_id') && $request->get('survey_id') != '' ) {
            $record->where('issue.survey_id',$request->get('survey_id'));
        }
		
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }

        return Datatables::of($record)->make(true);
    }


	public function create($survey_id,Request $request)
    {
		$survey = Survey::where("id",$survey_id)->first();
		if(!$survey){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect()->back();
		}
		$categories = Categories::select(['id','name','slug','parent_id'])->Parentonly()->orderby('display_order','asc')->get();

		return view('admin.issues.create',compact('survey','categories'));
	}
	public function editImages($issue_id,Request $request)
    {
		$item = Issue::where("id",$issue_id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect()->back();
		}
		$ifile = null;
		if($request->has('selected_file')){
			$ifile =  $item->refefile->where('id',$request->selected_file)->first();
		}


		return view('admin.issues.image-update',compact('item','ifile'));
	}
	public function updateImages(Request $request)
    {
		$varr = [
            'issue_id' => 'required',
            'file_id' => 'required',
            'base_64_img' => 'required',
        ];

		$this->validate($request,$varr,[],trans('issue.label'));

		$item = Issue::where("id",$request->issue_id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect()->back();
		}

		$ifile =  $item->refefile->where('id',$request->file_id)->first();

		if(!$ifile){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect()->back();
		}

		updateBase64($request->base_64_img,$ifile);

		Session::flash('flash_success',trans('common.responce_msg.record_updated_succes'));
        return redirect()->back();

	}

	public function store(Request $request)
    {
        $result = array();



		$varr = [
            'survey_id' => 'required',
            'images.*' => 'mimes:jpeg,png,jpg,gif,svg',
            'image' => 'mimes:jpeg,png,jpg,gif,svg',
        ];

		$this->validate($request,$varr,[],trans('issue.label'));

        $survey = Survey::where("id",$request->survey_id)->first();

		if(!$survey){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect()->back();
		}

        $input = $request->except(['other','cost_detail_img','image','images','file','files','survey_id','cat_img','show_default_price_detail']);
		$input['other_json_data'] = json_encode($request->get("other",[]));
		$image_selection = [];
		if(isset($request->cat_img) && $request->cat_img != "" ){

			foreach($request->cat_img as $p=> $valp){
				$image_selection[] = ["img_id"=>$p,"hint"=>""];
			}
		}

		if(isset($request->child_category_id) && $request->child_category_id != "" ){
			$cat = Categories::where("id",$request->child_category_id)->first();
			$surcat = Surveycategories::where("unique_id",$request->child_category_id)->first();
			if(is_numeric($request->child_category_id) && $cat){
				$input['child_category_id'] = $request->child_category_id;
				$input['child_unique_id'] = null;
			}else if($surcat){
				$input['child_category_id'] = null;
				$input['child_unique_id'] = $request->child_category_id;
			}

		}

		$input['img_hint'] =  json_encode($image_selection);

		$input['unique_id'] = uniqid();
		$input['created'] =  Carbon::now()->format('Y-m-d');
		$input['surveyor_id'] =  \Auth::user()->id;
		$input['survey_id'] =  $request->survey_id;
		$input['local_id'] =  0;
		$input["show_default_price_detail"] = $request->get("show_default_price_detail",0);
		//$input["cost_detail_report_flag"] = $request->get("cost_detail_report_flag",0);
		$item =Issue::create($input);

        if($item){
			$survey->issue_count = $survey->issue->count();
			$survey->save();

            if($request->hasFile('images'))
			{
				$files = $request->file('images');
				uploadModalReferenceFile($files,'uploads/issues/'.$item->id,'issue_id',$item->id,'issue_image',[]);
			}
			if($request->hasFile('image'))
			{
				$files = [$request->file('image')];
				uploadModalReferenceFile($files,'uploads/issues/'.$item->id,'issue_id',$item->id,'issue_image',[]);
			}
			if($request->has('cost_detail_img') && $request->cost_detail_img !="" ){
				if($item->costImage){
					removeRefeImage($item->costImage);
				}
				uploadBase64($request->cost_detail_img,'uploads/issues/'.$item->id,'issue_id_cost',$item->id,'cost_detail_img' ,[]);
			}
            $result['message'] = trans('common.responce_msg.record_created_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);

			if(Session::has('previous_url') && Session::get('previous_url') != ""){
                $pre_url = Session::get('previous_url');
                $request->session()->replace(['previous_url' => '']);
				return redirect($pre_url);
			}
            return redirect('admin/survey/'.$item->survey_id);
        }

    }

    public function show($id,Request $request)
    {
		$item = Issue::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
		}
		return view('admin.issues.show',compact('item'));
	}

    public function edit($id,Request $request)
    {
		
        $result = array();
        $item = Issue::findOrFail($id);
		$categories = Categories::select(['id','name','slug','parent_id'])->Parentonly()->orderby('display_order','asc')->get();

		$cat = Categories::where('id',21)->first();

        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;

			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
        }

		$survey = Survey::where("id",$item->survey_id)->first();
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            return view('admin.issues.edit', compact('item','categories','cat','survey'));
        }


    }

    public function update($id, Request $request)
    {
		$result = array();

		$varr = [
            'images.*' => 'mimes:jpeg,png,jpg,gif,svg',
            'image' => 'mimes:jpeg,png,jpg,gif,svg',
        ];

		$this->validate($request,$varr,[],trans('issue.label'));

        $item = Issue::where("id",$id)->first();
        $requestData = $request->except(['other','cost_detail_img','image','images','file','files','cat_img','show_default_price_detail','saveandnext']);
		$requestData['other_json_data'] = json_encode($request->get("other",[]));
		
		$image_selection = [];
		if(isset($request->cat_img) && $request->cat_img != "" ){

			foreach($request->cat_img as $p=> $valp){
				$image_selection[] = ["img_id"=>$p,"hint"=>""];
			}
		}
		$requestData['img_hint'] =  json_encode($image_selection);

		if(isset($request->child_category_id) && $request->child_category_id != "" ){
			$cat = Categories::where("id",$request->child_category_id)->first();
			$surcat = Surveycategories::where("unique_id",$request->child_category_id)->first();
			if(is_numeric($request->child_category_id) && $cat){
				$requestData['child_category_id'] = $request->child_category_id;
				$requestData['child_unique_id'] = null;
			}else if($surcat){
				$requestData['child_category_id'] = 0;
				$requestData['child_unique_id'] = $request->child_category_id;
			}

		}
		$requestData["show_default_price_detail"] = $request->get("show_default_price_detail",0);
		//$requestData["cost_detail_report_flag"] = $request->get("cost_detail_report_flag",0);
		
		if($item){
            $item->update($requestData);
			if($request->hasFile('images'))
			{
				$files = $request->file('images');
				uploadModalReferenceFile($files,'uploads/issues/'.$item->id,'issue_id',$item->id,'issue_image',[]);
			}
			if($request->hasFile('image'))
			{
				$files = [$request->file('image')];
				uploadModalReferenceFile($files,'uploads/issues/'.$item->id,'issue_id',$item->id,'issue_image',[]);
			}
			if($request->has('cost_detail_img') && $request->cost_detail_img !="" ){
				if($item->costImage){
					removeRefeImage($item->costImage);
				}
				uploadBase64($request->cost_detail_img,'uploads/issues/'.$item->id,'issue_id_cost',$item->id,'cost_detail_img' ,[]);
			}
			
            $result['message'] = trans('common.responce_msg.record_updated_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
		
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else if($request->has('saveandnext') && $request->saveandnext != "" && $item){
			Session::flash('flash_message',$result['message']);
			$next_id = $this->findnext($item);	
			$nextissue = Issue::where("id",$next_id)->first();
			if($nextissue){
				return redirect('admin/issues/'.$nextissue->id."/edit");
			}
			return redirect('admin/issues/'.$item->next()->id."/edit");
		}else{
            Session::flash('flash_message',$result['message']);
			if(Session::has('previous_url') && Session::get('previous_url') != ""){
				$pre_url = Session::get('previous_url');
                $request->session()->replace(['previous_url' => '']);
				return redirect($pre_url);
			}
			return redirect('admin/survey/'.$item->survey_id);

        }

    }


    public function findnext($_issue){
		$cat_group = [];
		
		$item = $_issue->survey;
		foreach($item->issue as $k => $issue){
			if(isset($issue->getsubcategory) && isset($issue->category) ){
				//Specific category data
				if(1){
					$_CATID = $issue->getsubcategory->id;

					if(!isset($cat_group[$issue->category_id]) && isset($issue->category)){
						$cat_group[$issue->category_id] = ['name'=>$issue->category->name,'items'=>[]];
					}

					if(isset($cat_group[$issue->category_id]['items'][$_CATID]) && isset($issue->getsubcategory)){
						$cat_group[$issue->category_id]['items'][$_CATID]['issue_id'][] = $issue->id;
					}else if(isset($issue->getsubcategory)){
						$cat_group[$issue->category_id]['items'][$_CATID]['name'] = $issue->getsubcategory->name;
						$cat_group[$issue->category_id]['items'][$_CATID]['issue_id'][] = $issue->id;
					}
				}
			}
		}
		$issueids = [];
		foreach($cat_group as $cb){
			foreach($cb['items'] as $cbi){
				foreach($cbi['issue_id'] as $issue_id){
					$issueids[] = $issue_id;	
				}
			}
		}
		
		$index = array_search($_issue->id, $issueids);
		//if($index !== false && $index > 0 ) $prev = $issueids[$index-1];
		if($index !== false && $index < count($issueids)-1){
			$next = $issueids[$index+1];
		}else if(isset($issueids[0])){
			$next = $issueids[0];
		}else{
			$next = 0;
		} 
		return $next;
	}
    public function destroy($id,Request $request)
    {
        $item = Issue::where("id",$id)->first();
		$is_hard_delete = 0;
		

		if(!$item){
			$item = Issue::withTrashed()->where('id',$id)->first();
			$is_hard_delete = 1;
		}

		$survey_id = 0;

        $result = array();

        if($item){
			$survey = Survey::where("id",$item->survey_id)->first();
			if($is_hard_delete){
				foreach($item->refefile as $rf){
					removeRefeImage($rf);
				}
				$item->forceDelete();
			}
            $item->delete();
            $result['message'] = trans('common.responce_msg.record_deleted_succes');
            $result['code'] = 200;
			$survey_id = $item->survey_id;
			if(!$survey){
				$survey->issue_count = $newS->issue->count();
				$survey->save();
			}

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			if($survey_id){
				return redirect('admin/survey/'.$survey_id);
			}else{
				return redirect('admin/survey');
			}
		}


    }


}
