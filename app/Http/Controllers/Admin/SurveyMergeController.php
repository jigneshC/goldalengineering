<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Survey;
use App\Branches;
use App\Issue;
use App\Responcetext;
use PhpOffice\PhpWord\Element\TextRun;

use Carbon\Carbon;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;
use App\Refefile;

class SurveyMergeController extends Controller
{

    public function merge(Request $request)
    {
		$record = null;
		$surveyor = \Auth::user();
		if ($request->has('survey_id') && $request->get('survey_id') != '' && $request->get('survey_id') != 0 ) {
            $record = Survey::with('surveyor')->where("id",$request->survey_id)->first();
        }
		
        return view('admin.survey.merge',compact('record'));
    }
    public function mergeSubmit(Request $request) {
		
		$this->validate($request, [
            'master_survey' => 'required',
            'sub_survey' => 'required'
		]);
		
		$sub_survey = explode(",",$request->sub_survey);

		if (($key = array_search($request->master_survey, $sub_survey)) !== false) {
			unset($sub_survey[$key]);
		}
		
		if(!\Schema::hasColumn("survey", "merge_ids")){
			$q = "ALTER TABLE `survey` ADD `merge_ids` VARCHAR(55) NULL AFTER `enable_local_sync`;" ;
			\DB::statement($q);
		}
		
		$survey = Survey::where("id",$request->master_survey)->first();
			 
		if(!$survey){
			 Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
		}else{
			$newS = $survey->replicate();
			$newS->unique_id = $survey->unique_id."_".rand(0,999);
			$newS->parent_id = 0;
			$newS->merge_ids = $survey->id.",".$request->sub_survey;
			$newS->created = Carbon::now();
			$newS->survey_date = Carbon::now();
			$newS->save();
			
			
			$this->mergeIssue($survey->issue,$newS);
			
			$issues = Issue::whereIn("survey_id",$sub_survey)->orderby("survey_id","asc")->get();
			
			$this->mergeIssue($issues,$newS);
			
			$newS->issue_count = $newS->issue->count();
			$newS->save();
			
			
		}
		
		Session::flash('flash_success',"Survey Merged");
        return redirect('admin/survey');
		
	}
	public function mergeIssue($issues,$newS){
		foreach($issues as $issue){
				
				
				$cunitqe_id = null;
				if($issue->child_category_id && $issue->child_category_id > 0){
					
				}else if($issue->child_unique_id && $issue->child_unique_id !="" && $issue->surveycategory){
					
					$newcat = $issue->surveycategory->replicate();
					$cunitqe_id = $newcat->unique_id."_".rand(0,999);
					$newcat->unique_id =  $cunitqe_id;
					$newcat->survey_unique_id =  $newS->unique_id;
					$newcat->save();
				}
				
				$newI = $issue->replicate();
				$newI->child_unique_id = $cunitqe_id;
				$newI->duplicate_from = $issue->id;
				$newI->survey_id = $newS->id;
				$newI->unique_id = $issue->unique_id."_".rand(0,999);
				$newI->created = Carbon::now();
				$newI->save();
				
				foreach($issue->refefile as $ref){
					$newR = $ref->replicate();
					$newR->refe_field_id = $newI->id;
					$newR->refe_code = $newS->id;
					$newR->save();
					
					//$ress[]= "<br/>-s=".$newS->id." i=".$newI->id." r=".$newR->id." : os=".$survey->id." oi=".$issue->id." or=".$ref->id;
					
					resetFolder($newR,$ref->refe_field_id);
				}
		}
	}
	public function search(Request $request)
    {
        $result = array();

        $data =Survey::select('id','owner_name','owner_email','owner_phone','address','survey_date');

        if ($request->has('master_survey') &&  $request->get('master_survey') != '') {
			$data->where('id', '!=',$request->get('master_survey'));
		}
        if ($request->has('search') &&  $request->get('search') != '') {
			$q= $request->search;
            $data->where('id', 'LIKE', '%'.$q.'%')
			->orWhere('owner_name', 'LIKE', '%'.$q.'%')
			->orWhere('owner_email', 'LIKE', '%'.$q.'%')
			->orWhere('owner_phone', 'LIKE', '%'.$q.'%')
			->orWhere('address', 'LIKE', '%'.$q.'%')
			->orWhere('survey_date', 'LIKE', '%'.$q.'%');
        }
       

		$result['data'] = $data->orderby("id",'ASC')->get()->toArray();
        $result['code'] = 200;

        return response()->json($result, $result['code']);
		
        
    }

}
