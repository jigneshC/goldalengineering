<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Responcetext;
use App\Setting;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class PrefillImagesController extends Controller
{
    
    public function index(Request $request)
    {
        return view('admin.prefill-images.index');
    }
    public function datatable(Request $request) {
        $record = Responcetext::with('refefile')->where("responce_type","=","static_images");
        return Datatables::of($record)->make(true);
    }

    
    public function create()
    {
		return view('admin.prefill-images.create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'responce_type' => 'required',
            'title' => 'required',
            'slug' => 'required',
        ],[],trans('constant.label'));


		
        $requestData = $request->except(['image','images','file','files','professional_materials','other_information_a','other_information_b','accessories']);
       
        $module = Responcetext::create($requestData);
		
        if($module){
			if($request->has('accessories')){
				addRefeText($request->accessories,"rt_id" ,$module->id ,"default_accessories" ,0);
			}
			if($request->has('professional_materials')){
				addRefeText($request->professional_materials,"rt_id" ,$module->id ,"default_pf_materials" ,0);
			}
			if($request->has('other_information_a')){
				addRefeText([$request->other_information_a],"rt_id" ,$module->id ,"default_spinfoa" ,0);
			}
			if($request->has('other_information_b')){
				addRefeText([$request->other_information_b],"rt_id" ,$module->id ,"default_spinfob" ,0);
			}
			
			
			
			
			if($request->hasFile('images'))
			{
				$files = $request->file('images');
				uploadModalReferenceFile($files,'uploads/rt/'.$module->id,'rt_id',$module->id,'rt_image',[]);
			}
			if($request->hasFile('image'))
			{
				$files = [$request->file('image')];
				uploadModalReferenceFile($files,'uploads/rt/'.$module->id,'rt_id',$module->id,'rt_image',[]);
			}
            $result['message'] = trans('common.responce_msg.record_created_succes');
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/prefill-images');
        }

    }
	
	public function edit($id,Request $request)
    {
        $result = array();
		$item = Responcetext::where("id",$id)->first();
        
        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
			return view('admin.prefill-images.edit', compact('item'));
        }
		

    }

    public function update($id, Request $request)
    {
        $result = array();

		$rules = [
            'responce_type' => 'required',
            'title' => 'required',
            'slug' => 'required'
        ];
		
		$item = Responcetext::where("id",$id)->first();
		if($item && $item->refefile){
			
		}else{
			//$rules['image'] = "required";
		}
		
        $this->validate($request,$rules ,[],trans('constant.label'));


		
        $requestData = $request->except(['image','images','file','files','professional_materials','other_information_a','other_information_b','accessories']);
		$item = Responcetext::where("id",$id)->first();
       
        
        if($item){
			
			if($request->has('accessories')){
				addRefeText($request->accessories,"rt_id" ,$item->id ,"default_accessories" ,0);
			}
			if($request->has('professional_materials')){
				addRefeText($request->professional_materials,"rt_id" ,$item->id ,"default_pf_materials" ,0);
			}
			if($request->has('other_information_a')){
				addRefeText([$request->other_information_a],"rt_id" ,$item->id ,"default_spinfoa" ,0);
			}
			if($request->has('other_information_b')){
				addRefeText([$request->other_information_b],"rt_id" ,$item->id ,"default_spinfob" ,0);
			}
			
            $item->update($requestData);
			
			if($request->hasFile('images'))
			{
				$files = $request->file('images');
				uploadModalReferenceFile($files,'uploads/rt/'.$item->id,'rt_id',$item->id,'rt_image',[]);
			}
			if($request->hasFile('image'))
			{
				$files = [$request->file('image')];
				uploadModalReferenceFile($files,'uploads/rt/'.$item->id,'rt_id',$item->id,'rt_image',[]);
			}
			
			Setting::updateOrCreate(['key'=>'last_update'],['key'=>'last_update','value'=>\Carbon\Carbon::now()]);
            $result['message'] = trans('common.responce_msg.record_updated_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/prefill-images');
        }
        
    }

    

    public function destroy($id,Request $request)
    {
        $item = Responcetext::where("id",$id)->first();

        $result = array();

        if($item){
			if($item->refefile){
				removeRefeImage($item->refefile);
			}
            $item->delete();
            $result['message'] = trans('common.responce_msg.record_deleted_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			return redirect('admin/prefill-images');
        }
    }


}
