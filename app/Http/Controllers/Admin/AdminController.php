<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;
use Carbon\Carbon;

use App\User;
use App\Categories;
use App\Branches;
use App\Setting;
use App\Survey;
use App\Issue;
use App\Refefile;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
	
	public function surveyDublicate($uid,Request $request)
    {
		$item = Survey::where("unique_id",$uid)->first();
		if(!$item){
			return redirect()->back();
		}else{
			return view('admin.survey.dublicate',compact('item'));
		}
	}		
	public function doSurveyDublicate(Request $request)
    {
		$result = ["data" => [], "code" => 400, "messages" => ""];
		
		$rules = array(
            'unique_id' => 'required',
        );

		$ress = [];
		$validator = \Validator::make($request->all(), $rules, [],trans('survey.label'));
		
		if ($validator->fails())
        {
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
			
		}else{
			 $survey = Survey::where("unique_id",$request->unique_id)->first();
			 
			if(!$survey){
				 $result['code'] = 400;
				 $result['messages'] = trans('common.responce_msg.data_not_found');
			}else{
				$newS = $survey->replicate();
				$newS->unique_id = $survey->unique_id."_".rand(0,999);
				$newS->parent_id = $survey->id;
				//$newS->created = Carbon::now();
				//$newS->survey_date = Carbon::now();
				$newS->created_at = Carbon::now();
				$newS->save();
				
				if($request->has("duplicate_option") && $request->duplicate_option == "partial"){
					$newS->issue_count = 0;
					$newS->save();
				}else{
					foreach($survey->textChecklist as $tc){
						$newTc = $tc->replicate();
						$newTc->refe_field_id = $newS->id;
						$newTc->save();
					}
					foreach($survey->issue as $issue){
						
						
						$cunitqe_id = null;
						if($issue->child_category_id && $issue->child_category_id > 0){
							
						}else if($issue->child_unique_id && $issue->child_unique_id !="" && $issue->surveycategory){
							
							$newcat = $issue->surveycategory->replicate();
							$cunitqe_id = $newcat->unique_id."_".rand(0,999);
							$newcat->unique_id =  $cunitqe_id;
							$newcat->survey_unique_id =  $newS->unique_id;
							$newcat->save();
						}
						
						$newI = $issue->replicate();
						$newI->child_unique_id = $cunitqe_id;
						$newI->survey_id = $newS->id;
						$newI->duplicate_from = $issue->id;
						$newI->unique_id = $issue->unique_id."_".rand(0,999);
						$newI->created = Carbon::now();
						$newI->save();
						
						foreach($issue->refefile as $ref){
							$newR = $ref->replicate();
							$newR->refe_field_id = $newI->id;
							$newR->refe_code = $newS->id;
							$newR->save();
							
							//$ress[]= "<br/>-s=".$newS->id." i=".$newI->id." r=".$newR->id." : os=".$survey->id." oi=".$issue->id." or=".$ref->id;
							
							resetFolder($newR,$ref->refe_field_id);
						}
					}
				}
				
				// Rest order of images
				Survey::fileOrder($newS->id);
				
				$result['code'] = 200;
				
			}
		}
		
		if($result['code'] == 200){
			Session::flash('flash_success',$result['messages']);
			return redirect('admin/survey?surveyor_id=0');
		}else{
			Session::flash('flash_error',$result['messages']);
			 return redirect()->back();
		}
		
        
		
		
	}
	
    public function index()
    {
		return view('admin.dashboard');
    }
	
	public function export(Request $request)
    {
		$tbl = $request->export;
		
        $filename = $tbl.Carbon::now('Asia/Kolkata')->format("d-m-Y").".csv";
        //$modal = 'App\\'.ucfirst($tbl);
		//$list = $modal::get()->toArray();
		
		$list =  \DB::table($tbl)->get()->toArray();
             
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename='.$filename,
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];
        if(isset($list[0])){
			$list = json_encode($list);
			$list = json_decode($list,true);
			array_unshift($list, array_keys($list[0]));

			$callback = function() use ($list) 
			{
				 $FH = fopen('php://output', 'w');
				 foreach ($list as $row) { 
					 fputcsv($FH, $row);
				 }
				 fclose($FH);
			};

         return Response::stream($callback, 200, $headers);
        }
    }
	
	public function renamefiles(Request $request)
    {
		//$this->testfileinfo($request);
		$this->setfileinfo($request);
		exit;
        $files = Refefile::all();
		
		foreach($files as $file){
			
			$pathinfo = pathinfo($file->file_path);
			if(isset($pathinfo['extension'])){
					
				$name = uniqid()."_".$file->refe_field_id.".".$pathinfo['extension'];	 
				if($file->file_path != ""){
					$new_path = public_path('storage') .'/'.$file->refe_file_path."/".$name;
					echo "<br/>Real: ".$file->file_path. "  -to- ".$new_path;
					//File::move($file->file_path,$new_path);
				}
				if($file->file_thumb_path != ""){
					$new_path2 = public_path('storage') .'/'.$file->refe_file_path."/thumb/".$name;
					echo "<br/>Thumb: ".$file->file_thumb_path. "  -to- ".$new_path2;
					//File::move($file->file_thumb_path,$new_path2);
					
					
				}
				echo "<br/>: ".$file->file_url. " <br/><br/> ";
				// $file->refe_file_name = $name; $file->save();
			}
			
		}
    }
	
	public function resetissuecount()
    {
		$survey = Survey::orderby("id","DESC")->limit(200)->get();
		foreach($survey as $s){
			if($s->issue_count != $s->issue->count()){
				echo "<br/>".$s->id." =>".$s->issue_count ." - ".$s->issue->count();
				$s->issue_count = $s->issue->count();
				$s->save();
			}
		}
		exit;
	}
	
	public function compare($s1,$s2,Request $request)
    {
		
		$cnt= Issue::where("survey_id",$s2)->where("duplicate_from",">",0)->count();
		
		if($cnt >0){
			$issuel1 = Issue::where("survey_id",$s1)->orderby("child_category_id","ASC")->orderby("location","ASC")
			->orderby("unit_cost","ASC")->orderby("number_of_unit","ASC")->orderby("total_cost","ASC")->get();
			
			
			
			for($i=0;$i< $issuel1->count(); $i++){
				$issuel_2 = Issue::where("survey_id",$s2)->where("duplicate_from","=",$issuel1[$i]->id)->withTrashed()->first();

				if($issuel_2){
				$id = ($issuel_2)? $issuel_2->id : 'NA';
				$created = ($issuel_2)? $issuel_2->total_cost : 'NA';
				$local_id = ($issuel_2)? $issuel_2->local_id : 'NA';
				$child_category_id = ($issuel_2)? $issuel_2->getsubcategory->id : 'NA';
				$category_id = ($issuel_2)? $issuel_2->category_id : 'NA';
				$image = ($issuel_2)? $issuel_2->refefile->count() : 'NA';
				echo "</br>BB total_cost ".$issuel1[$i]->total_cost." | ".$created." ID ".$issuel1[$i]->id." | ".$id." -- . MainChapter ".$issuel1[$i]->category_id." | ".$category_id." -- . Chapter ".$issuel1[$i]->getsubcategory->id." | ".$child_category_id." -- . Image ".$issuel1[$i]->refefile->count()." | ".$image;
				}
			}
		}else{
			$issuel1 = Issue::where("survey_id",$s1)->orderby("child_category_id","ASC")->orderby("location","ASC")
			->orderby("unit_cost","ASC")->orderby("number_of_unit","ASC")->orderby("total_cost","ASC")->get();
			$issuel2 = Issue::where("survey_id",$s2)->orderby("child_category_id","ASC")->orderby("location","ASC")
			->orderby("unit_cost","ASC")->orderby("number_of_unit","ASC")->orderby("total_cost","ASC")->get();
			
			for($i=0;$i< $issuel1->count(); $i++){
				$id = ($issuel2[$i])? $issuel2[$i]->id : 'NA';
				$created = ($issuel2[$i])? $issuel2[$i]->total_cost : 'NA';
				$local_id = ($issuel2[$i])? $issuel2[$i]->local_id : 'NA';
				$child_category_id = ($issuel2[$i])? $issuel2[$i]->child_category_id : 'NA';
				$image = ($issuel2[$i])? $issuel2[$i]->refefile->count() : 'NA';
				echo "</br> Created ".$issuel1[$i]->total_cost." | ".$created." ID ".$issuel1[$i]->id." | ".$id." -- . Cat ".$issuel1[$i]->child_category_id." | ".$child_category_id." -- . Image ".$issuel1[$i]->refefile->count()." | ".$image;
			}
		
		}
		
		
		
	}
	//Reset missing isuee unique id
	public function resetUniqueId(Request $request)
    {
		if($request->has('log_id')){
			$log = \App\Logs::whereId($request->log_id)->first();
			
			$properties = json_decode($log->properties,true);
		//	$sync_data = json_decode($properties['sync_data'],true);
		//	$issues = $sync_data['issues'];
			echo $log->description; exit;
			
			if($request->has('dd')){
				echo "<pre>"; print_r($properties); 
			//	echo "<pre>"; print_r($issues); 
				exit;
			}
			
			$missing_counter = 0;
			foreach($issues as $k=> $issue){
				echo "<br/>--------------------------------------------------";
				echo "<br/>Check : ***** local id =>".$issue['id']." ***** survey_id =>".$issue['survey_id']." ***** child_category_id".$issue['child_category_id']." ***** unique_id=>".$issue['unique_id'];
				$issuel = Issue::where("unique_id",$issue['unique_id'])->first();
				if($issuel){
					echo "<br/>Exist_ : ***** local id =>".$issuel->local_id." ***** survey_id =>".$issuel->survey_id." ***** child_category_id".$issuel->child_category_id." ***** unique_id=>".$issuel->unique_id;
				}else{
					$issued = Issue::where("local_id",$issue['id'])->where("id",">",19590)->first();
					if($issued){
						$missing_counter = $missing_counter +1;
						echo "<br/>Miss_ : ***** local id =>".$issued->local_id." ***** survey_id =>".$issued->survey_id." ***** child_category_id".$issued->child_category_id." ***** unique_id=>".$issued->unique_id;
						
						if($request->has('reset_unique_id') && ($issued->unique_id == "0" || $issued->unique_id == 0 || $issued->unique_id == "" || !$issued->unique_id )){
							$issued->unique_id = $issue['unique_id'];
							$issued->save();
							echo "---<b>Updating </b>".$issue['unique_id'];
							
						}
					}else{
						echo "<br>---<b>Must be deleted from admin </b>".$issue['unique_id'];
					}
				}
			}

		}
		
		echo " <br/><br/><br/> Total missing =>".$missing_counter;
			
		exit;
	}
	public function setfileinfo($request)
    {
		if($request->has('survey_id')){
			$item = \App\Survey::where("id",$request->survey_id)->first();
			if($item){
				$isuee_ids = $item->issue->pluck("id");
				$files = Refefile::where("refe_table_field_name","issue_id")->whereIn("refe_field_id",$isuee_ids)->update(["refe_code"=>$item->id]);
				dd($files);
			}
		}
		
	}
	public function fileOrder(Request $request)
    {
		if($request->has('survey_id')){
			Survey::fileOrder($request->get('survey_id'));
		}
	}
	public function testfileinfo($request)
    {
		if($request->has('survey_id')){
			$newS = \App\Survey::where("id",$request->survey_id)->first();
			if($newS){
				
				foreach($newS->issue as $newI){
					foreach($newI->refefile as $ref){
						echo "<br/>-".$ref->id;
					}
				}
			}
		}
		
	}
	public function recoverItem(Request $request)
    {
		
		$result = array();

        $rules = array(
            'item' => 'required',
            'id' => 'required',
        );
		$validator = \Validator::make($request->all(), $rules,[]);

        if ($validator->fails())
        {
            $validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json(['message' =>$messages,'success' => false,'status' => 400],400);
        }
		
		$ob = null;
		$modal = $request->item;
		$id = $request->id;
		if($modal == 'survey'){
			$ob = Survey::withTrashed()->where('id',$id)->first();
			$issues = Issue::withTrashed()->where("survey_id",$id)->get();
			foreach($issues as $issue){
				$issue->restore();
			}
			$ob->restore();
		}else if($modal == 'issue'){
			$ob = Issue::withTrashed()->where('id',$id)->first();
			$ob->restore();
		}
		
		if($ob){
            $result['message'] = \Lang::get('common.responce_msg.record_restored_succes');;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('common.responce_msg.data_not_found');;
            $result['code'] = 400;
        }


        return response()->json($result, $result['code']);
	}
}
