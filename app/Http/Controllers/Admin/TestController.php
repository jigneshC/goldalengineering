<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Survey;
use App\Branches;
use App\Issue;
use App\Responcetext;
use PhpOffice\PhpWord\Element\TextRun;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;
use App\User;
use App\Refefile;

class TestController extends Controller
{
    public function phpinfoo(Request $request){
		echo phpinfo();
	}
    
	public function exportPlace($id,Request $request)
    {
		$hidprice = 0;
		$free_text = 0;
		if($request->has('price') && $request->price == 1){
			$hidprice = 1;
		}
		if($request->has('free_text') && $request->free_text == 1){
			$free_text = 1;
		}
		$_category_id = 0;
		if($request->has('category_id')){
			$_category_id = $request->category_id;
		}
		//return $this->test($request); exit;
		\App::setLocale("he");
		
		$logo = public_path('assets/images/logo.png');
		$name = "";
		
		$item = Survey::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
		}
		
		$licence_no = "";
		$experience = [];
		$qualification = [];
		
		if($item->surveyor){
			$licence_no = $item->surveyor->licence_no;
			$qualification = $item->surveyor->qualification; 
			$experience = $item->surveyor->experience;
		}
		
		$is_fullsizelogo = 0;
		if($item->branch && $item->branch->refeimg && $item->branch->refeimg->file_path != ""){
			$logo = $item->branch->refeimg->file_path;
			$name = $item->branch->name;
			$is_fullsizelogo = $item->branch->is_fullsizelogo;
		}
		
		$sign = "";
		if($item->surveyor && $item->surveyor->signature){
			$sign = $item->surveyor->signature->file_path;
		}else{
			$rf = Responcetext::where("slug","signature")->first();
			if($rf && $rf->refefile &&  $rf->refefile->file_path != ""){
				$sign = $rf->refefile->file_path;
			}
		}
		
		$align = "left";
		$opalign = "right";
		
		$lb = 1;
		$fontStyleh = new \PhpOffice\PhpWord\Style\Font();
		if(\App::getLocale() == 'he'){
			$fontStyleh->setRTL(true);
			$fontStyleh->setLang('he-IL');
			$align = "right";
			$opalign = "left";
			$lb = 2;
		}else{
			$fontStyleh->setRTL(false);
			$fontStyleh->setLang('en');
			$align = "left";
			$opalign = "right";
			$lb = 1;
		}
        
		$fontStyleh->setBold(true);
		$fontStyleh->setName('David');
		$fontStyleh->setSize(11);
		
		$fontStyle = new \PhpOffice\PhpWord\Style\Font();
		if(\App::getLocale() == 'he'){
			$fontStyle->setRTL(true);
			$fontStyle->setLang('he-IL');
		}else{
			$fontStyle->setRTL(false);
			$fontStyle->setLang('en');
		}
        
		$fontStyle->setBold(false);
		$fontStyle->setName('David');
		$fontStyle->setSize(11);
		
		$bluetitle = new \PhpOffice\PhpWord\Style\Font();
		$bluetitle->setRTL(true);
		$bluetitle->setBold(true);
		$bluetitle->setName('David');
		$bluetitle->setSize(18);
		$bluetitle->setLineHeight(0.01);
		$bluetitle->setColor("2b5ca7");
	
		
		$issuetitle = new \PhpOffice\PhpWord\Style\Font();
		$issuetitle->setRTL(true);
		$issuetitle->setBold(true);
		$issuetitle->setName('David');
		$issuetitle->setSize(18);
		$issuetitle->setColor("000000");
		
		
		$style = array('rtl' => true,'font-weight'=>'bold','size'=>11);
		$style_ul = array('rtl' => true,'font-weight'=>'bold','size'=>11,'bidi',true,'underline' => \PhpOffice\PhpWord\Style\Font::UNDERLINE_SINGLE);
		$borderStyle = array('borderSize' => 6,'borderColor' => '000000');
		$cellHCentered = array('alignment' => 'right');
        $cellHEnd = array('alignment' => 'right');
        $cellVCentered = array('valign' => 'center');
		$cs4 = array('gridSpan' => 4);
		$cs3 = array('gridSpan' => 3);
		$cs2 = array('gridSpan' => 2);
		$style_h = array('rtl' => true,'bold'=>true,'size'=>11);
		$style_hp = array('rtl' => true,'bold'=>true,'size'=>15);
		$style_hp_ul = array('rtl' => true,'bold'=>true,'size'=>15,'underline' => \PhpOffice\PhpWord\Style\Font::UNDERLINE_SINGLE);
		$style_s = array('rtl' => true,'size'=>11);
		$cell_c = array('alignment' => 'center');
        $cell_r = array('alignment' => 'right');
        $cell_vc = array('valign' => 'center');
		
		
	//	$bluetitle->setUnderline("single");
		
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
	//	$phpWord->getSettings()->setUpdateFields(true);
		$phpWord->setDefaultFontName('David');
		
				// Define styles
		$multipleTabsStyleName = 'multipleTab';
		$phpWord->addParagraphStyle(
			$multipleTabsStyleName,
			array(
				'tabs' => array(
					new \PhpOffice\PhpWord\Style\Tab('left', 1550),
					new \PhpOffice\PhpWord\Style\Tab('center', 3200),
					new \PhpOffice\PhpWord\Style\Tab('right', 5300),
				),
			)
		);
		$phpWord->addParagraphStyle('pStyle', array('align' => 'center', 'spaceAfter' => 100));
		
		$rightTabStyleName = 'rightTab';
		$phpWord->addParagraphStyle($rightTabStyleName, array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('right', 10000))));
		$leftTabStyleName = 'centerTab';
		$phpWord->addParagraphStyle($leftTabStyleName, array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('center', 4680))));

		$section = $phpWord->addSection($fontStyle);
		
		$subsequent = $section->addHeader();
		
		$footer = $section->createFooter();
		$footer->addPreserveText(' {NUMPAGES} מתוך {PAGE} עמוד');
		
		if($is_fullsizelogo){
			$subsequent->addImage($logo, array('height' => 80,'width' => 500,'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
		}else{
			$subsequent->addImage($logo, array('height' => 50,'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
			
			if($item->branch){
			$title_1 = str_replace(array('>','<'), ' ',$item->branch->title_1);
			$title_2 = str_replace(array('>','<'), ' ',$item->branch->title_2);
			$phone_1 = str_replace(array('>','<'), ' ',$item->branch->phone_1);
			$phone_2 = str_replace(array('>','<'), ' ',$item->branch->phone_2);
			$email_1 = str_replace(array('>','<'), ' ',$item->branch->email_1);
			$email_2 = str_replace(array('>','<'), ' ',$item->branch->email_2);
			
			
			$table = $subsequent->addTable();
			
			$table->addRow();
			$cell = $table->addCell(5200);
			$textrun = $cell->addTextRun($cellHCentered);
			$textrun->addText($title_1, $style);
			
			$cell = $table->addCell(400);
			$textrun = $cell->addTextRun($cell_r);
			$textrun->addText(" ",$style_s);
					
			$cell = $table->addCell(5200);
			$textrun = $cell->addTextRun([]);
			$textrun->addText($title_2, $style);
			
			$table->addRow();
			$cell = $table->addCell(5200);
			$textrun = $cell->addTextRun($cellHCentered);
			$textrun->addText($phone_1, $style);
			
			$cell = $table->addCell(400);
			$textrun = $cell->addTextRun($cell_r);
			$textrun->addText(" ",$style_s);
					
			$cell = $table->addCell(5200);
			$textrun = $cell->addTextRun([]);
			$textrun->addText($phone_2, $style);
			
			$table->addRow();
			$cell = $table->addCell(5200);
			$textrun = $cell->addTextRun($cellHCentered);
			$textrun->addText($email_1, $style);
			
			$cell = $table->addCell(400);
			$textrun = $cell->addTextRun($cell_r);
			$textrun->addText(" ", null, 'pStyle');
					
			$cell = $table->addCell(5200);
			$textrun = $cell->addTextRun([]);
			$textrun->addText($email_2, $style);
			
		
			//$subsequent->addText("$title_1    $title_2", null, 'pStyle');
			//$subsequent->addText("$phone_1  /  $phone_2", null, 'pStyle');
			//$subsequent->addText("$email_1  /  $email_2", null, 'pStyle');
			}
		}
		
		
		$subsequent->addLine(['weight' => 1, 'width' => 500, 'height' => 0,'dash'=> \PhpOffice\PhpWord\Style\Line::DASH_STYLE_LONG_DASH_DOT_DOT]);
		
		
		//$phpWord->addTableStyle('Fancy Table', array('borderSize' => 10, 'borderColor' => '000000', 'cellMargin' => 8));
		$phpWord->addTableStyle('NoborderTable', array('cellMargin' => 8));
		$tableA = $section->addTable('NoborderTable');
		
		
		$tableA->addRow();
		$cellA = $tableA->addCell(11000,$cs2);
		
		$document_title = trans('report.label.document_title_default');
		if($item->document_title && $item->document_title !=""){
			$document_title = $item->document_title;
		} 
		
		$cellA->addText(trans('survey.label.created')." : ".\Carbon\Carbon::now()->format("d/m/Y")."  \t  ", $fontStyleh, $rightTabStyleName);
		$cellA->addTextBreak(2);
		\PhpOffice\PhpWord\Shared\Html::addHtml($cellA,'<p style="direction: rtl; color:#2b5ca7; text-decoration: underline;text-align:center; font-weight: bold; font-size: 40px; alignment: center;">'.$document_title.'</p>');
		$cellA->addTextBreak(12);
		
		$table = $cellA->addTable();
		$table->addRow();
        

		$cell = $table->addCell(8000,$cs2);
        $textrun = $cell->addTextRun($cellHCentered);
		if($item->owner_name && $item->owner_name !=""){
			$textrun->addText($item->owner_name,$style);
			$textrun->addTextBreak(1);
		}
		if($item->address && $item->address !=""){
			$textrun->addText($item->address,$style);
			$textrun->addTextBreak(1);
		}
		if($item->owner_email && $item->owner_email !=""){
			$textrun->addText($item->owner_email,$style);
		}
		if($item->owner_phone && $item->owner_phone !=""){
			$textrun->addText($item->owner_phone,$style);
			$textrun->addTextBreak(1);
		}
		$cell = $table->addCell(1000,$cs2);
        $textrun = $cell->addTextRun($cellHCentered);
		$textrun->addText(trans('survey.label.owner_name'), $style);
		
		$cell = $table->addCell(2000,$cs2);
        $textrun = $cell->addTextRun($cellHCentered);
		$textrun->addText('', $style);
		
		
		//*************************First page End************************//
		
		
		//*************************second page meta info ************************//
		$section = $phpWord->addSection();
		
		$section->addText(trans('survey.label.created')." : ".\Carbon\Carbon::now()->format("d/m/Y")."  \t ".trans('survey.label.owner_name')." : ".$item->owner_name, $fontStyleh, $rightTabStyleName);
		
		if($item->owner_email && $item->owner_email !=""){
			$section->addText("  \t מייל : ".$item->owner_email,$fontStyle,$rightTabStyleName);
		}
		if($item->owner_phone && $item->owner_phone !=""){
			$section->addText("  \t טלפון : ".$item->owner_phone,$fontStyle,$rightTabStyleName);
		}

		$section->addTextBreak();
		$section->addText("  \t ".trans('report.label.report_above_header'),$fontStyle,$rightTabStyleName);
		
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<p style="direction: rtl; color:#2b5ca7; text-decoration: underline;text-align:center; font-weight: bold; font-size: 25px; alignment: center;"><span>הנדון:</span> '.$document_title.' ב'.$item->address.'  <br/>'.trans('survey.label.professional_opinion').'</p>');
		

		if($item->enable_court){
			$court_date = ($item->court_date)? \Carbon\Carbon::parse($item->court_date)->format("d/m/Y") : "";
			
			$table = $section->addTable();
			$table->addRow();
			$cell = $table->addCell(3000,$cs4,$cellVCentered);
			$textrun = $cell->addTextRun($cellHEnd);
			$textrun->addText($court_date,$style_ul);
			
			$cell = $table->addCell(666,$cs4,$cellVCentered);
			$textrun = $cell->addTextRun($cellHEnd);
			$textrun->addText(trans('report.label.court_date').": ",$fontStyleh);
			
			$cell = $table->addCell(3000,$cs4,$cellVCentered);
			$textrun = $cell->addTextRun($cellHEnd);
			$textrun->addText($item->case_number,$style_ul);
			
			$cell = $table->addCell(666,$cs4,$cellVCentered);
			$textrun = $cell->addTextRun($cellHEnd);
			$textrun->addText(trans('report.label.case_number').": ",$fontStyleh);
			
			$cell = $table->addCell(2500,$cs4,$cellVCentered);
			$textrun = $cell->addTextRun($cellHEnd);
			$textrun->addText($item->name_of_court,$style_ul);
			
			$cell = $table->addCell(1166,$cs4,$cellVCentered);
			$textrun = $cell->addTextRun($cellHEnd);
			$textrun->addText(trans('report.label.name_of_court').": ",$fontStyleh);
		}
		$section->addTextBreak(2);
		
		if($item->surveyor){
			
		
			$phpWord->addTableStyle('surveyerDetail',array('cellMargin' => 8));
			$table = $section->addTable('surveyerDetail');
			$table->addRow();
			

			$cell = $table->addCell(9000,$cs2);
			$textrun = $cell->addTextRun($cellHCentered);
			if($item->surveyor->full_name && $item->surveyor->full_name !=""){
				$textrun->addText($item->surveyor->full_name,$style);
				$textrun->addTextBreak(1);
			}
			if($item->surveyor->address && $item->surveyor->address !=""){
				$textrun->addText($item->surveyor->address,$style);
				$textrun->addTextBreak(1);
			}
			if($item->surveyor->email && $item->surveyor->email !=""){
				$textrun->addText($item->surveyor->email,$style);
			}
			
			$cell = $table->addCell(2000,$cs2);
			$textrun = $cell->addTextRun($cellHCentered);
			$textrun->addText(trans('survey.label.surveyor_name'), $style);
			$section->addTextBreak(2);
		}
		
		//*************************second page meta info end************************//
		
		//*************************second page 9 line info ************************//
        $table = $section->addTable();
        
		$table->addRow();
        $cell = $table->addCell(11000,$cs4,$cellVCentered);
        $textrun = $cell->addTextRun($cellHEnd);
		
		$survey_date = \Carbon\Carbon::parse($item->survey_date)->format("d/m/Y");
		if(isset($item) && $item->parent_id && $item->parent_id >0 && $item->survey_date_option){
			$survey_date = \Carbon\Carbon::parse($item->created_at)->format("d/m/Y");	
		}
		
		
        $textrun->addText(trans('report.declartaion.l1',["licence_no"=>$licence_no]),$style);
		$textrun->addTextBreak(1);
        $textrun->addText(trans('report.declartaion.l2',["name"=>$item->owner_name]),$style);
		$textrun->addTextBreak(1);
        $textrun->addText(trans('report.declartaion.l3',["date"=>$survey_date]),$style);
		$textrun->addTextBreak(1);
        $textrun->addText(trans('report.declartaion.l4'),$style);
		$textrun->addTextBreak(1);
        $textrun->addText(trans('report.declartaion.l5'),$style);
		$textrun->addTextBreak(1);
        $textrun->addText(trans('report.declartaion.l6'),$style);
		
		
		
 

		$table->addRow();
        $cell = $table->addCell(6000,$cs2);
        $textrun = $cell->addTextRun($cellHCentered);
        
		foreach($qualification as $key=> $qual){
			if($qual->desc && $qual->desc !=""){
				$textrun->addText("&#9679; ".$qual->desc, $style);
				if(isset($qualification[$key+1])){
					$textrun->addTextBreak(1);
				}
			}
		}
		
		
		$cell = $table->addCell(5000,$cs2);
        $textrun = $cell->addTextRun($cellHEnd);
        $textrun->addText(trans('report.declartaion.l6_a'), $style);
		
		$table->addRow();
        $cell = $table->addCell(6000,$cs2);
        $textrun = $cell->addTextRun($cellHCentered);
        
		foreach($experience as $key=> $exp){
			if($exp->desc && $exp->desc !=""){
				$textrun->addText("&#9679; ".$exp->desc, $style);
				if(isset($experience[$key+1])){
					$textrun->addTextBreak(1);
				}
			}
		}
	
        $cell = $table->addCell(5000,$cs2);
        $textrun = $cell->addTextRun($cellHEnd);
        $textrun->addText(trans('report.declartaion.l6_b'), $style);
		
		
		$table->addRow();
        $cell = $table->addCell(11000,$cs4,$cellVCentered);
        $textrun = $cell->addTextRun($cellHEnd);
		
        $textrun->addText(trans('report.declartaion.l7'),$style);
		$textrun->addTextBreak(1);
        $textrun->addText("    -    ".$item->getPerposeOfVisit()."",$style);
		
		$section = $phpWord->addSection();
		$table = $section->addTable();
		
		$table->addRow();
        $cell = $table->addCell(11000,$cs4,$cellVCentered);
        $textrun = $cell->addTextRun($cellHEnd);
		$textrun->addText(trans('report.declartaion.l8'),$style);
		
		$selected_q = [];
		if(1){
			foreach($item->getaccessories() as $val){
				if($val->desc && $val->desc != ""){
					$selected_q[] = $val->desc;
				}
			}
		}
			
		foreach($selected_q as $b => $q){
			if($b%2 == 0){
				$table->addRow();
        
				$cell = $table->addCell(5500,$cs2,$cellVCentered);
				$textrun = $cell->addTextRun($cellHCentered);
				$textrun->addText("		- ".$q, $style);
				
				$second = (isset($selected_q[$b+1]))? $selected_q[$b+1] : "";
				$cell = $table->addCell(5500,$cs2);
				$textrun = $cell->addTextRun($cellHEnd);
				$textrun->addText("		- ".$second, $style);	
			}
		}
		
		$table->addRow();
        $cell = $table->addCell(11000,$cs4,$cellVCentered);
        $textrun = $cell->addTextRun($cellHEnd);
		$textrun->addText("\t ".trans('report.declartaion.l9'),$fontStyle,$rightTabStyleName);
		
		
		$selected_pm = [];
		if(1){
			foreach($item->getpfmaterials() as $val){
				if($val->desc && $val->desc != ""){
					$selected_pm[] = $val->desc;
				}
			}
		}
		
		if($selected_pm && count($selected_pm)){
			$styleTable = array('borderSize' => 10, 'borderColor' => '000000', 'cellMargin' => 8);
			$phpWord->addTableStyle('Fancy Table', $styleTable);
			$table = $section->addTable('Fancy Table');
			foreach($selected_pm as $b => $q){
				if($b%2 == 0){
					$table->addRow();
			
					$cell = $table->addCell(5500,$cs2,$cellVCentered);
					$textrun = $cell->addTextRun($cellHCentered);
					$textrun->addText("		- ".$q, $style);
					
					$second = (isset($selected_pm[$b+1]))? $selected_pm[$b+1] : "";
					$cell = $table->addCell(5500,$cs2);
					$textrun = $cell->addTextRun($cellHEnd);
					$textrun->addText("		- ".$second, $style);	
				}
			}
		}else{
		
			$rtexts = Responcetext::where("slug","other_info_image_1")->get();
			foreach($rtexts as $rd){
				$rf = $rd->refefile;
				if($rf && $rf->file_path != ""){
					$section->addTextBreak($lb);
					$section->addImage($rf->file_path, array('width' => 450,  'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
				}
			}
		}
		//*************************second page 9 line info end************************//
		
		//***************Signature image*****************//
		$section->addTextBreak();
		$section->addText("  \t "."הריני מצהיר בזאת כי אין כל עניין אישי בנכס הנידון.",$fontStyle,$rightTabStyleName);
		if($sign != ""){
			$section->addImage($sign, array('height' => 100));
		}
		//***************Signature image end*****************//

		
		//*************************Survey general data ************************//
		
		
		$section = $phpWord->addSection();
		
		$table = $section->addTable();
        $table->addRow();
		
		$cell = $table->addCell(11000,$cs2,$cellVCentered);
        $textrun = $cell->addTextRun($cellHCentered);
        $textrun->addText(trans('survey.label.general_info'), $fontStyleh);
		$textrun->addTextBreak(1);
		
		
		$g_content = [];
		$ginfos = str_replace(trans('survey.label.general_info_text2'),"",$item->general_info);
		$ginfos = explode("<p>",$ginfos);
		foreach($ginfos as $n => $ginfo){
			if($n == 0){
				$g_content[] = ['type'=>'text','val'=>$ginfo];
			}else{
				$ginfos_s = (explode("</p>",$ginfo));
				
				if(isset($ginfos_s[0])){
					$g_content[] = ['type'=>'p','val'=>$ginfos_s[0]];
				}
				if(isset($ginfos_s[1])){
					$g_content[] = ['type'=>'text','val'=>$ginfos_s[1]];
				}
			}
		}
		foreach($g_content as $n => $gd){
			if($gd['type'] == "text"){
				$table->addRow();
				$cell = $table->addCell(11000,$cs2,[]);
				$textrun = $cell->addTextRun($cellHCentered);
				\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<pre style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br($gd['val'])).'</pre>');
			}else{
				$table->addRow();
				$cell = $table->addCell(10400,[],[]);
				$textrun = $cell->addTextRun($cellHCentered);
				\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<pre style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br($gd['val'])).'</pre>');
				
				
				$cell = $table->addCell(600);
				$textrun = $cell->addTextRun($cell_r);
				$textrun->addText(" ",$style_s);
			}
			
		}
		if(count($g_content) && $item->is_consultants ){
			$table->addRow();
			$cell = $table->addCell(10400,[],[]);
			$textrun = $cell->addTextRun($cellHCentered);
			\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<pre style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br(trans('survey.label.general_info_text2'))).'</pre>');


			$cell = $table->addCell(600);
			$textrun = $cell->addTextRun($cell_r);
			$textrun->addText(" ",$style_s);
		}
		
		$section = $section->addTextRun(array('alignment' => $align));
		
		
		
		
		
		if($item->property_desc && $item->property_desc != ""){
			$section->addTextBreak($lb);
			$section->addText(trans('survey.label.property_desc'),$fontStyleh);
			$section->addTextBreak();
			\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br($item->property_desc)).'</div>');
		}
		
		if($item->legal_thing_isa && $item->legal_thing_isa != ""){
			$section->addTextBreak($lb);
			$section->addText(trans('survey.label.legal_thing_isa'),$fontStyleh);
			$section->addTextBreak();
			\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br($item->legal_thing_isa)).'</div>');
		}
		
		if($item->other_info && $item->other_info != "" && $item->is_the_sale_law_1973 ){
		$section->addTextBreak($lb);
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br($item->other_info)).'</div>');
		}else if($item->other_info && $item->other_info != ""){
			$section->addTextBreak($lb);
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br(str_replace('על פי חוק מכר (דירות) על הקונה להודיע למוכר על אי התאמות: (חוק המכר, תשל"ג 1973)',"",$item->other_info))).'</div>');
		}
		
		if($item->is_the_sale_law_1973 ){
		
			if($item->getspinfoa()){
				$section->addTextBreak($lb);
				\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br($item->getspinfoa()->desc)).'</div>');
			}else{
				$section->addTextBreak($lb);
				$rtexts = Responcetext::where("slug","other_info_image_3")->get();
				foreach($rtexts as $rd){
					$rf = $rd->refefile;
					if($rf && $rf->file_path != ""){
						$section->addTextBreak($lb);
						$section->addImage($rf->file_path, array('width' => 450,  'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
					}
				}
			}
			
			if($item->getspinfob()){
				$section->addTextBreak($lb);
				\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br($item->getspinfob()->desc)).'</div>');
			}else{	
				$rtexts = Responcetext::where("slug","other_info_image_2")->get();
				foreach($rtexts as $rd){
					$rf = $rd->refefile;
					if($rf && $rf->file_path != ""){
						$section->addTextBreak($lb);
						$section->addImage($rf->file_path, array('width' => 450,  'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
					}
				}
			}
		
		}
		
		
		$section->addTextBreak(2);
		
		$section->addTextBreak($lb);
		$section->addText(trans('constant.slug.report_focus_note'),$fontStyleh);
		
		$rd = Responcetext::where("slug","report_focus_note")->first();
		if($item->legal_thing_a != ""){
			$section->addTextBreak();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<pre style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br($item->legal_thing_a)).'</pre>');
		}
		
		//*************************Survey general data end************************//


		//*************************Survey issue************************//
		if(!$free_text){
	
		$section = $phpWord->addSection();
		$section->addText("\t ".trans('report.label.findings'), $issuetitle, $leftTabStyleName);
		
		
		
		
		$cat_id = 0;
		$cat_cntr = 0;
		$cat_sub_cntr = 0;
		
		$grp_location = "";
		$total_cost = 0;
		$cat_cost = [];
		$sub_cat_cost = [];
		
		$placeob = [];
		$lastlocation = "";
		$lastplacekey = 0;
		
		$cat_group = array();
		foreach($item->issueplace as $k => $issue){

			if(isset($issue->getsubcategory) && isset($issue->category) ){
				//Specific category data
				if($_category_id == 0 || $_category_id == $issue->category->id){
					
				$_CATID = $issue->getsubcategory->id;

				if(!$issue->location || $issue->location == ""){ $issue->location = "Default"; }
				if($lastlocation != $issue->location && $k != 0){
					$lastplacekey = $lastplacekey + 1;
					$cat_group = [];
				}
				$lastlocation = $issue->location;
				
				if(!isset($cat_group[$issue->category_id]) && isset($issue->category)){
					$cat_group[$issue->category_id] = ['name'=>$issue->category->name,'items'=>[]];
				}
				
				if(isset($cat_group[$issue->category_id]['items'][$_CATID]) && isset($issue->getsubcategory)){
					$cat_group[$issue->category_id]['items'][$_CATID]['items'][] = $issue;
				}else if(isset($issue->getsubcategory)){
					$cat_group[$issue->category_id]['items'][$_CATID]['name'] = $issue->getsubcategory->name;
					$cat_group[$issue->category_id]['items'][$_CATID]['items'][] = $issue;
				}
				
				$placeob[$lastplacekey] = ['name'=>$issue->location,'items'=>$cat_group];
				}
			}
			
			
		}	
	//	echo "<pre>"; print_r($placeob);exit;
		
		$_IS_TABLE = 0;
		
		
		
        $j = 9;
		$k = 0;
		$d = 0;
		$e = 0;
		foreach($placeob as  $pob){
			if($j != 9){
				$section = $phpWord->addSection();
				$_IS_TABLE = 0;
			}
			$j++;
			
			if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
			$table->addRow();
			$cell = $table->addCell(10600,$cs3,$cell_vc);
			$textrun = $cell->addTextRun($cell_r);
			$textrun->addText($pob['name'],$style_hp_ul);
			 
			$cell = $table->addCell(400);
			$textrun = $cell->addTextRun($cell_r);
			$textrun->addText($j.". ",$style_h);
			
			
		$k = 0;	
		foreach($pob['items'] as  $grp){
			$k++;
			if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
			$table->addRow();
			$cell = $table->addCell(10600,$cs3,$cell_vc);
			$textrun = $cell->addTextRun($cell_r);
			$textrun->addText($grp['name'],$style_hp);
			 
			$cell = $table->addCell(400);
			$textrun = $cell->addTextRun($cell_r);
			$textrun->addText($j.".".$k.". ",$style_h);
			
			$d=0;
			foreach($grp['items'] as  $gr){
				$d++;
				
				
				$e = 0;
				$total_issue_c = count($gr['items']);
				foreach($gr['items'] as  $issue){
					$_CATID = $issue->getsubcategory->id;
					$e++;
					
					$otherJson = json_decode($issue->other_json_data,true);
					
					$unit_cost_qty = "";
					if(isset($otherJson['flag_show_unit_price_in_report'])){
						$unit_cost_qty = "( ".$issue->unit_cost."X".$issue->number_of_unit.")";
					}
					
					if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
					
					
					if($e == 1 && $issue->issue_detail && $issue->issue_detail != ""){
						if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
						
						$table->addRow();
						$cell = $table->addCell(9600,$cs3,$cell_vc);
						$textrun = $cell->addTextRun($cell_r);
						\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<div style="direction: rtl;float:right;font-size:15px;font-weight: bold;">'.preg_replace('/\t+/', '     ',nl2br($issue->issue_detail)).'</div>');
						
						$issue_detail_3 = "";
						$ms = explode(' ',$issue->issue_detail);
						foreach($ms as $z=> $word){
							if($z <= 2){
								$issue_detail_3 = $issue_detail_3." ".$word;
							}
						}
						//$textrun->addField('XE', array(), array('Bold'),$k.".".$d.". ".$issue_detail_3.' ( ש\"ח '.$issue->total_cost. ') ',$style_h);
						//$textrun->addField('XE', array(), array('Bold'),$k.".".$d.". ",$style_h);
						
						
						$cell = $table->addCell(400);
						$textrun = $cell->addTextRun($cell_r);
						$textrun->addText($j.".".$k.".".$d.". ",$style_h);
						
						$cell = $table->addCell(1000);
						$textrun = $cell->addTextRun($cell_r);
						$textrun->addText("",$style_h);
					}
				
					
					$used_img = [];
					if($issue->refefile->count()){
						
						foreach($issue->refefile as $rf){
							if($rf->file_path && $rf->file_path != "" && !in_array($rf->refe_file_real_name,$used_img )){  // && !in_array($rf->refe_file_real_name,$used_img)
								//$section->addTextBreak();
								$section->addImage($rf->file_path, array('width' => 300,  'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
								$_IS_TABLE = 0;
							}
							
							$used_img[] = $rf->refe_file_real_name;
						}
						
					}
					
					if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
					
					
					if($issue->note && $issue->note != ""){
						if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
						
						$table->addRow();
						$cell = $table->addCell(10000,$cs3,$cell_vc);
						$textrun = $cell->addTextRun($cell_r);
					
						$textrun->addTextBreak(1);
					\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<div style="direction: rtl;font-size:15px;">'.preg_replace('/\t+/', '     ',nl2br($issue->note)).'</div>');
					
						$cell = $table->addCell(1000);
						$textrun = $cell->addTextRun($cell_r);
						$textrun->addText("",$style_s);
						
					}
					
					if($issue->total_cost > 0 && !$hidprice){	
						if($issue->costImage && $issue->show_default_price_detail){
							$section->addImage($issue->costImage->file_path, array('width' => 300,  'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
							$_IS_TABLE = 0;
						}
						
						if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
						
						$table->addRow();
						$cell = $table->addCell(9600,$cs2,$cell_vc);
						$textrun = $cell->addTextRun($cell_r);
						

						if($issue->costImage){
							
						}else if($issue->cost_detail && $issue->cost_detail != "" && $issue->show_default_price_detail){
							$textrun->addTextBreak(1);
							\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<div style="direction: rtl;font-size:15px;">'.preg_replace('/\t+/', '     ',nl2br($issue->cost_detail)).'</div>');
						}
						$textrun->addText(" : ".trans('report.label.cost')." ".$issue->total_cost. ' ש"ח '.$unit_cost_qty,$style_h);
						
						$cell = $table->addCell(400);
						$textrun = $cell->addTextRun($cell_r);
						$textrun->addText($j.".".$k.".".$d.".",$style_h);
						
						$cell = $table->addCell(1000);
						$textrun = $cell->addTextRun($cell_r);
						$textrun->addText("",$style_s);
						
					}
					$total_cost = $total_cost + $issue->total_cost;
					
					if(isset($cat_cost[$issue->category_id]) && isset($cat_cost[$issue->category_id]['cost'])){
						$cat_cost[$issue->category_id]['cost'] = $cat_cost[$issue->category_id]['cost'] + $issue->total_cost;
					}else{
						$cat_cost[$issue->category_id] = ['cost'=>$issue->total_cost,'name'=>$issue->category->name];
					}
					
					if(isset($sub_cat_cost[$_CATID]) && isset($sub_cat_cost[$_CATID]['cost'])){
						$sub_cat_cost[$_CATID]['cost'] = $sub_cat_cost[$_CATID]['cost'] + $issue->total_cost;
					}else{
						$sub_cat_cost[$_CATID] = ['cost'=>$issue->total_cost,'name'=>$issue->getsubcategory->name];
					}
					
					
					
					if($total_issue_c == $e){
						
						if($issue->quote && $issue->quote != ""){
							
							if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
							
							/*
							$table->addRow();
							$cell = $table->addCell(11000,$cs2,$cell_vc);
							$textrun = $cell->addTextRun($cell_r);
							$textrun->addText("\t "."ציטוט:",$style_h); 
						*/
							$table->addRow();
							$cell = $table->addCell(9600,$cs2,$cell_vc);
							$textrun = $cell->addTextRun($cell_r);
							$textrun->addText($issue->quote,$style_s);
							
							$cell = $table->addCell(1400);
							$textrun = $cell->addTextRun($cell_r);
							$textrun->addText("",$style_s);
						}
												
						if($issue->getsubcategory){
							$pluck = $issue->getsubcategory->refefile->pluck("id");
							if($issue->img_hint && $issue->img_hint != ""){
								
								$img_quote = [];
				
								if($issue->getsubcategory->quote_desc && $issue->getsubcategory->quote_desc != ""){
									$img_quote = json_decode($issue->getsubcategory->quote_desc,true);
								}
													
								//$img_hint = json_decode($issue->img_hint,true);
								$img_hint = $issue->hintImages();
								$imgc = 0;
								foreach($img_hint as $z => $hint){
									if(isset($hint['img_id']) && in_array($hint['img_id'],$pluck->toArray())){
										$rf = Refefile::whereId($hint['img_id'])->first();
										if($rf && $rf->file_path && $rf->file_path != ""){
											if($imgc ==0){
												
											}
											
											
											//$section->addTextBreak(1);
											$section->addImage($rf->file_path, array('width' => 300, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
											$imgc++;
											
											$_IS_TABLE = 0;
											
											if(isset($img_quote[$rf->id]) && $img_quote[$rf->id] != ""){
												if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
												
												$table->addRow();
												$cell = $table->addCell(10000,$cs3,$cell_vc);
												$textrun = $cell->addTextRun($cell_r);
												$textrun->addText("\t ".$img_quote[$rf->id],$fontStyleh, $rightTabStyleName);
						
												$cell = $table->addCell(1000);
												$textrun = $cell->addTextRun($cell_r);
												$textrun->addText("",$style_s);
																		
											}
										}
									}
								}
								
							}
							
						
						}
					
					
					//	$section = $section->addTextRun(array('alignment' => $align));
						if(!$_IS_TABLE){ $table = $section->addTable(); $_IS_TABLE = 1; }
						
						if($issue->recommendation && $issue->recommendation != ""){
							$table->addRow();
							$cell = $table->addCell(10000,$cs3,$cell_vc);
							$textrun = $cell->addTextRun($cell_r);
						
							$textrun->addTextBreak(1);
						$textrun->addText("\t ".trans('issue.label.recommendation'),$fontStyleh, $rightTabStyleName);
						$textrun->addTextBreak(1);
						
					\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<div style="direction: rtl;font-size:15px">'.preg_replace('/\t+/', '     ',nl2br($issue->recommendation)).'</div>');
					
							$cell = $table->addCell(1000);
							$textrun = $cell->addTextRun($cell_r);
							$textrun->addText("",$style_s);
						
						}
						if($sub_cat_cost[$_CATID]['cost'] && !$hidprice){
							$table->addRow();
							$cell = $table->addCell(9600,$cs2,$cell_vc);
							$textrun = $cell->addTextRun($cell_r);
							$textrun->addText("\t ".trans('issue.label.total_cost')." : ".$sub_cat_cost[$_CATID]['cost']. ' ש"ח',$style_h,$rightTabStyleName);
							$textrun->addTextBreak(1);
							 
							$cell = $table->addCell(400);
							$textrun = $cell->addTextRun($cell_r);
							$textrun->addText($j.".".$k.".".$d.". ",$style_h);
							
							$cell = $table->addCell(1000);
							$textrun = $cell->addTextRun($cell_r);
							$textrun->addText("",$style_h);
						}
						
					}
					
				}
				
				
			}
		
		}		
			
		}	
	
		}
		//*************************Survey issue end************************//

		//*************************Survey issue total table************************//
		
		$supervision_charge = 10;
		$gdp_charge = 15;
		$vat_charge = 17;
		
		$gdp_per = round($gdp_charge*$total_cost/100);
		$supervision_per = round($supervision_charge*$total_cost/100);
		
		$sub_total = $total_cost + $gdp_per + $supervision_per;
		$vat_per = round($vat_charge*$sub_total/100);
		$net_toal = $vat_per + $sub_total;
		
		$cat_tr = "";
		$index = 1;
		foreach($cat_cost as $k=> $cs){
			if($cs['cost'] >0){
				if(!$hidprice){
					$_price = $cs['cost'].' ש"ח';
				}else{
					$_price = "";
				}
			$cat_tr.= '<tr><td style="text-align: center; "> '.$_price.'</td><td style="text-align: center; "> '.$cs['name'].'</td><td style="text-align: center; ">'.$index.'</td></tr>';
			$index++;
			}
		}
		
		if(!$hidprice){
			$_gdp_per = $gdp_per.' ש"ח';
			$_supervision_per = $supervision_per.' ש"ח';
			$_total_cost = $total_cost.' ש"ח';
			$_sub_total = $sub_total.' ש"ח';
			$_vat_per = $vat_per.' ש"ח';
			$_net_toal = number_format(round($net_toal)).' ש"ח';
		}else{
			$_gdp_per = "";
			$_supervision_per = "";
			$_total_cost = "";
			$_sub_total = "";
			$_vat_per = "";
			$_net_toal = "";
		}
		
		$table = '<table style="width: 100%; border: 6px #000000 solid;">'.
            '<thead>'.
                '<tr style="text-align: center; font-weight: bold; ">'.
                    '<th style="text-align: center;margin-top: 5px;  ">'.trans('survey.label.price').'</th>'.
                    '<th style="text-align: center; margin-top: 5px;">'.trans('survey.label.section').'</th>'.
                    '<th style="text-align: center;margin-top: 5px; ">'.trans('survey.label.tax').'</th>'.
                '</tr>'.
            '</thead>'.
            '<tbody>'.$cat_tr.
                '<tr><td style="text-align: center; "> '.$_total_cost.'</td><td style="text-align: center; "> '.trans('survey.label.all_problem_price').'</td><td ></td></tr>'.
                '<tr><td style="text-align: center; "> '.$_gdp_per.'</td><td style="text-align: center; "> '.trans('survey.label.gdp')." ".$gdp_charge."%".'</td><td></td></tr>'.
                '<tr><td style="text-align: center; "> '.$_supervision_per.'</td><td style="text-align: center; "> '.trans('survey.label.supervision')." ".$supervision_charge."%".'</td><td></td></tr>'.
                '<tr><td style="text-align: center; ">'.$_sub_total.'</td><td style="text-align: center; ">'.trans('survey.label.total').'</td><td></td></tr>'.
                '<tr><td style="text-align: center; ">'.$_vat_per.'</td><td style="text-align: center; ">'.trans('survey.label.vat')." ".$vat_charge."%".'</td><td></td></tr>'.
                '<tr><th style="text-align: center; font-weight: bold; ">'.$_net_toal.'</th><th style="text-align: center; font-weight: bold; ">'.trans('survey.label.total_payment_including_VAT').'</th><td></td></tr>'.
            '</tbody>'.
         '</table>';;
		 
		$section = $phpWord->addSection();
		
		
		$section->addText("\t ".trans('survey.label.estimated_repair_costs'), $issuetitle, $leftTabStyleName);
		
		if(!$hidprice){
			\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;font-size:15px;">'.$table.'</div>');
		}
		
		$section->addTextBreak(1);
		
		//*************************Survey issue total table end************************//
		
		$table = $section->addTable();
		
		$table->addRow();
		$cell = $table->addCell(11000,$cs2,$cellVCentered);
		$textrun = $cell->addTextRun($cellHEnd);
		$textrun->addText("\t ".trans('survey.label.reviews_and_summary'),$fontStyleh, $rightTabStyleName);
		$textrun->addTextBreak(1);
		\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<div style="direction: rtl;float:right;text-align:right;font-size:15px;">'.preg_replace('/\t+/', '     ',nl2br($item->reviews_and_summary)).'</div>');
		
	//	$section = $section->addTextRun(array('alignment' => $align));
		
		
		
		if($item->checklist && $item->checklist != ""){
			$checklist = json_decode($item->checklist,true);
			if($checklist && count($checklist) > 0){
				foreach($checklist as $chl){
					$textrun->addTextBreak(2);
					$textrun->addText("\t".$chl['title'],$fontStyleh,$rightTabStyleName);
					$textrun->addTextBreak();
					\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<div style="direction: rtl;text-align:right;font-size:15px;">'.preg_replace('/\t+/', '     ',nl2br($chl['desc'])).'</div>');
				}
			}
		}
		
		if($item->not_tested && $item->not_tested!=""){
			$textrun->addTextBreak(3);
			$textrun->addText("\t ".trans('survey.label.not_tested'),$fontStyleh, $rightTabStyleName);
			$textrun->addTextBreak(1);
			\PhpOffice\PhpWord\Shared\Html::addHtml($textrun,'<div style="direction: rtl;text-align:right;font-size:15px;">'.preg_replace('/\t+/', '     ',nl2br($item->not_tested)).'</div>');
			
		}
		$textrun->addTextBreak(3);
		
		//***************Signature image*****************//
		$textrun->addText("הריני מצהיר בזאת כי אין כל עניין אישי בנכס הנידון.",$fontStyleh, $rightTabStyleName);
		if($sign != ""){
			$section->addImage($sign, array('height' => 100));
		}
		//***************Signature image end*****************//
			
	/*	$section = $phpWord->addSection();
		//$section = $section->addTextRun(array('alignment' => $align));
		$section->addText("\t ".trans('report.label.index'), $issuetitle, $leftTabStyleName);
		$section->addField('INDEX', array(), array('\\e "	"'), 'right click to update the index');
	*/	
		
		/*$section->addTextBreak(1);
		
		$section->addText("\t ".trans('survey.label.not_tested'),$fontStyleh,$rightTabStyleName);
		$section->addTextBreak();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;float:right">'.nl2br($item->not_tested).'</div>');
		
		*/
		
		


		



		
		
		//$file_name = "survey_report_place_".str_slug($item->owner_name)."_".$item->id;
		$file_name = "survey_report_place";
		
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		
		\File::exists(public_path('uploads/survey/')) or mkdir(public_path('uploads/survey/'), 0777, true);
		$loc = public_path('uploads/survey/'.$file_name.'.docx');
		
		if (\File::exists($loc)) {
            unlink($loc);
        }
		
		$objWriter->save($loc);
		
		return response()->download($loc);
	}

}
