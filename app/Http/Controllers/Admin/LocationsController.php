<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Responcetext;
use App\Setting;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class LocationsController extends Controller
{
    
    public function index(Request $request)
    {
        return view('admin.location-text.index');
    }
    public function datatable(Request $request) {
        $record = Responcetext::where("responce_type","=","location");
        return Datatables::of($record)->make(true);
    }

    
    public function create()
    {
		return view('admin.location-text.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'responce_type' => 'required',
            'desc' => 'required',
        ],[],trans('constant.label'));


        $requestData = $request->all();
       
        $module = Responcetext::create($requestData);
		
        if($module){
			Setting::updateOrCreate(['key'=>'last_update'],['key'=>'last_update','value'=>\Carbon\Carbon::now()]);
            $result['message'] = trans('common.responce_msg.record_created_succes');
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/locations');
        }

    }
	public function show($id,Request $request)
    {
		$item = Responcetext::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/locations');
		}
		return view('admin.location-text.show',compact('item'));
	}
	
    public function edit($id,Request $request)
    {
        $result = array();
		$item = Responcetext::where("id",$id)->first();
        
        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
			return view('admin.location-text.edit', compact('item'));
        }
		

    }

    public function update($id, Request $request)
    {
        $result = array();

        $this->validate($request, [
            'responce_type' => 'required',
            'desc' => 'required',
        ],[],trans('constant.label'));

        $item = Responcetext::where("id",$id)->first();
        $requestData = $request->all();
        
        if($item){
            $item->update($requestData);
			Setting::updateOrCreate(['key'=>'last_update'],['key'=>'last_update','value'=>\Carbon\Carbon::now()]);
            $result['message'] = trans('common.responce_msg.record_updated_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/locations');
        }
        
    }
    

    public function destroy($id,Request $request)
    {
        $item = Responcetext::where("id",$id)->first();

        $result = array();

        if($item){
            $item->delete();
            $result['message'] = trans('common.responce_msg.record_deleted_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			return redirect('admin/locations');
        }
    }
	
	public function search(Request $request)
    {
        $result = array();

        $data =Responcetext::select(['id','desc'])->where("responce_type","=","location");

        if ($request->has('search') &&  $request->get('search') != '') {
            $data->where('desc', 'LIKE', "%$request->search%");
        }
		
		
		
		$res = $data->get()->toArray();
		$res[] = ["id"=>"","desc"=>$request->search];
		if ($request->has('survey_id') &&  $request->get('survey_id') != '') {
            $item = \App\Survey::where("id",$request->survey_id)->first();
			if($item){
				$last_loc = $item->getLastLocation();
				if($last_loc != "" && $last_loc){
					array_unshift($res,["id"=>"","desc"=>$last_loc]);
				}
			}
        }
		
		
        $result['data'] = $res;
        $result['code'] = 200;

        return response()->json($result, $result['code']);
    }


}
