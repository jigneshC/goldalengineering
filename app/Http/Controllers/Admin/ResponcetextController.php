<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Responcetext;
use App\Setting;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class ResponcetextController extends Controller
{
    
    public function index(Request $request)
    {
        return view('admin.responce-text.index');
    }
    public function datatable(Request $request) {
        $record = Responcetext::where("responce_type","=","constant");
        return Datatables::of($record)->make(true);
    }

    
    public function create()
    {
		$editable_slug = trans('constant.slug');
		return view('admin.responce-text.create',compact('editable_slug'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addTitle(Request $request){
	
        $result = array();
		$rules = [
            'document_title_a' => 'required|max:100',
			'responce_type' => 'required',
        ];

		$validator = \Validator::make($request->all(), $rules, [],[]);

        if ($validator->fails())
        {
            $validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json(['message' =>$messages,'success' => false,'status' => 400],400);
        }
		
        $input = [];
		$input['title'] = $request->document_title_a;
		$input['responce_type'] = $request->responce_type;
		$item =\App\Responcetext::create($input);
        
		$result['message'] = \Lang::get('common.responce_msg.item_created_success',['item'=>"Title"]);
		$result['data'] = $item;
        $result['code'] = 200;
			
        return response()->json($result, $result['code']);

    }
    public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'responce_type' => 'required',
            'slug' => 'required',
			'title' => 'required',
        ],[],trans('constant.label'));


        $requestData = $request->all();
       
        $module = Responcetext::updateOrCreate(['slug'=>$request->slug],$requestData);
		
        if($module){
			Setting::updateOrCreate(['key'=>'last_update'],['key'=>'last_update','value'=>\Carbon\Carbon::now()]);
            $result['message'] = trans('common.responce_msg.record_created_succes');
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/constants');
        }

    }
	public function show($id,Request $request)
    {
		$item = Responcetext::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/constants');
		}
		return view('admin.responce-text.show',compact('item'));
	}
	
    public function edit($id,Request $request)
    {
        $result = array();
		if ($request->has('type') && $request->get('type') == 'slug') {
            $item = Responcetext::where("slug",$id)->first();
        }else{
			$item = Responcetext::where("id",$id)->first();
		}
        
		
		
		

        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }
        
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
			
			$slugs = trans('constant.slug');
			$editable_slug = [];
			
			foreach($slugs as $k=>$slug){
				if($item && $item->slug == $k){
					$editable_slug[$k] = $slug;
				}
			}
		
			return view('admin.responce-text.edit', compact('item','editable_slug'));
        }
		

    }

    public function update($id, Request $request)
    {
        $result = array();

        $this->validate($request, [
            'responce_type' => 'required',
            'slug' => 'required',
			'title' => 'required'
		],[],trans('constant.label'));
		
		$item = Responcetext::where("id",$id)->first();
        $requestData = $request->except("desc_list","refe_types","customise_option");
		$refe_types = $request->refe_types;
        
        if($item){
            $item->update($requestData);
			foreach($item->textChecklist as  $list){
				$list->delete();
			}
			
			if($request->has('desc_list')){
				foreach($request->desc_list as $k=> $list){
					if($list && $list != ""){
						$refe_type = (isset($refe_types[$k]))? $refe_types[$k] : 0;
						\App\RefeTextCheckbox::create(["refe_type"=>$refe_type,"desc"=>$list,"priority"=>$k,"refe_field_id"=>$item->id,"refe_table_field_name"=>$item->getTable()]);
					}
				}
			}
			Setting::updateOrCreate(['key'=>'last_update'],['key'=>'last_update','value'=>\Carbon\Carbon::now()]);
            $result['message'] = trans('common.responce_msg.record_updated_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/constants');
        }
        
    }
    

    public function destroy($id,Request $request)
    {
        $item = Responcetext::where("id",$id)->first();

        $result = array();

        if($item){
            $item->delete();
            $result['message'] = trans('common.responce_msg.record_deleted_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			return redirect('admin/constants');
        }
    }


}
