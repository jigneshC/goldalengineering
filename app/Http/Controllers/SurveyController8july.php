<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Survey;
use App\Branches;
use App\Issue;
use App\Responcetext;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;
use App\User;
use App\Refefile;

class SurveyController extends Controller
{
    
    public function index(Request $request)
    {
		$surveyor = null;
		if ($request->has('surveyor_id') && $request->get('surveyor_id') != '' && $request->get('surveyor_id') != 0) {
            $surveyor = User::where("id",$request->surveyor_id)->first();
        }
		
        return view('admin.survey.index',compact('surveyor'));
    }
    public function datatable(Request $request) {
        $record = Survey::where("id",">",0);
		
		if ($request->has('status') && $request->get('status') != 'all' && $request->get('status') != '' ) {
            $record->where('status',$request->get('status'));
        }
		if ($request->has('surveyor_id') && $request->get('surveyor_id') != '' && $request->get('surveyor_id') != 0) {
            $record->where('surveyor_id',$request->get('surveyor_id'));
        }
        return Datatables::of($record)->make(true);
    }

    public function show($id,Request $request)
    {
		$item = Survey::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
		}
		return view('admin.survey.show',compact('item'));
	}
	public function detail($id,Request $request)
    {
		$item = Survey::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
		}
		return view('admin.survey.detail',compact('item'));
	}
	
    public function edit($id,Request $request)
    {
        $result = array();
        $item = Survey::findOrFail($id);
		
		$branches = Branches::pluck('name', 'id');

        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
			
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
        }
        
		if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            return view('admin.survey.edit', compact('item','branches'));
        }
		

    }

    public function update($id, Request $request)
    {
        $result = array();

        
		
		$varr = [
            'owner_name' => 'required|min:2|max:150',
            'owner_email' => 'required|min:2|max:150',
            'address' => 'required',
            'property_desc' => 'required',
            'legal_thing_a' => 'required',
            'legal_thing_isa' => 'required',
            'surveyor_note' => 'required',
            'reviews_and_summary' => 'required',
            'images.*' => 'mimes:jpeg,png,jpg,gif,svg|max:5000',
            'image' => 'mimes:jpeg,png,jpg,gif,svg|max:5000',
        ];

		$this->validate($request,$varr,[],trans('survey.label'));
		
        $item = Survey::where("id",$id)->first();
        $requestData = $request->except(['image','images','file','files']);
        
        if($item){
            $item->update($requestData);
			
			$item->issue_count = $item->issue->count();
			$item->save();
			
			if($request->hasFile('images'))
			{
				
				$files = $request->file('images');
				
				uploadModalReferenceFile($files,'uploads/survey/'.$item->id,'survey_id',$item->id,'logo_image',[]);
			}
			if($request->hasFile('image'))
			{
				$files = [$request->file('image')];
				uploadModalReferenceFile($files,'uploads/survey/'.$item->id,'survey_id',$item->id,'logo_image',[]);
			}
            $result['message'] = trans('common.responce_msg.record_updated_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect()->back();
        }
        
    }
    

    public function destroy($id,Request $request)
    {
        $item = Survey::where("id",$id)->first();

        $result = array();

        if($item){
			$issues = Issue::where("survey_id",$id)->get();
			foreach($issues as $issue){
				foreach($issue->refefile as $rf){
					removeRefeImage($rf);
				}
				$issue->forceDelete();
			}
			foreach($item->refefile as $rf){
				removeRefeImage($rf);
			}
            $item->forceDelete();
            $result['message'] = trans('common.responce_msg.record_deleted_succes');
            $result['code'] = 200;

        }else{
            $result['message'] = trans('common.responce_msg.something_went_wr');
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
			return redirect('admin/survey');
        }
    }
	public function export($id,Request $request)
    {
		$rd = Responcetext::where("slug","report_declaration")->first();
		
		\App::setLocale("he");
		
		$logo = public_path('assets/images/logo.png');
		
		$item = Survey::where("id",$id)->first();
		if(!$item){
			Session::flash('flash_error',trans('common.responce_msg.data_not_found'));
            return redirect('admin/survey');
		}
		
		$align = "left";
		$opalign = "right";
		
		$lb = 1;
		$fontStyleh = new \PhpOffice\PhpWord\Style\Font();
		if(\App::getLocale() == 'he'){
			$fontStyleh->setRTL(true);
			$fontStyleh->setLang('he-IL');
			$align = "right";
			$opalign = "left";
			$lb = 2;
		}else{
			$fontStyleh->setRTL(false);
			$fontStyleh->setLang('en');
			$align = "left";
			$opalign = "right";
			$lb = 1;
		}
        
		$fontStyleh->setBold(true);
		$fontStyleh->setName('Tahoma');
		$fontStyleh->setSize(10);
		
		$fontStyle = new \PhpOffice\PhpWord\Style\Font();
		if(\App::getLocale() == 'he'){
			$fontStyle->setRTL(true);
			$fontStyle->setLang('he-IL');
		}else{
			$fontStyle->setRTL(false);
			$fontStyle->setLang('en');
		}
        
		$fontStyle->setBold(false);
		$fontStyle->setName('Tahoma');
		$fontStyle->setSize(10);
		
		$bluetitle = new \PhpOffice\PhpWord\Style\Font();
		$bluetitle->setRTL(true);
		$bluetitle->setBold(true);
		$bluetitle->setName('Tahoma');
		$bluetitle->setSize(18);
		$bluetitle->setLineHeight(0.01);
		$bluetitle->setColor("00bfff");
	
		
		$issuetitle = new \PhpOffice\PhpWord\Style\Font();
		$issuetitle->setRTL(true);
		$issuetitle->setBold(false);
		$issuetitle->setName('Tahoma');
		$issuetitle->setSize(18);
		$issuetitle->setColor("000000");
		
	//	$bluetitle->setUnderline("single");
		
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		
				// Define styles
		$multipleTabsStyleName = 'multipleTab';
		$phpWord->addParagraphStyle(
			$multipleTabsStyleName,
			array(
				'tabs' => array(
					new \PhpOffice\PhpWord\Style\Tab('left', 1550),
					new \PhpOffice\PhpWord\Style\Tab('center', 3200),
					new \PhpOffice\PhpWord\Style\Tab('right', 5300),
				),
			)
		);
		$rightTabStyleName = 'rightTab';
		$phpWord->addParagraphStyle($rightTabStyleName, array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('right', 9090))));
		$leftTabStyleName = 'centerTab';
		$phpWord->addParagraphStyle($leftTabStyleName, array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('center', 4680))));

		$section = $phpWord->addSection($fontStyle);
		
		$subsequent = $section->addHeader();
		//$subsequent->addText("Subsequent pages in Section 1 will Have this!");
		$subsequent->addImage($logo, array('width' => 30, 'height' => 30,'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
		
		$subsequent->addText("\t Inspection and Engineering Services                  ביקורת מבנים ושירותי הנדסה", null, $leftTabStyleName);
		$subsequent->addText("\t 052-6517770      /      058-7517771", null, $leftTabStyleName);
		$subsequent->addText("\t sliorgl@gmail.com      /      salexgl@gmail.com", null, $leftTabStyleName);
	/*	$table = $subsequent->addTable(array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
		$table->addRow();
		$table->addCell(7000)->addText("Inspection and Engineering Services             ביקורת מבנים ושירותי הנדסה");
		$table->addRow();
		$table->addCell(700)->addText('                                    052-6517770      /      058-7517771');
		$table->addRow();
		$table->addCell(700)->addText('                              sliorgl@gmail.com      /      salexgl@gmail.com');
*/
		
		
		
		$section->addTextBreak();
		
		$section->addText(trans('survey.label.created')." : ".\Carbon\Carbon::now()->format("d/m/Y")."  \t ".trans('survey.label.owner_name')." : ".$item->owner_name, $fontStyleh, $rightTabStyleName);
		
		
		$section->addText("  \t מייל : ".$item->owner_email,$fontStyle,$rightTabStyleName);
		$section->addText("  \t טלפון : ".$item->owner_phone,$fontStyle,$rightTabStyleName);
		
		
		//$section->addTextBreak(1);
		
		//$section->addText(trans('survey.label.professional_opinion')." \n "."הנדון : ליקויים בניה ב  ".$item->address,$bluetitle, array('alignment' => 'center'));
		
		
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<p style="direction: rtl; color:#00bfff; text-decoration: underline;text-align:center; font-weight: bold; font-size: 25px; alignment: center;">הנדון : ליקויים בניה ב  '.$item->address.'<br/>'.trans('survey.label.professional_opinion').'</p>');
	//	
	
		//$section->addText("הנדון : ליקויים בניה ב  ".$item->address, $bluetitle, $leftTabStyleName);
		//$section->addText("\t ".trans('survey.label.professional_opinion'), $bluetitle, $leftTabStyleName);
		
	//	$section = $section->addTextRun(array('alignment' => $align));
		
		
		
		/*if($item->refefile->count()){
			$section->addText("\t ".trans('survey.label.images'),$fontStyleh, $rightTabStyleName);
			$section->addTextBreak();
			foreach($item->refefile as $rf){
				if($rf->file_path && $rf->file_path != ""){
					$section->addImage($rf->file_path, array('width' => 210, 'height' => 210, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
					break;
				}
			}
			
		}
		$section->addTextBreak($lb);*/
		
		
	/*	$table = $section->addTable();
		for ($r = 1; $r <= 8; $r++) {
			$table->addRow();
			for ($c = 1; $c <= 5; $c++) {
				$table->addCell(1750)->addText(htmlspecialchars("Row {$r}, Cell {$c}"));
			}
		}*/

		//$section = $section->addTextRun(array('alignment' => $align));
		
	
		
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<p style="direction: rtl;    alignment: right;">'.trans('report.declartaion.l1').'<br/>'.trans('report.declartaion.l2').'<br/>'.trans('report.declartaion.l3',['date'=>$item->created_exp]).'<br/>'.trans('report.declartaion.l4').'<br/>'.trans('report.declartaion.l5').'<br/>'.trans('report.declartaion.l6').'</p>');
	
	 
		$html  = '<table style="width: 100%;">'.
            '<tbody>'.
                '<tr><td  style="direction: rtl; width: 50%;"> '.trans('report.declartaion.l6_a_1').' </td><td>&#9679;</td><td  style="direction: rtl; width: 500px;">			'.trans('report.declartaion.l6_a').'</td></tr>'.
               '<tr><td  style="direction: rtl; width: 50%;"> '.trans('report.declartaion.l6_b_1').'</td><td>&#9679;</td><td  style="direction: rtl; width: 500px;">			'.trans('report.declartaion.l6_b').'</td></tr>'.
			   '<tr><td  style="direction: rtl; width: 50%;"> '.trans('report.declartaion.l6_b_2').'</td><td>&#9679;</td><td  style="direction: rtl; width: 500px;"></td></tr>'.
            '</tbody>'.
         '</table>';


		\PhpOffice\PhpWord\Shared\Html::addHtml($section, $html);

		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<p style="direction: rtl; alignment: right;">'.trans('report.declartaion.l7').'</p>');
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<p style="direction: rtl; alignment: right;">'.trans('report.declartaion.l8').'</p>');
		
		
		$html  = '<table style="width: 100%;">'.
            '<tbody>'.
                '<tr><td  style="direction: rtl; width: 60%;">					'.trans('report.declartaion.l8_r1_b').'</td><td>-</td><td  style="direction:rtl;  ">- '.trans('report.declartaion.l8_r1_a').'</td></tr>'.
               '<tr><td  style="direction: rtl; width: 60%;">						'.trans('report.declartaion.l8_r2_b').'</td><td>-</td><td  style="direction: rtl;">- '.trans('report.declartaion.l8_r2_a').'</td></tr>'.
			   '<tr><td  style="direction: rtl; width: 60%;">					'.trans('report.declartaion.l8_r3_b').'</td><td>-</td><td  style="direction: rtl;">- '.trans('report.declartaion.l8_r3_a').'</td></tr>'.
            '</tbody>'.
         '</table>';


		\PhpOffice\PhpWord\Shared\Html::addHtml($section, $html);
		
		
		if($rd){
			//\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl">'.$rd->desc.'</div>');
			//$section->addTextBreak(2);
		}
		
		/*
		$section->addTextBreak($lb);
		$section->addText(trans('survey.label.property_desc'),$fontStyleh);
		$section->addTextBreak();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl">'.$item->property_desc.'</div>');
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.legal_thing_a'),$fontStyleh);
		$section->addTextBreak();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl">'.$item->legal_thing_a.'</div>');
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.legal_thing_isa'),$fontStyleh);
		$section->addTextBreak();
		$Text_to_Add = htmlentities($item->legal_thing_isa);
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl">'.$item->legal_thing_isa.'</div>');
		
		$section->addTextBreak($lb);
		
		$section->addText(trans('survey.label.surveyor_note'),$fontStyleh);
		$section->addTextBreak();
		//$section->addText($item->surveyor_note,$fontStyle);
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl">'.$item->surveyor_note.'</div>');
		
		$section->addTextBreak($lb);
		
		*/
		
	//	$section->addText(trans('survey.label.general_info'),$fontStyleh);
	//	$section->addTextBreak();
	//	\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl">'.$item->general_info.'</div>');
		
	//	$section->addTextBreak($lb);
		
	/*	$section->addText(trans('survey.label.other_info'),$fontStyleh);
		$section->addTextBreak();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl">'.$item->other_info.'</div>');
		*/
	//	$section = $phpWord->addSection();
		//$section->addText("\t ".trans('issue.label.issue_detail'), $issuetitle, $leftTabStyleName);
		
		
		//$section = $section->addTextRun(array('alignment' => $align));
		
	/*	$cat_id = 0;
		$cat_cntr = 0;
		$cat_sub_cntr = 0;
		
		$grp_location = "";
		$total_cost = 0;
		
		foreach($item->issue as $k => $issue){
			$section->addTextBreak($lb);
			if($issue->category_id != $cat_id){
				$cat_sub_cntr = 1;
				$grp_location = "";
				$cat_cntr = $cat_cntr + 1;
				
				
				if($issue->subcategory){
					$section->addText("\t ".$cat_cntr." : ".$issue->subcategory->name,$fontStyleh, $rightTabStyleName);
				}else if($issue->category){
					$section->addText("\t ".$cat_cntr." : ".$issue->category->name,$fontStyleh, $rightTabStyleName);
				}
				
				$cat_id = $issue->category_id;
			}
			
			
			$section->addText("\t ".$cat_cntr.".".$cat_sub_cntr.". ".$issue->issue_detail,$fontStyleh, $rightTabStyleName);
			
			$section->addText("\t ".$cat_cntr.".".$cat_sub_cntr.".1 ".$issue->location,$fontStyle, $rightTabStyleName);
			
			
			$section->addText("\t ".trans('issue.label.recommendation'),$fontStyleh,$rightTabStyleName);
			$section->addText("\t ".$issue->recommendation,$fontStyle,array("align" => "right"));
		*/	
		//	\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;float:right">'.$issue->recommendation.'</div>');
			
		/*	
			if($issue->subcategory){
			
				if($issue->img_hint && $issue->img_hint != ""){
					
					$section->addText("\t ".trans('categories.label.category_images'),$fontStyleh,$rightTabStyleName);
					$section->addTextBreak();
				
					$img_hint = json_decode($issue->img_hint,true);
					foreach($img_hint as $z => $hint){
						$rf = Refefile::whereId($hint['img_id'])->first();
						if($rf->file_path && $rf->file_path != ""){
							$section->addImage($rf->file_path, array('width' => 210, 'height' => 210, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
							$section->addTextBreak();
						}
					}
				}
				
			}
			$section->addText("\t ".trans('issue.label.unit_cost')." : ".$issue->unit_cost,$fontStyleh,$rightTabStyleName);
			$section->addText("\t ".trans('issue.label.number_of_unit')." : ".$issue->number_of_unit,$fontStyleh,$rightTabStyleName);
			$section->addText("\t ".trans('issue.label.total_cost')." : ".$issue->total_cost,$fontStyleh,$rightTabStyleName);
			
			$total_cost = $total_cost + $issue->total_cost;
			
			$section->addTextBreak($lb);
			
			if($issue->refefile->count()){
				$section->addText("\t ".trans('issue.label.issue_images'),$fontStyleh,$rightTabStyleName);
				$section->addTextBreak();
				foreach($issue->refefile as $rf){
					if($rf->file_path && $rf->file_path != ""){
						$section->addImage($rf->file_path, array('width' => 210, 'height' => 210, 'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER));
						$section->addTextBreak();
					}
				}
				
			}
			
			$section->addTextBreak($lb);
			
			$section->addText("\t ".trans('issue.label.quote'),$fontStyleh,$rightTabStyleName);
			$section->addText($issue->quote,$fontStyle,array("align" => "right"));
			
			$section->addText("\t ".trans('issue.label.note'),$fontStyleh,$rightTabStyleName);
			$section->addText("\t ".$issue->note,$fontStyle,array("align" => "right"));
			
		
			
			$section->addTextBreak($lb);
			
			$cat_sub_cntr = $cat_sub_cntr +1 ;
			
		}
		
		$supervision_charge = 10;
		$gdp_charge = 15;
		$vat_charge = 17;
		
		$gdp_per = round($gdp_charge*$total_cost/100,2);
		$supervision_per = round($supervision_charge*$total_cost/100,2);
		
		$sub_total = $total_cost + $gdp_per + $supervision_per;
		$vat_per = round($vat_charge*$sub_total/100,2);
		$net_toal = $vat_per + $sub_total;
		
		$table = '<table style="width: 100%; border: 6px #0000FF solid;">'.
            '<thead>'.
                '<tr style="text-align: center; font-weight: bold; ">'.
                    '<th style="text-align: center; ">'.trans('survey.label.price').'</th>'.
                    '<th style="text-align: center; ">'.trans('survey.label.section').'</th>'.
                    '<th style="text-align: center; ">'.trans('survey.label.tax').'</th>'.
                '</tr>'.
            '</thead>'.
            '<tbody>'.
                '<tr><td style="text-align: center; ">'.$total_cost.'</td><td style="text-align: center; ">'.trans('survey.label.all_problem_price').'</td><td ></td></tr>'.
                '<tr><td style="text-align: center; ">'.$gdp_per.'</td><td style="text-align: center; ">'.trans('survey.label.gdp').$gdp_charge."% ".'</td><td></td></tr>'.
                '<tr><td style="text-align: center; ">'.$supervision_per.'</td><td style="text-align: center; ">'.trans('survey.label.supervision').$supervision_charge."% ".'</td><td></td></tr>'.
                '<tr><td style="text-align: center; ">'.$sub_total.'</td><td style="text-align: center; ">'.trans('survey.label.total').'</td><td></td></tr>'.
                '<tr><td style="text-align: center; ">'.$vat_per.'</td><td style="text-align: center; ">'.trans('survey.label.vat').$vat_charge."% ".'</td><td></td></tr>'.
                '<tr><th style="text-align: center; font-weight: bold; ">'.$net_toal.'</th><th style="text-align: center; font-weight: bold; ">'.trans('survey.label.total_payment_including_VAT').'</th><td></td></tr>'.
            '</tbody>'.
         '</table>';;
		 
		$section = $phpWord->addSection();
		
		
		$section->addText("\t ".trans('survey.label.estimated_repair_costs'), $issuetitle, $leftTabStyleName);
		
		
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl">'.$table.'</div>');
		
		$section->addTextBreak($lb);
		
		$section = $section->addTextRun(array('alignment' => $align));
		
		$section->addText("\t ".trans('survey.label.reviews_and_summary'),$fontStyleh,$rightTabStyleName);
		$section->addTextBreak();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;text-align:right;">'.$item->reviews_and_summary.'</div>');
		
		$section->addTextBreak($lb);
		
		$section->addText("\t ".trans('survey.label.not_tested'),$fontStyleh,$rightTabStyleName);
		$section->addTextBreak();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section,'<div style="direction: rtl;float:right">'.$item->not_tested.'</div>');
		
		$section->addTextBreak($lb);
		*/
		
	/*	$section->addText(
			'"Great achievement is usually born of great sacrifice, '
				. 'and is never the result of selfishness." '
				. '(Napoleon Hill)',
			$fontStyle
		);
		
		$fontStyleName = 'oneUserDefinedStyle';
		$phpWord->addFontStyle(
			$fontStyleName,
			array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
		);
		$section->addText(
			'"The greatest accomplishment is not in never falling, '
				. 'but in rising again after you fall." '
				. '(Vince Lombardi)',
			$fontStyleName
		);
		
		$fontStyle = new \PhpOffice\PhpWord\Style\Font();
		$fontStyle->setBold(true);
		$fontStyle->setName('Tahoma');
		$fontStyle->setSize(13);
		$myTextElement = $section->addText('"Believe you can and you\'re halfway there." (Theodor Roosevelt)');
		$myTextElement->setFontStyle($fontStyle);*/

		// Saving the document as OOXML file...
		
		
		$file_name = "survey_report_".str_slug($item->owner_name)."_".$item->id;
		
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		
		\File::exists(public_path('uploads/survey/')) or mkdir(public_path('uploads/survey/'), 0777, true);
		$loc = public_path('uploads/survey/'.$file_name.'.docx');
		
		if (\File::exists($loc)) {
            unlink($loc);
        }
		
		$objWriter->save($loc);
		
		return response()->download($loc);
	}


}
