<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
	use SoftDeletes;
	protected $table = 'survey';

    
    protected $primaryKey = 'id';
	
    protected $guarded = [];
	
	protected $appends = ['created_tz','created_a','last_sync_at_a','created_exp'];
	
	
	function getNetCost() {
        return $this->issue->sum('total_cost');
    }

	public function getCreatedTzAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->diffForHumans();
        }
		return $this->created_at;
    }
	public function getCreatedExpAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->format("d/m/Y");
        }
		return $this->created_at;
    }
	public function getCreatedAAttribute()
    {
		if($this->created_at != ""){
           return \Carbon\Carbon::parse($this->created_at)->format(\config('settings.date_format_on_app'));
        }
		return $this->created_at;
    }
	public function getLastSyncAtAAttribute()
    {
		if($this->last_sync_at != ""){
           return \Carbon\Carbon::parse($this->last_sync_at)->format(\config('settings.date_format_on_app'));
        }
		return $this->last_sync_at;
    }
	
    public function issue()
    {
        return $this->hasMany('App\Issue', 'survey_id', 'id')->select("issue.*")->join('categories as pc','issue.category_id','=','pc.id')->leftjoin('categories as cc','issue.child_category_id','=','cc.id')->orderby('pc.display_order','asc')->orderby('cc.display_order','asc')->orderby('id','asc')->orderby('location','asc');
    }
	public function usedParentCategory()
    {
        $pc = $this->issue->pluck("category_id");
		return \App\Categories::whereIn("id",$pc)->get();
    }
	public function getLastLocation(){
		$res = "";
		$issue = \App\Issue::where('survey_id',$this->id)->orderby('updated_at','DESC')->first();
		if($issue){
			$res = $issue->location;
		}
		return $res;
	}
	public function issueplace()
    {
        return $this->hasMany('App\Issue', 'survey_id', 'id')->select("issue.*")->join('categories as pc','issue.category_id','=','pc.id')->leftjoin('categories as cc','issue.child_category_id','=','cc.id')->orderby('location','asc')->orderby('pc.display_order','asc')->orderby('cc.display_order','asc')->orderby('id','asc');
    }
	
	public function surveyor()
    {
        return $this->belongsTo('App\User', 'surveyor_id', 'id');
    }
	
	public function branch()
    {
        return $this->belongsTo('App\Branches', 'surway_logo', 'id');
    }
	
	public function refefile()
    {
        return $this->hasMany('App\Refefile', 'refe_field_id', 'id')->where('refe_table_field_name', 'survey_id');
    }
	
	
	
	
	///////// image to text ////////////
	public function accessories()
    {
        return $this->hasMany('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'survey_id')->where('refe_type', 'accessories');
    }
	public function pfmaterials()
    {
        return $this->hasMany('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'survey_id')->where('refe_type', 'pf_materials');
    }
	// support info image
	public function spinfoa()
    {
        return $this->hasOne('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'survey_id')->where('refe_type', 'spinfoa');
    }
	public function spinfob()
    {
        return $this->hasOne('App\Refetext', 'refe_field_id', 'id')->where('refe_table_field_name', 'survey_id')->where('refe_type', 'spinfob');
    }
	public function getaccessories($default=0){
		if($this->accessories && $this->accessories->count() >0){
			return $this->accessories;
		}else if($default){
			return \App\Refetext::where('refe_type', 'default_accessories')->get();
		}else if($this->default_fields != "no"){
			return \App\Refetext::where('refe_type', 'default_accessories')->get();
		}else{
			return [];
		}
	}
	public function getpfmaterials($default=0){
		if($this->pfmaterials && $this->pfmaterials->count() >0){
			return $this->pfmaterials;
		}else if($default){
			return \App\Refetext::where('refe_type', 'default_pf_materials')->get();
		}else if($this->default_fields != "no"){
			return \App\Refetext::where('refe_type', 'default_pf_materials')->get();
		}else{
			return [];
		}
	}
	public function getspinfoa(){
		if($this->spinfoa || $this->spinfob){
			return $this->spinfoa;
		}else{
			return \App\Refetext::where('refe_type', 'default_spinfoa')->first();
		}
	}
	public function getspinfob(){
		if($this->spinfoa || $this->spinfob){
			return $this->spinfob;
		}else{
			return \App\Refetext::where('refe_type', 'default_spinfob')->first();
		}
	}
	public function getPerposeOfVisit(){
		$res = \Lang::get('report.declartaion.l7_a');
		if($this->purpose_of_visit && $this->purpose_of_visit != ""){
			$res = $this->purpose_of_visit;
		}
		return $res;
	}
	public function textChecklist()
    {
        return $this->hasMany('App\RefeTextSurveyCheckbox', 'refe_field_id', 'id')->where('refe_table_field_name', $this->getTable())->orderby("priority","asc");
    }
	
	public static function fileOrder($survey_id)
    {
		$search = array("JPEG_","jpeg_","PNG_","png_","_.jpeg","_.JPEG","_.jpg","_.JPG","_.png", "_.PNG");
	    $replace = array("","","","","","","","","","");
		if($survey_id){
			$newS = \App\Survey::where("id",$survey_id)->first();
			if($newS){
				$_kv = [];
				foreach($newS->issue as $newI){
					$_ved = [];
					foreach($newI->refefile as $ref){
						$_str=str_replace($search, $replace,$ref->refe_file_real_name);
						$arrs = (explode("_",$_str));
						//$ved[$newI->id][] = $_str;
						if(count($arrs) ==2 && strlen($arrs[0]) == 8 && strlen($arrs[1]) == 6 && is_numeric($arrs[0]) && is_numeric($arrs[1]) ){
							$ved = strtotime(\Carbon\Carbon::createFromFormat('Ymd His',$arrs[0]." ".$arrs[1])->toDateTimeString());
							$_kv[$ved] = $ref->id;
							$_ved[] = $ved;
						}
						
					}
					if(count($_ved)){
						//echo "-----------------<pre>"; print_r($_ved);
						sort($_ved);
						//echo "<pre>"; print_r($_ved);
						foreach($_ved as $key=>$val){
							if(isset($_kv[$val])){
								//echo ":".$key . "-".Refefile::where("id",$_kv[$val])->first()->priority;
								Refefile::where("id",$_kv[$val])->update(["priority"=>$key]);
							}
						}
					}
					
				}
			}
		}
		
	}
}
