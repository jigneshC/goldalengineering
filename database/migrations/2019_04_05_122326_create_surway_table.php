<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurwayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey', function (Blueprint $table) {
            $table->increments('id');
			
			$table->integer('parent_id')->nullable()->default(0);
			$table->integer('surveyor_id')->nullable()->default(0);
			$table->integer('assign_to_id')->nullable()->default(0);
			$table->integer('owner_id')->nullable()->default(0);
			
			$table->string('unique_id',191)->nullable()->default(0);
			$table->integer('local_id')->nullable()->default(0);
			 
			$table->string('owner_name',191)->nullable();
			$table->string('owner_email',191)->nullable();
			$table->string('owner_phone',191)->nullable();
			
			$table->integer('surway_logo')->nullable()->default(0);
			$table->integer('is_the_sale_law_1973')->nullable();
			$table->integer('is_consultants')->nullable();
			
			$table->integer('issue_count')->nullable()->default(0);
			
			$table->longtext('property_desc')->nullable();
			$table->longtext('address')->nullable();
			$table->longtext('legal_thing_a')->nullable();
			$table->longtext('legal_thing_isa')->nullable();
			$table->longtext('surveyor_note')->nullable();
			
			$table->longtext('reviews_and_summary')->nullable();
			$table->longtext('not_tested')->nullable();
			$table->longtext('general_info')->nullable();
			$table->longtext('other_info')->nullable();
			$table->timestamp('created')->nullable();
			$table->integer('survey_date_option')->nullable()->default(0);
			$table->timestamp('survey_date')->nullable();
			$table->enum('status', ['new','open','cancelled','closed'])->default('new');
			
			$table->softDeletes();
            $table->timestamps();
			$table->dateTime('last_sync_at')->nullable();
			$table->string('default_fields',255)->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey');
    }
}
